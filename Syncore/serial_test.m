clear all;

figure(1);

n = 2500*3*4;

%open serial object
s = serial('COM3');
s.BaudRate = 115200*4;
s.InputBufferSize = n*4;
s.timeout = 100;
s.flowcontrol = 'none';
fopen(s);



fwrite(s,'0','uchar');  
%fwrite(s,'4','uchar'); 
pause(3.234564)
d=COM_get_data(s);
fclose(s);
%%
 fclose(s);
b=s.BytesAvailable
a = uint32(fread(s, b/4, 'uint32'));
 
 channels = bitand(a, 255);
 data = typecast(a, 'int32');
 data = data/256;
 

 
 %fwrite(s,'1','uchar');
 fclose(s);
 
 x0data = data(channels == 0, 1);
 y0data = data(channels == 1, 1);
 z0data = data(channels == 2, 1);
 
 x1data = data(channels == 3, 1);
 y1data = data(channels == 4, 1);
 z1data = data(channels == 5, 1);
 
 x2data = data(channels == 6, 1);
 y2data = data(channels == 7, 1);
 z2data = data(channels == 8, 1);
 
 x3data = data(channels == 9, 1);
 y3data = data(channels == 10, 1);
 z3data = data(channels == 11, 1);
 
 x0data = x0data(500:end);
 y0data = y0data(500:end);
 z0data = z0data(500:end);
 x1data = x1data(500:end);
 y1data = y1data(500:end);
 z1data = z1data(500:end);
 x2data = x2data(500:end);
 y2data = y2data(500:end);
 z2data = z2data(500:end);
 x3data = x3data(500:end);
 y3data = y3data(500:end);
 z3data = z3data(500:end);
 

 
 figure(1)
 subplot(311)
 plot(x0data-mean(x0data))
 title(['x0data, mean = ',num2str(mean(x0data)),'  std = ',num2str(std(double(x0data))),'  SNR = ',num2str(abs(round(100*mean(x0data)/std(double(x0data)))))])
 subplot(312)
 plot(y0data-mean(y0data))
 title(['y0data, mean = ',num2str(mean(y0data)),'  std = ',num2str(std(double(y0data))),'  SNR = ',num2str(abs(round(100*mean(y0data)/std(double(y0data)))))])
 subplot(313)
 plot(z0data-mean(z0data))
 title(['z0data, mean = ',num2str(mean(z0data)),'  std = ',num2str(std(double(z0data))),'  SNR = ',num2str(abs(round(100*mean(z0data)/std(double(z0data)))))])
 
 figure(2)
 subplot(311)
 plot(x1data-mean(x1data))
 title(['x1data, mean = ',num2str(mean(x1data)),'  std = ',num2str(std(double(x1data))),'  SNR = ',num2str(abs(round(100*mean(x1data)/std(double(x1data)))))])
 subplot(312)
 plot(y1data-mean(y1data))
 title(['y1data, mean = ',num2str(mean(y1data)),'  std = ',num2str(std(double(y1data))),'  SNR = ',num2str(abs(round(100*mean(y1data)/std(double(y1data)))))])
 subplot(313)
 plot(z1data-mean(z1data))
 title(['z0data, mean = ',num2str(mean(z1data)),'  std = ',num2str(std(double(z1data))),'  SNR = ',num2str(abs(round(100*mean(z1data)/std(double(z1data)))))])
 
 figure(3)
 subplot(311)
 plot(x2data-mean(x2data))
 title(['x2data, mean = ',num2str(mean(x2data)),'  std = ',num2str(std(double(x2data))),'  SNR = ',num2str(abs(round(100*mean(x2data)/std(double(x2data)))))])
 subplot(312)
 plot(y2data-mean(y2data))
 title(['y2data, mean = ',num2str(mean(y2data)),'  std = ',num2str(std(double(y2data))),'  SNR = ',num2str(abs(round(100*mean(y2data)/std(double(y2data)))))])
 subplot(313)
 plot(z2data-mean(z2data))
 title(['z2data, mean = ',num2str(mean(z2data)),'  std = ',num2str(std(double(z2data))),'  SNR = ',num2str(abs(round(100*mean(z2data)/std(double(z2data)))))])

 figure(4)
 subplot(311)
 plot(x3data-mean(x3data))
 title(['x2data, mean = ',num2str(mean(x3data)),'  std = ',num2str(std(double(x3data))),'  SNR = ',num2str(abs(round(100*mean(x3data)/std(double(x3data)))))])
 subplot(312)
 plot(y3data-mean(y3data))
 title(['y2data, mean = ',num2str(mean(y3data)),'  std = ',num2str(std(double(y3data))),'  SNR = ',num2str(abs(round(100*mean(y3data)/std(double(y3data)))))])
 subplot(313)
 plot(z3data-mean(z3data))
 title(['z2data, mean = ',num2str(mean(z3data)),'  std = ',num2str(std(double(z3data))),'  SNR = ',num2str(abs(round(100*mean(z3data)/std(double(z3data)))))])
