function [y,J]=h(x,network,K,meas_zero)
    % Computes the measurement and its jacobian of the dipole model
    
    % y         - The measurement
    % J         - The Jacobian (with respect to x)
    % x         - The state
    % network   - Struct consisting of sensor stuff
    % K         - Number of tools
    % meas_zero - Thes static magentic field
    
    
    % Sensor Model
        
    % y_n = h(x_n) + e_n, where e_n ~ N(0,R) describes the sensor modeling, 
    % where y_k is the measurement, x_k the state of the system and
    % e_n measurment noise
    
    % We model the tool as a magnetic dipole. Therefor, here we define the dipole model. This models the magnetic field as position r
    % relative to a magnetic dipole with dipole moment m. For reference see
    % for example 
    %
    % [1] J. D. Jackson. Classical Electrodynamics. John Wiley and Sons, Inc., 2 edition, 1975.
    %
    % or more related to this application
    %
    %[T1] N. Wahlstr�m, Target Tracking using Maxwell's Equations. Master's Thesis. Presented June 15, 2010.    
            
    % We use 9 components state-vector for each tool. x(1:3) encodes the position of the
    % tool, x(4:6) the velocity, x(7:9) the orientation. Here the
    % orientation is encoded with the dipole vector m. The last state
    % x(end) the sensor gain mainly depending on the temperature. Thus, in
    % total the state dimension will be 9*K+1, where K is the number of
    % tools.
    
    % Using spatially distributed network we will measure the magnetic
    % field at diffeent positions relative to the dipole. Since the sensor
    % i is located at th(i,:), the relative positions will be
    % x(1:3)-th(:,i). This gives the following sensor model:
    
    th=network.th;
    N_imu=size(th,2);
    y=zeros(3*N_imu,1);
    J=zeros(3*N_imu,9*K+1);
    for k=1:K %Iterate over the number of tools
        m=x((7:9)+9*(k-1)); %Extract the magnetic dipole moment for tool k        
        for i=1:N_imu
            %Compute all terms that are needed (such that we don't compute them
            %multiple times

            r=x((1:3)+9*(k-1))-th(:,i);
            R=r*r';
            r2=r'*r;
            r5=r2^(-5/2);

            % The the derivative of the dipole model with respect to the magnetic
            % dipole moment m

            Jm= (3*R-r2*eye(3))*r5;
            %Jm= (3*(r*r')-r'*r*eye(3))*(r'*r)^(-5/2);

            %The dipole model
            %y(3*(i-1)+(1:3)) = y(3*(i-1)+(1:3))+Jm*m;            
            y(3*(i-1)+(1:3)) = y(3*(i-1)+(1:3))+network.A(:,:,i)*Jm*m;            

            if nargout ==2 %If the Jacobian is an outbut, compute it as well
                % The the derivative of the didipole model with respect to the position r
                %Jr= 3*((r'*m)*eye(3)+(r*m')+(m*r')-5*((r'*m)/(r'*r))*r*r')*(r'*r)^(-5/2);
                RM=r*m';
                rm=r'*m;
                Jr= 3*(rm*eye(3)+RM+RM'-5*rm/r2*R)*r5;

                % The Jaccobian (with respect to x=[r,v,m]
                J(3*(i-1)+(1:3),(1:9)+9*(k-1))= [Jr zeros(3,3) Jm];
            end
        end
    end    
    %% Add the gain as a state    
    
    y=(y+meas_zero'); %Add the static magnetic field to the measurement
    if nargout==2 %If the Jacobian is an output, compute it as well
        J=x(9*K+1)*J; %Multiply with the gain...
        J(:,9*K+1)=y; %.. and augment the last column corresponding to derivative of the measurement with respect to the gain.
    end
    y=x(9*K+1)*y; %Multiply the measurement with the gain
        
end
% -------------------------------------------------------------------------
% end of function
