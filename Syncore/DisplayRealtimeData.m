function str_date=DisplayRealTimeData(vargin,tool,network,sensor,tuning,saveData,mode_data,calibrate_startup)

%% DisplayRealTimeData(vargin);
%
% Real-time display of calibrated data or 3D orientation data from an MTi
% or MTx
%
% Input argument
% COMports [integer, integer, ...] to which MTs are connected
%
%
% Press key on keyboard while running:
%
% "Q" = Quit
% "Z" = Toggle Zoom in/out
% "M" = View only magnetometer data
% "T" = Pose tracker
% "O" = Color calligraphy
% "G" = Grey calligraphy
% "C" = Clear screen
% "D" = View all MTi / MTx data (default)
%
% Orientation data output MODE:
% "Q" = Quit 

% set default values
defaultValues={'caldata',2,1.0,1,1.0};
[DisplayMode,zoom_level_setting,filterSettings_gain,filterSettings_corr,filterSettings_weight]...
   =deal(defaultValues{:});


%global mode_data %0: Read data from file 1: Read data from sensor

if mode_data==0; %Input is a file name
    DATA=load(['data/',vargin,'_meas.csv']);    
    % Number of sensors
    N_imu=size(DATA,2)/3; %Each sensor has 3 measurement
    
else %Input is a vector of COM-ports    
    % Number of sensors
    N_imu=4;
    
    %open serial object
    h = serial(vargin);
end    

try
    % use try-catch to avoid the need to shutdown MATLAB if m-function fails (e.g. during debugging)
    % This is needed because MATLAB does not properly release COM-objects when
    % the m-function fails/crasehes and the object is not explicitly released with "fclose(h)"
    
    if mode_data==1 %Input is a vector of COM-ports
        
        h.BaudRate = 115200*4;
        h.InputBufferSize = 500*3*4*4; % Have 500 samples in the buffer
        h.timeout = 100;
        h.flowcontrol = 'none';
        fopen(h);
        fwrite(h,'0','uchar');  
        
    else %Data from file
        h=[]; %No sensor to handle
        
    end
    
    for i=1:N_imu                        
            d_cell{i}=zeros(1,3);
            last_d_cell{i} = d_cell{i};            
    end
            
    fpIMU=[];
    fpXF=[];
    
    %Compute the sacle factor of the sensor data
    sensor.scale_factor=1;
    str_date='no captured data';
    if mode_data==1 %Data comes from the COM-ports
        if calibrate_startup==1
            %Wait a while        
            pause(1)
            %% Compute the stationary offset for sensor (due to earth magnetic field)
            meas=COM_get_data(h,sensor); % retreive data from MTObj object                
            meas=meas((end-100):end,:); %Remove the first 100 samples (usually defected)                        
            
            N=size(meas,1); %Number of samples in the calibration             
            
            meas_zero=sum(meas,1)/N; %Compute the offset for all sensors
            
            y=(meas-repmat(meas_zero,N,1));
            R=y'*y/N; %Compute the covariance matrix of the measurement noise.                       
            
            % Add the contribution from the quantification
            %R=R+0.004^2*eye(12);
            %R=R+eye(12)*((sensor.range(2)-sensor.range(1))/sensor.bits)^2;

            save('data/meas_zero.mat','meas_zero','R');
        else
            % Load calibration data
            load(['data/',vargin,'_meas_zero.mat'])
        end        
        
        if saveData==1
            %% Output File - IMU
            % Create an output file and open it
            str_date=datestr(now(), 30);
            dumpfile = sprintf('data/%s_meas.csv', str_date);
            fpIMU = fopen(dumpfile, 'w');
            if fpIMU < 0                
                error('Could not open output file.');
            end
            
            dumpfile2 = sprintf('data/%s_xf.csv', str_date);
            fpXF = fopen(dumpfile2, 'w');
            if fpXF < 0                
                error('Could not open output file.');
            end
            save(['data/',str_date,'_meas_zero.mat'],'meas_zero','R');
        end
    else
       load(['data/',vargin,'_meas_zero.mat'])       
    end
    
    sensor.R=R; %Add measurement noise to the model
        
    %Compute the orientation of the network (notise that the
    %z-component of the earth magnetic field is negative on the northen
    %hemisphere, not neccecary to implement
    %if mean(meas_zero(3*(1:N_imu))) > 0 %Turned upside down
    %    A=diag([1 -1 -1]);
    %else
        A=eye(3); %Turned "correctly"
    %end        
    %network.th=A*network.th;
    %A=blkdiag(A,A,A,A);
    %meas_zero=(A*meas_zero')';       
    

    
    %% Motion model
    
    % x_{n+1} = A*x_n + B*w_n, where w_n ~ N(0,Q) describes the dynamics of the tool
    %
    % Assume a (nearly) constant velocity model fo the position and a (nearly) constant position model for the orientation
    % This gives the following entries, see for example 
    %
    % [3] F. Gustafsson. Statistical Sensor Fusion. Studentlitteratur, 1 edition, 2010.
    
    T=1/sensor.SF; %The sampling time
    m.A= blkdiag([eye(3) T*eye(3);zeros(3,3) eye(3)],eye(3));
    m.B= blkdiag([(T^2/2)*eye(3);T*eye(3)],T*eye(3));
    m.Q= blkdiag(tuning.Q_acc^2*eye(3), (tuning.Q_ori*tool(1).dipole_strength)^2*eye(3));
       
    %% Initial guess of the state    
    m.x0=[10;10;10;zeros(3,1);zeros(3,1)]; %Use zero velocity and zero dipole moment as initial state. Use initial position far awy from the network    
    m.xP0=diag([1 1 1 0 0 0 (tool(1).dipole_strength)^2*ones(1,3)]);
    
    % Only for multiple tools-------
    K=length(tool); %Number of tools
    m.A=kron(eye(K),m.A);
    m.B=kron(eye(K),m.B);
    m.Q=kron(eye(K),m.Q);
    m.x0=kron(ones(K,1),m.x0);
    m.xP0=kron(eye(K),m.xP0);
    %----------
    
    % Include the temperature dependent sensor gain as a state
    m.A= blkdiag(m.A,1);
    m.B= blkdiag(m.B,T*1);
    m.Q= blkdiag(m.Q,tuning.Q_gain^2);
    
    % Include the temperature dependent sensor gain as a state
    m.x0=[m.x0;1]; %Use the nominal value 1 as initial state.
    m.xP0=blkdiag(m.xP0, 0.001^2);
    
    
    m.Qs=1;
    
    %% Other initializations        
    xp=m.x0;
    Pp=m.xP0;
    xf=xp;
    Pf=Pp;
    yf=meas_zero;
    
    %Initialize time duration for a certain plot    
    t_acc=0;
    accN=0;
        
    % init figure plotting variables
    % set time scale zoom level
    zoom_levels=[12*sensor.SF,8*sensor.SF,4*sensor.SF]; % in seconds
    zoom_level=zoom_levels(zoom_level_setting);
    OldFigureUserData = [0 0 0 0]; status = 0; last_t=0;
    
    ResetFigure =1; first_time=1;
    
    % create figure
    % NOTE, the releasing of MTObj is done in the Figure Close Request function
    % "mt_figure_close"
    [f_handle, p_handle] = mt_create_figure(OldFigureUserData(3), -1 ,h, sensor,...
        zoom_level, OldFigureUserData,network.th,fpIMU,fpXF);
        
        % set figure UserData to appropriate values (format:
        % [MT_being_plotted, Zoom, PlotType])
        set(f_handle,'UserData', [0 0 0 0]) 
    
    tic %Start counting time
    TIME=0; %Starting time
        
    % Now we can start looping over all samples!!       
    while ishandle(f_handle) % check if all objects are alive
        if mode_data==1           
                [meas,s,N]=COM_get_data(h,sensor); % the measurement meas is on th form [mag0_x mag0_y mag0_z mag1_x mag1_y mag1_z mag2_x mag2_y mag2_z mag3_x mag3_y mag3_z], however in this application we will on�y use the magnetometer measurement!
                if s                    
                    status=1;
                    
                    for i=1:N_imu %Extract the measurement from each sensor
                        d_cell{i}=meas(:,(3*(i-1)+(1:3)));                    
                    end
                    if saveData==1                        
                        for n=1:N %Iterate over all samples and save them to a file
                            str='%d, %d, %d';
                            str_acc='';
                            data_acc=[];
                            for i=1:N_imu
                                str_acc=[str_acc,str];
                                if i < N_imu
                                    str_acc=[str_acc,', '];
                                end
                                data_acc=[data_acc,double(d_cell{i}(n,:))];
                                
                            end
                            fprintf(fpIMU, [str_acc,' \r\n'],double(data_acc));
                        end
                    end
                end
        else
            TIME_new=toc;
            IND=((floor(TIME*sensor.SF)+1):floor(TIME_new*sensor.SF));
            TIME=TIME_new;
            if max(IND)>size(DATA,1);
                break;
            end
            for i=1:N_imu
                d_cell{i}=DATA(IND,(1:3)+(i-1)*3);
            end
            N=length(IND);
        end
        % Now the data is available! The rest of the code is only to
        % display the data and to make it look nice and smooth on display....
        
        if N > 0, % If there are samples to proces, go further

            %% Filter
            last_xf=xf;
            last_yf=yf;  
            for n=1:N %Do a filter update for the last sample in the iteration (would take too long time to proces all samples on my laptop)
                % create measurement
                y=zeros(3*N_imu,1);
                for i=1:N_imu
                    y((3*(i-1)+1):3*i)=d_cell{i}(n,:)';
                end

                % To the filter step!!
                [xf,Pf,yf]=measurement_update(sensor,y,xp,Pp,tool,tuning,network, meas_zero);               
                [xp,Pp]=time_update(m,xf,Pf,tuning);
                if saveData==1 % Save to file
                    str='%d, %d, %d, %d, %d, %d, %d, %d, %d, %d';   
                    fprintf(fpXF, [str,' \r\n'],xf);                        
                end
            end
            %accN=accN+N;
            %accN/toc
            %% Vizualize the result!!!
            
            % retrieve values from figure
            CurrentFigureUserData = get(f_handle,'UserData'); 
            
            % create timebase            
            t=[last_t (last_t+(1:N)/sensor.SF)];
            last_t=t(end);
            T=t(end)-t(1);
            t_acc=t_acc+T;
            
            if ResetFigure ==1, % check if figure should be reset
                last_t=0; % wrap plot around
                figureUserData = get(f_handle,'UserData');
                % call local function to (re)setup figure
                [f_handle, p_handle, a_handle] = mt_create_figure(CurrentFigureUserData(3), f_handle,h, sensor, zoom_level, figureUserData,network,fpIMU,fpXF);
                ResetFigure = 0;
                t_acc=10;
            end
            
            % check if figures should be reset                           
            if any(CurrentFigureUserData(1:3) ~= OldFigureUserData(1:3)), % check if figure UserData changed by KeyPress
                ResetFigure =1; % make sure plot is reset
                first_time =1; % re-initialize zoom levels too
                
            elseif ((last_t*sensor.SF>zoom_level)&&((CurrentFigureUserData(3) == 0)||(CurrentFigureUserData(3) == 1)))|| (first_time==1) || (CurrentFigureUserData(4)==1)
                ResetFigure =1; % make sure plot is reset
                first_time =0;
                tmp=get(f_handle,'UserData');
                set(f_handle,'UserData',[tmp(1:3) 0]);
                t_acc = mt_plot_data(d_cell, last_d_cell, t, CurrentFigureUserData, f_handle,p_handle,a_handle,xf,last_xf,yf,last_yf,meas_zero,t_acc,tool);
            else % other wise --> plot
                % plot the data using a local funtion                
                    t_acc = mt_plot_data(d_cell, last_d_cell, t, CurrentFigureUserData, f_handle,p_handle,a_handle,xf,last_xf,yf,last_yf,meas_zero,t_acc,tool);
            end
            
            %Save values for next iteration
            OldFigureUserData = CurrentFigureUserData;
            for i=1:N_imu
                last_d_cell{i}=d_cell{i}(end,:);
            end
            status=zeros(1,3);
            
        elseif status>1,
            % MTObj not correctly configured, stopping
            [str_out]=MT_return_error(status);
            disp(str_out);
            disp('MTObj not correctly configured, stopping.....');
            break
        end % if

    end % while

    % release MTObj is done on figure close...not here
    if ishandle(f_handle), % check to make sure figure is not already gone
        close(f_handle)
    end

catch % try catch for debugging
    % make sure serial port object is released even on error
    if ~isempty(h) %Data comes from the COM-ports
        fclose(h)
    end
    % display some information for tracing errors
    disp('Error was catched by try-catch.....MTobj released')
    crashInfo = lasterror; % retrieve last error message from MATLAB
    disp('Line:')
    crashInfo.stack.line
    disp('Name:')
    crashInfo.stack.name
    disp('File:')
    crashInfo.stack.file
    rethrow(lasterror)
end
% -------------------------------------------------------------------------
% end of function MT_DisplayRealtimeData(varargin);

%% -------------------------------------------------------------------------
% LOCAL FUNCTIONS
% -------------------------------------------------------------------------
function [f_handle, p_handle, a_handle] = mt_create_figure(type, f_handle, h, sensor, zoom_level, figureUserData, network ,fpIMU, fpXF)

% local function to create the figure for real time plotting of data.
% accepts plot type information for custom plots
%
% if figure does not yet exist enter -1 in figure_handle input

if ~ishandle(f_handle),
    f_handle = figure('Name','Real-time display of MTi or MTx data','NumberTitle','off');
end

fontSizeUsed = 12;
% init
p_handle = zeros(12); a_handle = zeros(12);

switch type
    case 0% magnetic data (default)        
        if figureUserData(2)==0
           a=[-0.01 0.01];
        else
           a=sort(sensor.range);         
        end
        
        YLims = [ a; a; a; a];     
        for i=1:12,
            subplot(3,4,i), p_handle(i)=plot(0,0,'EraseMode','none');a_handle(i) = gca; axis(a_handle(i),[0 (zoom_level+1)./sensor.SF YLims(mod(i-1,4)+1,:)]);
            grid on;
            if figureUserData(2)==1
                set(gca,'YTick',-10:1:10);
            end
        end
        tlh = title(a_handle(1),['Magnetometer1']);   set(tlh,'FontSize',fontSizeUsed);   xlh = xlabel(a_handle(9),'time [s]');  ylh = ylabel(a_handle(1),'X_S, [Gauss]');
        tlh = title(a_handle(2),['Magnetometer2']);           set(tlh,'FontSize',fontSizeUsed);   xlh = xlabel(a_handle(10),'time [s]');  ylh = ylabel(a_handle(5),'Y_S, [Gauss]');
        tlh = title(a_handle(3),['Magnetometer3']);    set(tlh,'FontSize',fontSizeUsed);   xlh = xlabel(a_handle(11),'time [s]');  ylh = ylabel(a_handle(9),'Z_S, [Gauss]');
        tlh = title(a_handle(4),['Magnetometer4']);    set(tlh,'FontSize',fontSizeUsed);   xlh = xlabel(a_handle(12),'time [s]');                 
        
    case 1 % magnetic data + estimated states                
        YLims = [-0.02 0.02; -0.7 0.7; -0.1 0.1; -2e-3 2e-3];
        for i=1:12
            subplot(3,4,i), p_handle(i)=plot(0,0,'EraseMode','none');a_handle(i) = gca; axis(a_handle(i),[0 (zoom_level+1)./sensor.SF YLims(mod(i-1,4)+1,:)]); grid on; 
        end
        
        tlh = title(a_handle(1),['Magnetometer [a.u.]']);   set(tlh,'FontSize',fontSizeUsed);   xlh = xlabel(a_handle(9),'time [s]');  ylh = ylabel(a_handle(1),'X_S');
        tlh = title(a_handle(2),['Position']);           set(tlh,'FontSize',fontSizeUsed);   xlh = xlabel(a_handle(10),'time [s]');  ylh = ylabel(a_handle(5),'Y_S');
        tlh = title(a_handle(3),['Velocity']);    set(tlh,'FontSize',fontSizeUsed);   xlh = xlabel(a_handle(11),'time [s]');  ylh = ylabel(a_handle(9),'Z_S');
        tlh = title(a_handle(4),['Dipole moment']);    set(tlh,'FontSize',fontSizeUsed);   xlh = xlabel(a_handle(12),'time [s]');
           
    case 2 % Plot pose tracker
             YLims = [-0.7 0.7];
        
            subplot(1,1,1); p_handle(1)=surf(zeros(2,2),zeros(2,2),zeros(2,2),'EraseMode','none');%hold on;plot3(th(1),th(2),th(3),'b*','EraseMode','none');plot3(-th(1),-th(2),-th(3),'r*','EraseMode','none');
            a_handle = gca; axis(a_handle,[YLims YLims YLims]); 
            
            %Fix the position and angle of the camera
            pos_t=[0,0,8];
            campos(pos_t);
            ang=get(a_handle,'CameraViewAngle');
            camva(ang/11.5);
            set(a_handle,'CameraUpVector',[0 1 0])
            set(f_handle,'units','normalized','outerposition',[0 0 1 1])
            
            %Make a grid
            grid on;
            tick=-0.20:0.1:0.20;
            set(a_handle,'XTick',tick);set(a_handle(1),'YTick',tick);set(a_handle,'ZTick',tick);
            
            %Labels
            xlabel(a_handle,'X_S');
            ylabel(a_handle,'Y_S');
            zlabel(a_handle,'Z_S');
            
            %The colormap
            %colormap([0.8 0.8 0.8;0 1 0])
            colormap([0.8 0.8 0.8;1 0 0;0 1 0])            
            caxis([0 1])
                 
    case 3  %Color calligraphy
            YLims = [-0.7 0.7];
            
            subplot(1,1,1); 
            p_handle(1)=surf(zeros(2,2),zeros(2,2),zeros(2,2),'EraseMode','none');
            a_handle(1) = gca; axis(a_handle(1),[YLims YLims YLims]); 
            
            %Fix the position and angle of the camera
            pos_t=[0,0,8];
            campos(pos_t);
            ang=get(a_handle(1),'CameraViewAngle');
            camva(ang/11.5);
            set(a_handle(1),'CameraUpVector',[0 1 0])
            set(f_handle,'units','normalized','outerposition',[0 0 1 1])
            
            %Make grid
            grid on;
            tick=-0.20:0.1:0.20;
            set(a_handle(1),'XTick',tick);set(a_handle(1),'YTick',tick);set(a_handle(1),'ZTick',tick);
            
            %Lables
            xlabel(a_handle(1),'X_S');
            ylabel(a_handle(1),'Y_S');
            zlabel(a_handle(1),'Z_S');
            tlh = title(a_handle(1),['Pose tracker']); set(tlh,'FontSize',fontSizeUsed);           
            
            %Colormap
            shading interp 
            colormap(hsv(128))
            caxis([0 1])

            
   case 4 % Grey calligraphy
            YLims = [-0.7 0.7];
            
            subplot(1,1,1); p_handle(1)=surf(zeros(2,2),zeros(2,2),zeros(2,2),'EraseMode','none');
            a_handle(1) = gca; axis(a_handle(1),[YLims YLims YLims]); 
            
            %Fix the position and angle of the camera
            pos_t=[0,0,8];
            campos(pos_t);
            ang=get(a_handle(1),'CameraViewAngle');
            camva(ang/11.5);
            set(a_handle(1),'CameraUpVector',[0 1 0])
            set(f_handle,'units','normalized','outerposition',[0 0 1 1])
            
            %Make grid
            grid on;
            tick=-0.20:0.1:0.20;
            set(a_handle(1),'XTick',tick);set(a_handle(1),'YTick',tick);set(a_handle(1),'ZTick',tick);
            
            %Labels
            xlabel(a_handle(1),'X_S');
            ylabel(a_handle(1),'Y_S');
            zlabel(a_handle(1),'Z_S');
            tlh = title(a_handle(1),['Pose tracker']); set(tlh,'FontSize',fontSizeUsed);           
            
            %Colormap
            shading interp            
            colormap(gray(128))
end

set(f_handle,'CloseRequestFcn',{@mt_figure_close,h,f_handle,fpIMU,fpXF});
set(f_handle,'KeyPressFcn',{@mt_figure_keypress,f_handle});
set(f_handle,'UserData', figureUserData);
% -------------------------------------------------------------------------
% end of function


%% -------------------------------------------------------------------------
function mt_figure_keypress(obj, eventdata, f_handle)

% local function to handle KeyPress events on figure.
% (D)efault display, (M)agnetometer only, Pose
% (T)racker, c(O)lor calligraphy, (G)rey calligraphy
%
% envoked when a key is pressed when figure is in focus

in_key=lower(get(f_handle,'CurrentCharacter'));
tmp = get(f_handle,'UserData');

switch in_key
    case 'z' % toggle zoom mode
        pause(0.2)
        figure(f_handle)
        if tmp(2) == 0, % check zoom level
            set(f_handle,'UserData',[tmp(1) 1 tmp(3) 0]); % toggle to next zoom mode
        else
            set(f_handle,'UserData',[tmp(1) 0 tmp(3) 0]); % toggle to default zoom mode
        end
        
    case 'd'
        disp('Switching the default display mode, all 9 data streams...')
        pause(0.2)
        figure(f_handle)
        set(f_handle,'UserData',[tmp(1:2) 0 0]); % set to default mode

    case 'm'
        disp('Switching to display only 3D magnetometer data stream...')
        pause(0.2)
        figure(f_handle)
        set(f_handle,'UserData',[tmp(1:2) 1 0]); % set to defult mode

    case 't'
        disp('Switching to display the pose tracker')
        pause(0.2)
        figure(f_handle)
        set(f_handle,'UserData',[tmp(1:2) 2 0]); % set to pose tracker mode
       
     case 'o'
        disp('Switching to display the color calligraphy')
        pause(0.2)
        figure(f_handle)
        set(f_handle,'UserData',[tmp(1:2) 3 0]); % set to color calligraphy mode
  
     case 'g'
        disp('Switching to display the grey calligraphy')
        pause(0.2)
        figure(f_handle)
        set(f_handle,'UserData',[tmp(1:2) 4 0]); % set to color calligraphy mode 
               
    case 'c'
        disp('Clear graph')
        pause(0.2)
        set(f_handle,'UserData',[tmp(1:3) 1]); % set to mag mode
        
    case '1'
        disp('Calibrate sensor network')
        
    case 'q'
        disp('Quitting demo DisplayRealtimeData...')
        pause(0.2)
        figure(f_handle)
        close(f_handle)
 
    otherwise
        disp('Unknown command option....displaying help data')
        disp(' ')
        eval('help DisplayRealtimeData')

end

%% -------------------------------------------------------------------------
function t_acc = mt_plot_data(d_cell, last_d_cell, t, CurrentFigureUserData, f_handle,p_handle, a_handle, xf, last_xf,yf,last_yf,meas_zero,t_acc,tool)

% local function to plot the data using "low-level" set fucntions for smooth plotting
    N_imu=length(d_cell);
    style={'-',':','--','-.'};
    col={'b','r','g','k'};
    tmp = get(f_handle,'UserData');
    
switch CurrentFigureUserData(3) % check plot type
        case 0 %default
            
            
        if CurrentFigureUserData(2)==1
           meas_zero=zeros(size(meas_zero)); % Plot around true zero if zoomed out
        end
 
        %     plot the data
        for i=1:N_imu
            %Plot the measurement
            set(p_handle(i),'XData',t,'YData',[last_d_cell{i}(1,1) d_cell{i}(:,1)']-meas_zero(1+(i-1)*3),'Color','b','LineWidth',2)
            
            set(p_handle(4+i),'XData',t,'YData',[last_d_cell{i}(1,2) d_cell{i}(:,2)']-meas_zero(2+(i-1)*3),'Color','b','LineWidth',2)
            set(p_handle(8+i),'XData',t,'YData',[last_d_cell{i}(1,3) d_cell{i}(:,3)']-meas_zero(3+(i-1)*3),'Color','b','LineWidth',2)
            
            %Plot the filterd measurement from the model (they should
            %correspond to the real measurement if the model is good)
            set(p_handle(i),'XData',t([1 end]),'YData',[last_yf(1+3*(i-1)) yf(1+3*(i-1))]-meas_zero(1+(i-1)*3),'Color','g','LineWidth',2)
            set(p_handle(4+i),'XData',t([1 end]),'YData',[last_yf(2+3*(i-1)) yf(2+3*(i-1))]-meas_zero(2+(i-1)*3),'Color','g','LineWidth',2)
            set(p_handle(8+i),'XData',t([1 end]),'YData',[last_yf(3+3*(i-1)) yf(3+3*(i-1))]-meas_zero(3+(i-1)*3),'Color','g','LineWidth',2)
          
        end
        
        

    case 1 % Only magnetometers
   
        
        %     plot the data                
        for i=1:N_imu        
            set(p_handle(1),'XData',t,'YData',[last_d_cell{i}(1,1) d_cell{i}(:,1)']-meas_zero(1+(i-1)*3),'Color',col{i},'LineStyle',style{i},'LineWidth',2)
            set(p_handle(5),'XData',t,'YData',[last_d_cell{i}(1,2) d_cell{i}(:,2)']-meas_zero(2+(i-1)*3),'Color',col{i},'LineStyle',style{i},'LineWidth',2)
            set(p_handle(9),'XData',t,'YData',[last_d_cell{i}(1,3) d_cell{i}(:,3)']-meas_zero(3+(i-1)*3),'Color',col{i},'LineStyle',style{i},'LineWidth',2)
        end
        K=(length(xf)-1)/9;
        for k=1:K
            set(p_handle(2),'XData',t([1 end]),'YData',[last_xf(1+9*(k-1)) xf(1+9*(k-1))'],'Color','b','LineWidth',2)
            set(p_handle(6),'XData',t([1 end]),'YData',[last_xf(2+9*(k-1)) xf(2+9*(k-1))'],'Color','g','LineWidth',2)
            set(p_handle(10),'XData',t([1 end]),'YData',[last_xf(3+9*(k-1)) xf(3+9*(k-1))'],'Color','r','LineWidth',2)

            set(p_handle(3),'XData',t([1 end]),'YData',[last_xf(4+9*(k-1)) xf(4+9*(k-1))'],'Color','b','LineWidth',2)
            set(p_handle(7),'XData',t([1 end]),'YData',[last_xf(5+9*(k-1)) xf(5+9*(k-1))'],'Color','g','LineWidth',2)
            set(p_handle(11),'XData',t([1 end]),'YData',[last_xf(6+9*(k-1)) xf(6+9*(k-1))'],'Color','r','LineWidth',2)

            set(p_handle(4),'XData',t([1 end]),'YData',[last_xf(7+9*(k-1)) xf(7+9*(k-1))'],'Color','b','LineWidth',2)
            set(p_handle(4),'XData',t([1 end]),'YData',[norm(last_xf((7:9)+9*(k-1))) norm(xf((7:9)+9*(k-1)))],'Color','k','LineWidth',2)
            set(p_handle(8),'XData',t([1 end]),'YData',[last_xf(8+9*(k-1)) xf(8+9*(k-1))'],'Color','g','LineWidth',2)
            set(p_handle(12),'XData',t([1 end]),'YData',[last_xf(9+9*(k-1)) xf(9+9*(k-1))'],'Color','r','LineWidth',2)
        end
        
    case 2 % Pose tracker
        
        K=(length(xf)-1)/9; %Number of tools          
        for k=1:K
            % Change the size of the cylinder dependent on the distance
            scale=0.9/(1-xf(3+9*(k-1)));
            % Construct the cylinder
            [X(:,:,:,k),Y(:,:,:,k),Z(:,:,:,k),C(:,:,:,k)]=cylinder_my(xf((1:3)+9*(k-1)),-xf((7:9)+9*(k-1))/norm(xf((7:9)+9*(k-1)))*tool(k).length*scale,tool(k).thickness*scale);
        end
           
        
        
        % Update the plot handle        
        set(p_handle(1),'EraseMode','normal')        
        for k=1:K
            if k>=2
                set(p_handle(1),'EraseMode','none')
                drawnow
            end
            set(p_handle(1),'XData',X(:,:,:,k),'YData',Y(:,:,:,k),'ZData',Z(:,:,:,k),'CData',C(:,:,:,k)/k) 
        end                                

        
    case 3 % Color calligraphy
                
        %Compute the coordinates of painted region
        [X,Y,Z,C,paint]=drawer_my_color(xf,last_xf,tool(1));
        
        % Decide if the pen is close enough to the screen        
        if paint
            set(p_handle(1),'XData',X,'YData',Y,'ZData',Z,'CData',C)
        end
        
        %if norm(d_cell{1}(:,7:9))>3
        %    text(0,0,0,'Ej f�r n�ra sensor 1!','FontSize',22)
        %end
        %if norm(d_cell{2}(:,7:9))>3
        %    text(0,0,0,'Ej f�r n�ra sensor 2!','FontSize',22)
        %end
        
    case 4 % Grey calligraphy
              
        shading interp
                
        [X,Y,Z,C,paint]=drawer_my(xf,last_xf,tool(1));
        
        % Decide if the pen is close enough to the screen         
       
        if paint
            set(p_handle(1),'XData',X,'YData',Y,'ZData',Z,'CData',C)
        end
        
        %Don't apply too large magnetic field, it might harm the sensor
        %if norm(d_cell{1}(:,7:9))>3
        %    text(0,0,0,'Ej f�r n�ra sensor 1!','FontSize',22)
        %end
        %if norm(d_cell{2}(:,7:9))>3
        %    text(0,0,0,'Ej f�r n�ra sensor 2!','FontSize',22)
        %end
        
end % switch

% flush the graphics to screen
drawnow
% -------------------------------------------------------------------------
% end of function

function mt_figure_close(obj, eventdata, h, f_handle,fpIMU,fpXF)

if ~isempty(h) %Data comes from the COM-ports
    % local function to properly release the serial port object and the output file          
    fclose(h);        
end
if ~isempty(fpIMU) %Close the file if we write to it
    fclose(fpIMU);
end
if ~isempty(fpXF) %Close the file if we write to it
    fclose(fpXF);
end
% kill figure window as requested
delete(f_handle)
% -------------------------------------------------------------------------
% end of function




