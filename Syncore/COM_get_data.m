function [d, s,N] = COM_get_data(h,sensor)

    % [d,status]=COM_get_data(h);
    %
    % Input variables:
    %  h = handle COM object
    %
    % Output variables:
    % d = output data vector Size N*12
    % N = number of samples

    %Number of available samples
    N=floor(h.BytesAvailable/(4*4*3));
    %N=1;
    if N > 0
        s=1;
        %nr of uint32 values to read. N samples * 3 axis * 4 boards
        n=4*3*N;

        %read data from serial port
        a=uint32(fread(h,n,'uint32'));

        %extract channel-nr. contained in lower 8-bits of 32 bit data received.
        channels = bitand(a, 255);

        %extract adc-data contained in higher 24 bits of 32 bit data received
        data = typecast(a, 'int32');
        data = data/(2^8);        
        
        %reorder the data . The rows of r corresponds to the samples and the
        %columns to the 12 channels
        d=zeros(N,3*4);
                       
        %N
        for i=1:(3*4)            
            d(:,i)=data(channels == i-1,1);            
        end
        d=double(d);

    else       
        s=0;
        d=zeros(0,12);
    end
    A = diag([1 1 1 -1 -1 1 -1 -1 1 1 1 1]);
    %A=-blkdiag([0 1 0;1 0 0;0 0 -1],[0 -1 0;-1 0 0;0 0 -1],[0 -1 0;-1 0 0;0 0 -1],[0 1 0;1 0 0;0 0 -1]);
    d=(A*d')';
    
    % Rescale the data
    d=d/sensor.bits*(sensor.range(2)-sensor.range(1));

end

