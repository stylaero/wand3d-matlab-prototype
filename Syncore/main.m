% Run this file to define the parameters and un the demo!!
clear all


%% Sensor specific parameters

% The positions of the sensosrs, positive x - up, positive y - right, positive z into the screen. Origin in the center of the screen. (we could change this coordinate system, but this was how the sensors where aligned)
th1=[-0.152 ;0.088 ;0]; % These sensors are deployed behind the screen
th2=[0.152 ;0.088 ;0];    
th3=[0.152 ;-0.088 ;0];
th4=[-0.152 ;-0.088 ;0];
network.th=[th1,th2, th3, th4]; 
    for i=1:4
        network.A(:,:,i)=eye(3);        
    end
    
sensor.SF=220; %The sampling frequency
sensor.bits=2^24; %Number of bits
sensor.range=[-5.6 5.6]; %The range in Guass

%% Target specific parameters

K=1; %Number of tools
for k=1:K
    tool(k).dipole_strength=2.15e-4; % A upper limit of the strength for the magnetic dipole
    % These are only for vizualization...
    tool(k).length=0.14; %Length of the pen
    tool(k).screen_touch=0.1; %Max distance from screen for painting
    tool(k).thickness=0.009; %Thickness of the pen
    tool(k).cal_width = 0.01; %Maximum width of the calligraphy pen
end



%% Tuning parameter
tuning.Q_acc=0.1; %Standard diviation for the process noise beeing the acceleration (m/s^2)
tuning.Q_ori=1; %Standard diviation for the process noise beeing the angular velocity (rad/s)
tuning.Q_gain=0.0001; %Standard diviation for the process noise on the sensor gain (Nominal value of the gain = 1, a change of 0.01 corresponds to ca 0.5 degrees Celsius)
tuning.tracking_volume=[-1 1 -1 1 0 1]'; %[XMIN XMAX YMIN YMAX ZMIN ZMAX] for the tracking volume of intrest, here a box infont of the screen.

% Parameters for adjusting the process noise dependent on residual.
tuning.residual_level=[0.05 0.2];
tuning.residual_level=[0.003 0.2];
tuning.noise_scaling=[4^(-2) 1 4^2];


%% Run the demo!!
port = '20120928T152849'; 
saveData=0; %0: Do not save data. 1: Do save data
mode_data=1; %0: Read data from file 1: Read data from sensor
calibrate_startup=1; %0: Do not calibrate, use old calibration. 1: Calibrate
port='COM3';

%Run the demo in real time using data from the sensors
str_date=DisplayRealtimeData(port,tool,network,sensor,tuning,saveData,mode_data,calibrate_startup);

% These are the commands!!
% "Q" = Quit
% "Z" = Toggle Zoom in/out
% "M" = View only magnetometer data
% "T" = Pose tracker
% "O" = Color calligraphy
% "G" = Grey calligraphy
% "C" = Clear screen
% "D" = View all magnetometers (default)