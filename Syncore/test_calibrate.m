Y=untitled;
load meas_zero;
meas_zero=mean(Y(1:300,:));
R=cov(Y(1:300,:));



    %% Motion model
    
    % x_{n+1} = A*x_n + B*w_n, where w_n ~ N(0,Q) describes the dynamics of the target
    %
    % Assume a (nearly) constant velocity model fo the position and a (nearly) constant position model for the orientation
    % This gives the following entries, see for example 
    %
    % [3] F. Gustafsson. Statistical Sensor Fusion. Studentlitteratur, 1 edition, 2010.
    SF=300;
    T=1/SF;
    m.A= blkdiag([eye(3) T*eye(3);zeros(3,3) eye(3)],eye(3));
    m.B= blkdiag([(T^2/2)*eye(3);T*eye(3)],T*eye(3));
    m.Q= blkdiag(tuning.Q_acc^2*eye(3), (tuning.Q_ori*tool(1).dipole_strength)^2*eye(3));
       
    
    % Initial guess of the state    
    m.x0=[10;10;10;zeros(3,1);zeros(3,1)]; %Use zero velocity and zero dipole moment as initial state. Use initial position far awy from the network
    %m.x0=[10;10;10;zeros(3,1);0.001*ones(3,1)]; %Use zero velocity and zero dipole moment as initial state. Use initial position far awy from the network    
    m.xP0=diag([1 1 1 0 0 0 (tool(1).dipole_strength)^2*ones(1,3)]);  
    
    % Include the temperature dependent sensor gain as a state
    tuning.Q_gain=0;
    m.A= blkdiag(m.A,1);
    m.B= blkdiag(m.B,T*1);
    m.Q= blkdiag(m.Q,tuning.Q_gain^2);
    
    % Include the temperature dependent sensor gain as a state
    m.x0=[m.x0;1]; %Use the nominal value 1 as initial state.
    m.xP0=blkdiag(m.xP0, 0^2);
    
    % Measuremen noise
    sensor.R=R;
    m.Qs=1;
    
    %%
    tool(1).dipole_strength=600;
    for i=1:4
        network.A(:,:,i)=eye(3);        
    end
    network.A(:,:,1)=1.1*eye(3);        
    
    
    Nstart=1300;
    %%
    %for i=1:4
    %    network.A(:,:,i)=network.A(:,:,i)*(1+0.1*randn);        
    %end
    %%
    for k=1:30
        xp=m.x0;
        Pp=m.xP0;
        xf=xp;
        Pf=Pp;
        yf=meas_zero;
        N=size(Y,1);
        % To the filter step!!
        XF=zeros(N,10);
        YF=zeros(N,12);
        
        for n=1:N
            [xf,Pf,yf]=measurement_update(sensor,Y(n,:)',xp,Pp,tool,tuning,network, meas_zero);   
            [xp,Pp]=time_update(m,xf,Pf,tuning);
            XF(n,:)=xf';
            YF(n,:)=yf';
        end
        %%
        % Subtract the offset from filtered and real measurement
        YFn=YF-XF(:,end)*meas_zero;
        Yn=Y-XF(:,end)*meas_zero;
        sqrt(mean((YFn(Nstart:N,:)-Yn(Nstart:N,:)).^2,1))
        sqrt(mean(mean((YFn(Nstart:N,:)-Yn(Nstart:N,:)).^2,1)))



        %% Compute the dipole strength
        %Nstart=1200;
        %m_norm=sqrt(sum(XF(:,7:9).^2,2));
        %plot(m_norm)
        %m_norm=m_norm/mean(m_norm(Nstart:N));
        XFn=XF;
        %for i=1:3
        %    XFn(:,6+i)=XF(:,6+i)./m_norm; %Normalize with the dipole strength
        %end

        Ybar=zeros(N,12);
        for n=1:N
            x= XFn(n,:)';
            ybar=h(x,network,1,zeros(1,12));
            %ybar=h(x,network,1,meas_zero);
            Ybar(n,:)=ybar';
        end

                %% Plot all residuals
        for i=1:12
            subplot(4,3,i)
            plot(Yn(:,i),'b')
            hold all
            plot(YFn(:,i),'r')                        
            plot(Ybar(:,i),'g') 
        end



        %%
        
        Ybar2=zeros(N-Nstart+1,12);
        for i=1:4
            y_reg=Yn(Nstart:end,(1:3)+3*(i-1))';
            y_reg=y_reg(:);
            y_bar=Ybar(Nstart:end,(1:3)+3*(i-1));
            A_reg=kron(y_bar,eye(3));
            b=A_reg\y_reg;
            Yt=A_reg*b;            
            Ybar2(:,(1:3)+3*(i-1))=reshape(Yt,3,N-Nstart+1)';
            reshape(b,3,3);
            A(:,:,i)=network.A(:,:,i)*reshape(b,3,3);            
            scale(i)=trace(A(:,:,i))/3;
            %sensors.A(:,:,i)=A;
            q=eye(3);
            Yt=A_reg*q(:);            
            Ybar3(:,(1:3)+3*(i-1))=reshape(Yt,3,N-Nstart+1)';
        end
        tool(1).dipole_strength=tool(1).dipole_strength*mean(scale)
        for i=1:4
            A(:,:,i)=A(:,:,i)/mean(scale);
            A(:,:,i)
            network.A(:,:,i)=A(:,:,i);
        end
    sqrt(mean(mean((Ybar2-Yn(Nstart:N,:)).^2,1)))    
        for n=Nstart:N
            x= XFn(n,:)';
            ybar=h(x,network,1,zeros(1,12));
            %ybar=h(x,network,1,meas_zero);
            Ybar4(n-Nstart+1,:)=ybar';
        end
        

    end
    

        %% Plot all residuals
        close all
        for i=1:12
            subplot(4,3,i)
            %plot(Yn(Nstart:end,i)-Ybar(Nstart:end,i),'r')
            plot(Yn(Nstart:end,i),'b')
            hold all        
            plot(Ybar(Nstart:end,i),'r')
            
            plot(Ybar2(:,i),'c')
            plot(Ybar4(:,i),'g')
        end
        figure;
        for i=1:12
            subplot(4,3,i)
            %plot(Yn(Nstart:end,i)-Ybar(Nstart:end,i),'r')
            plot(Yn(Nstart:end,i)-Ybar(Nstart:end,i),'r')            
            hold on
            plot(Yn(Nstart:end,i)-Ybar2(:,i),'c')
        end
    

    
    

        
    
    
    