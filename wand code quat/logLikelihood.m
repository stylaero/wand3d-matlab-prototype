function p=logLikelihood(y,yp,Sinv)
    %P=LOGLIKLIHOOD  Computes N(y;yp,S) where S=Sinv^-1
    n=length(y);
    p=-log(det(Sinv)^(1/2))-1/2*(y-yp)'*Sinv*(y-yp);
end