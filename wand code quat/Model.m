classdef Model
    %MODEL Contains the model of the target(s)
    
    % We use 9 components state-vector for each tool. x(1:3) encodes the position of the
    % tool, x(4:6) the velocity, x(7:9) the orientation. Here the
    % orientation is encoded with the dipole vector m. The last state
    % x(end) the sensor gain mainly depending on the temperature. Thus, in
    % total the state dimension will be 9*K+1, where K is the number of
    % tools.
     
    
    properties                
        A           % System matric
        B           % Input matrix
        Q           % Process noise covariance
        Qs          % Scale parameter fo the process noise
        x0          % initial state
        xP0         % Initial state covariance
        network     % Sensor network struct
        K           % Number of targets 
        meas_zero   % Background field
        R           % Measurement noise covariance
        tool        % The tool information
        nx          % State dim for each target
    end
    
    methods
        function obj = Model(sensor,tool,network,meas_zero,tuning,R)
            % Constructor
            
            %% Set motion model specific parameters
            % x_{n+1} = A*x_n + B*w_n, where w_n ~ N(0,Q) describes the dynamics of the tool
            %
            % Assume a (nearly) constant velocity model fo the position and a (nearly) constant position model for the orientation
            % This gives the following entries, see for example 
            %
            % [3] F. Gustafsson. Statistical Sensor Fusion. Studentlitteratur, 1 edition, 2010.
            q = @(x) x(7:10);
            w = @(x) x(11:13);
            nx = 13;
            obj.nx = nx;
            
            T=1/sensor.SF; %The sampling time                    
            A= @(x,N) blkdiag([eye(3) T*N*eye(3);zeros(3,3) eye(3)],[eye(4), (T*N/2)*Sq2(q(x));zeros(3,4), eye(3)]);
            B= @(x,N) sqrt(T*N)*blkdiag(T*N*eye(3),eye(3),T*N/2*Sq2(q(x)),eye(3));
            Qd = [1/3 1/2;1/2 1];
            Q= blkdiag(tuning.Q_acc^2*kron(Qd,eye(3)), tuning.Q_ori^2*kron(Qd,eye(3)));
            
            % Initial guess of the state        
            q0 = randn(1,4);
            q0 = q0/norm(q0);
            x0=[10;10;10;zeros(3,1);tool(1).dipole_strength^(1/2)*[1 0 0 0]';zeros(3,1)]; %Use zero velocity and zero dipole moment as initial state. Use initial position far awy from the network    
            xP0=diag([10 10 10 0 0 0 (tool(1).dipole_strength^(1/2))^2*ones(1,4) 0 0 0]);
            
            obj.A = A;
            obj.B = B;
            obj.Q = Q;
            obj.x0 = x0;
            obj.xP0 = xP0;
            
            % Only for multiple tools-------
            K=length(tool); %Number of tools
            for k = 2:K
                obj.A = @(x,N) blkdiag(obj.A(x(1:(nx*(k-1))),N),A(x((1:nx)+(k-1)*nx),N));
                obj.B = @(x,N) blkdiag(obj.B(x(1:(nx*(k-1))),N),B(x((1:nx)+(k-1)*nx),N));
                obj.Q = blkdiag(obj.Q,Q);
                obj.x0=[obj.x0;x0];
                obj.xP0=blkdiag(obj.xP0,xP0);
            end
            
            % Include the temperature dependent sensor gain as a state
            obj.A= @(x,N) blkdiag(obj.A(x,N),1);
            obj.B= @(x,N) blkdiag(obj.B(x,N),T*N*1);
            obj.Q= blkdiag(obj.Q,tuning.Q_gain^2);
            
            % Include the temperature dependent sensor gain as a state
            obj.x0=[obj.x0;1]; %Use the nominal value 1 as initial state.
            obj.xP0=blkdiag(obj.xP0, 0.001^2);
            
            % Adaptive process noise
            obj.Qs=1;
            
            
            %% Set sensor model specific parameters
            obj.network=network;
            obj.K=K;
            obj.meas_zero=meas_zero;
            obj.R=R; %Add measurement noise to the model
            obj.tool = tool;
            
            
        end
        
        function [y,J]=h(obj,x)
       
            % Computes the measurement and its jacobian of the dipole model

            % y         - The measurement
            % J         - The Jacobian (with respect to x)
            % x         - The state
            % network   - Struct consisting of sensor stuff
            % K         - Number of tools
            % meas_zero - Thes static magentic field


            % Sensor Model

            % y_n = h(x_n) + e_n, where e_n ~ N(0,R) describes the sensor modeling, 
            % where y_k is the measurement, x_k the state of the system and
            % e_n measurment noise

            % We model the tool as a magnetic dipole. Therefor, here we define the dipole model. This models the magnetic field as position r
            % relative to a magnetic dipole with dipole moment m. For reference see
            % for example 
            %
            % [1] J. D. Jackson. Classical Electrodynamics. John Wiley and Sons, Inc., 2 edition, 1975.
            %
            % or more related to this application
            %
            %[T1] N. Wahlstr�m, Target Tracking using Maxwell's Equations. Master's Thesis. Presented June 15, 2010.    

            % We use 9 components state-vector for each tool. x(1:3) encodes the position of the
            % tool, x(4:6) the velocity, x(7:9) the orientation. Here the
            % orientation is encoded with the dipole vector m. The last state
            % x(end) the sensor gain mainly depending on the temperature. Thus, in
            % total the state dimension will be 9*K+1, where K is the number of
            % tools.

            % Using spatially distributed network we will measure the magnetic
            % field at diffeent positions relative to the dipole. Since the sensor
            % i is located at th(i,:), the relative positions will be
            % x(1:3)-th(:,i). This gives the following sensor model:

            th=obj.network.th;
            nx = obj.nx;
            A=obj.network.A;
            K=obj.K;
            meas_zero=obj.meas_zero;
            N_imu=size(th,2);
            y=zeros(3*N_imu,1);
            J=zeros(3*N_imu,nx*K+1);
            for k=1:K %Iterate over the number of tools                
                q=x((7:10)+nx*(k-1)); %Extract the quaternion for tool k 
                q2 = q'*q;
                Qout = q*q';
                q3 = q2^(-3/2);
                Q = Qq(q); % The corresponding rotation matrix                
                for i=1:N_imu % Iterate over sensors
                    for l = 1:length(obj.tool(k).dipole) % Iterate over number of dipoles in tool k                                                                
                        m0 = obj.tool(k).dipole(l).ori;
                        pos = obj.tool(k).dipole(l).pos;
                        
                        %Compute all terms that are needed (such that we don't compute them
                        %multiple times
                        
                        r=x((1:3)+nx*(k-1))-th(:,i) + Q*pos/q2;
                        R=r*r';
                        r2=r'*r;
                        r5=r2^(-5/2);

                        % The the derivative of the dipole model with respect to the magnetic
                        % dipole moment m

                        Jm= (3*R-r2*eye(3))*r5;

                        % The magnetic dipole moment                    
                        m = Q*m0;
                        %m=m2*norm(q)^2;            
                        y(3*(i-1)+(1:3)) = y(3*(i-1)+(1:3))+A(:,:,i)*Jm*m;%quatrotate(q',[0 0 -m])';

                        if nargout ==2 %If the Jacobian is an outbut, compute it as well
                            % The the derivative of the didipole model with respect to the position r
                            %Jr= 3*((r'*m)*eye(3)+(r*m')+(m*r')-5*((r'*m)/(r'*r))*r*r')*(r'*r)^(-5/2);
                            RM=r*m';
                            rm=r'*m;
                            Jr= 3*(rm*eye(3)+RM+RM'-5*rm/r2*R)*r5;
                            [Q0, Q1, Q2, Q3 ] = dQqdq(q);
                                                        
                            Jq = Jm*[Q0*m0, Q1*m0, Q2*m0, Q3*m0]; % Compute d(Jm*R(q)*m)/dq    
                            Jq_pos = Jr*[Q0*pos, Q1*pos, Q2*pos, Q3*pos]*(q2*eye(4)-Qout)/q2^2; % Compute d(Jm(q)*R*m)/dq
                            Jq = Jq + Jq_pos;% Compute d(Jm*R(q)*m)/dq

                            % The Jaccobian (with respect to x=[r,v,q]
                            J(3*(i-1)+(1:3),(1:nx)+nx*(k-1))= J(3*(i-1)+(1:3),(1:nx)+nx*(k-1)) + [Jr, zeros(3,3), Jq, zeros(3,3)];
                        end
                    end
                end
            end    
            %% Add the gain as a state    

            y=(y+meas_zero'); %Add the static magnetic field to the measurement
            if nargout==2 %If the Jacobian is an output, compute it as well
                J=x(nx*K+1)*J; %Multiply with the gain...
                J(:,nx*K+1)=y; %.. and augment the last column corresponding to derivative of the measurement with respect to the gain.
            end
            y=x(nx*K+1)*y; %Multiply the measurement with the gain
        end    
    end
end


