function S=Sq2(q)
% The matrix S(q) with for dot q = 1/2 S(q)w, where w is given in global
% coordinates
   q0=q(1);   q1=q(2);   q2=q(3);   q3=q(4);
   S=[-q1 -q2 -q3;
       q0  q3 -q2;
      -q3  q0  q1;
       q2 -q1  q0];
end
