function [xf,Pf,yf,yPf,res,p]=measurement_update(model,yk,xp,Pp,tuning,tool)
       % Implements a standard measurement update in an Extended kalman filter, here with a linear
       % dynamics and a couple of projections steps for increasing the
       % robustnes of the estimation, for reference see for example 
       % [3] F. Gustafsson. Statistical Sensor Fusion. Studentlitteratur, 1 edition, 2010.
       
       % model - Model object
       % yk -measurement
       % yf -measurement, filter
       % yPf -measurement, filter covariance       

       % xp - state, prediction
       % xp - state, prediction covariance
       
       % xf - state, filter
       % Pf - state, filter covariance
       
       % res - residual
       
       
       % Evaluation
       R=model.R; %Measurement noise       
       
       % Linearization
       %C=numgrad(@(x) model.h(x),1,xp); %Compute the Jacobian numerically
       
       %% Measurement update       
       [yphat,C]=model.h(xp); %Compute the predicted measurement and Jacobian       
       S=C*Pp*C'+R;
       Kg=Pp*C'*inv(S);
       
       % State
       xf=xp+Kg*(yk-yphat);
       
       Pf=Pp-Kg*C*Pp;
       Pf=0.5*(Pf+Pf');
       
       
       %% Projection step
       % Thise steps ae not included in the standard EKF but will decrease
       % the risk of divergence of the filter for this particulary setup
       
       % Make a projection of the magnetic dipole moment onto a sphere.                                          
       
       for k=1:1
           %norm(xf((7:9)+9*(k-1)))
           %if norm(xf((7:9)+9*(k-1))) > tool(k).dipole_strength
           %     xf((7:9)+9*(k-1))=xf((7:9)+9*(k-1))*tool(k).dipole_strength/norm(xf((7:9)+9*(k-1)));
           %end
       end
       
              
       % Project the position of the magnetic dipole onto a the tracking
       % volume of intrest, here given by a box
       
       lim=tuning.tracking_volume;
       nx = model.nx;
       for k=1:model.K             
               if lim(1) > xf(1+nx*(k-1)) || lim(2) < xf(1+nx*(k-1)) || lim(3) > xf(2+nx*(k-1)) || lim(4) < xf(2+nx*(k-1)) || lim(5) > xf(3+nx*(k-1)) || lim(6) < xf(3+nx*(k-1))
                   % Project the position onto the box.
                   xf((1:3)+nx*(k-1))=min(max(xf((1:3)+nx*(k-1)),lim([1 3 5])),lim([2 4 6]));
                   xf((4:6)+nx*(k-1))=zeros(3,1); %Set velocity to zero if on the boundery of the box.           
                   xf((11:13)+nx*(k-1))=zeros(3,1); %Set velocity to zero if on the boundery of the box.           
               end
               
               % If dipole strength too strong, project it back
               if norm(xf((7:10)+nx*(k-1))) > sqrt(tool(k).dipole_strength)
                   xf((7:10)+nx*(k-1)) = xf((7:10)+nx*(k-1))/norm(xf((7:10)+nx*(k-1)))*sqrt(tool(k).dipole_strength);
               end
               
               % If orientation on the wrong side, project it back
               if norm(xf([8 9]+nx*(k-1))) > norm(xf([7 10]+nx*(k-1)))
                   no = norm(xf((7:10)+nx*(k-1)));
                   xf([7 10]+nx*(k-1)) = xf([7 10]+nx*(k-1))*norm(xf([8 9]+nx*(k-1)))/norm(xf([7 10]+nx*(k-1)));                   
                   xf((7:10)+nx*(k-1)) = xf((7:10)+nx*(k-1))/norm(xf((7:10)+nx*(k-1)))*no;
               end
           %xf(7:8+9*(k-1))=[0.0000001;-0.0000001];
       end
       
       %Measurement
       yf=model.h(xf);

       %meas_zero(2)
       yPf=C*Pf*C'+R;
       res=norm(yk-yf);
       Sinv = inv(S);
       p = logLikelihood(yk,yf,Sinv);
       
       
end
