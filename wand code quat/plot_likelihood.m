function [X,Y,Y_ort,Y_ort2] = plot_likelihood(m,q_ref,q_est,x,y,m0)
   % plot the LH around the multimodal 6th DF.
   b = sum([m.tool.dipole(:).ori],2);
   m0 = b'/norm(b);
   
   %W=0;
   % for i = 1:size(m,2)        
   %     W = W  + m.tool.dipole(i).pos*m.tool.dipole(i).ori' + m.tool.dipole(i).ori*m.tool.dipole(i).pos';
   % end
   %p =  1/(b'*b)^2*(b'*b*eye(3)-b*b'/2)*W*b;
   x = fminunc(@(x) -logLikelihood(y,m.h(x),inv(m.R)),x);
   q_est = x(7:10)'/norm(x(7:10));
   %q_est = q_ref;
   
   % Rotate the dipole moment to global coordinates
   m1 = quatrotate(quatconj(q_ref),m0);
   m1_ort = [m1(2) -m1(1) 0];
   m1_ort = m1_ort/norm(m1_ort);
   m1_ort2 = cross(m1,m1_ort);
   %q_rot_est = quatmultiply(q_est,quatconj(q_ref));   
   alpha = 0:1:360;
   for i = 1:length(alpha)
       % Rotate alpha degrees around m1
       q2 = rot_m(alpha(i),m1,q_est);
       q2_ort = rot_m(alpha(i),m1_ort,q_est);
       q2_ort2 = rot_m(alpha(i),m1_ort2,q_est);
                            
       % Compute the likelihood for this alpha
       x2 = x;       
       x2(7:10) = q2'*norm(x(7:10));       
       Y(i) = -logLikelihood(y,m.h(x2),inv(m.R));
       
       % Compute the likelihood for first angle in orthogonal direction
       x2_ort = x;
       x2_ort(7:10) = q2_ort'*norm(x(7:10));
       Y_ort(i) = -logLikelihood(y,m.h(x2_ort),inv(m.R));
       
       % Compute the likelihood for second angle in orthogonal direction
       x2_ort2 = x;
       x2_ort2(7:10) = q2_ort2'*norm(x(7:10));              
       Y_ort2(i) = -logLikelihood(y,m.h(x2_ort2),inv(m.R));
       X(i) = alpha(i);
       
       % Compute the distance to the true q
       %ang_delta(i) = 2*acos(abs(sum(q2.*q_est,2)));
   end
   %[y,ind] = min(ang_delta);
   %y*180/pi
   figure;
   semilogy(X,Y);
   hold all
   semilogy(X,Y_ort);
   semilogy(X,Y_ort2);
   legend('last ortientation','another orientation')
   
   %hold on
   %al = alpha(ind);
   %plot(alpha(ind),Y(ind),'r*');
   %hold off
end

function [q2] = rot_m(alpha,m1,q)
       q_rot = [cosd(alpha/2) sind(alpha/2)*m1];
       %p_rot = quatrotate(q_rot,p)
       q2 = quatmultiply(q_rot,q);
end