close all
% New run
visualize('exp17/exp17_xf.csv','exp17/exp17_meas.csv','exp17/exp17_vicon','exp17/sensor_board_vicon','digital_setup',[21 75],'Exp17')

% A plain EKF
visualize('exp17/exp17_xf_no.csv','exp17/exp17_meas.csv','exp17/exp17_vicon','exp17/sensor_board_vicon','digital_setup',[21 75],'Exp17')
visualize('exp18/exp18_xf_no.csv','exp18/exp18_meas.csv','exp18/exp18_vicon','exp18/sensor_board_vicon','digital_setup',[20 100],'Exp18')
visualize('exp19/exp19_xf_no.csv','exp19/exp19_meas.csv','exp19/exp19_vicon','exp19/sensor_board_vicon','digital_setup',[24 124],'Exp19')

% Using a FB
visualize('exp17/exp17_xf_FB.csv','exp17/exp17_meas.csv','exp17/exp17_vicon','exp17/sensor_board_vicon','digital_setup',[21 75],'Exp17')
visualize('exp18/exp18_xf_FB.csv','exp18/exp18_meas.csv','exp18/exp18_vicon','exp18/sensor_board_vicon','digital_setup',[20 100],'Exp18')
visualize('exp19/exp19_xf_FB.csv','exp19/exp19_meas.csv','exp19/exp19_vicon','exp19/sensor_board_vicon','digital_setup',[24 124],'Exp19')


% Projecting it onto the upwards half
visualize('exp17/exp17_xf_half.csv','exp17/exp17_meas.csv','exp17/exp17_vicon','exp17/sensor_board_vicon','digital_setup',[21 75],'Exp17')
visualize('exp18/exp18_xf_half.csv','exp18/exp18_meas.csv','exp18/exp18_vicon','exp18/sensor_board_vicon','digital_setup',[20 100],'Exp18')
visualize('exp19/exp19_xf_half.csv','exp19/exp19_meas.csv','exp19/exp19_vicon','exp19/sensor_board_vicon','digital_setup',[24 124],'Exp19')


