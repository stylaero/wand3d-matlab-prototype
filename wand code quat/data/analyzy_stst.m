clear all
close all
str_file='digital_stat';
xf=load([str_file,'_xf.csv']);
meas=load([str_file,'_meas.csv']);
load([str_file,'_meas_zero'])
xf2=xf(1100:end,:);
meas2=meas(1100:end,:);

%% FFT of xf
N=size(xf2,1);
t=0:(N-1);
freq=(0:(N-1))/N*220;

x_error=xf2-repmat(mean(xf2),N,1);
pos_error=x_error(:,1:3);
ori_error=x_error(:,7:9);



figure
plot(freq,abs(fft(pos_error)))
title('FFT of estimated position')

%% FFT of y
N=size(meas2,1);
t=0:(N-1);
freq=(0:(N-1))/N*220;

y_error=meas2-repmat(mean(meas2),N,1);



figure
freq=(0:(N-1))/N*220;
plot(freq,abs(fft(y_error(:,3))))
title('FFT of the measurement')

break;


%% Save modified y
y_stat=repmat(mean(meas2),length(meas),1);
y_mod=y_stat+randn(size(meas))*chol(R);
dumpfile = sprintf([str_file,'_mod_meas.csv']);
fp = fopen(dumpfile, 'w');
for i=1:length(meas)
              str='%d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d';  
              if i<1000
                y=meas(i,:);
              else
                  y=y_mod(i,:);
              end
              fprintf(fp, [str,' \r\n'],y);
end
fclose(fp);
save([str_file,'_mod_meas_zero.mat'],'meas_zero','R')
