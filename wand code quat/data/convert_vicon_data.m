function [xbn_n,q_nb,q_vn,q_vb]=convert_vicon_data(DATA,DATA_board)
    % b - body fram (the magnet)
    % n - navigation frame  (the magnetometer network)
    % v - vicon frame
    %
    % xij_k - The position from origin of frame i to origin of frame j,
    % given in coordinates of frame k
    
    % Orientation and position of n-frame
    euler_vn=pi/180*DATA_board(1,1:3); % Magnetometer network stationary    
    q_vn=angle2quat(euler_vn(1),euler_vn(2),euler_vn(3),'ZYX');           
    xnv_v=DATA_board(1,4:6);
    
    
    % Orientation and position of b-frame
    euler_vb=pi/180*DATA(:,2:4);
    q_vb=angle2quat(euler_vb(:,1),euler_vb(:,2),euler_vb(:,3),'ZYX');        
    xbv_v=DATA(:,5:7);

    % Orientation of b-frame wrt n-frame        
    %q_nb=quatmultiply(q_vb,quatinv(q_vn));
    q_nb=quatmultiply(quatinv(q_vn),q_vb);

    xbn_v = xbv_v-repmat(xnv_v,size(xbv_v,1),1); % The vector between frame frame n to frame b, givien in coordinates of v
    xbn_n=quatrotate(q_vn,xbn_v);
    xbn_n=xbn_n/1000; % use meter
end