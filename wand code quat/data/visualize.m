function visualize(str_est,str_meas,str_vicon,str_sensorboard,str_setup,syncInt,titleStr)
    % str1 - Name of the data
    % str2 - Name of sensor network
    % syncInt - Time interval to syncronize
    saveIm=0;
    saveDat = 0;
    %close all
    load(str_setup)
    
    f_mag=sensor.SF; %Frequency of the magnetometer network
    f_vicon=100; %Frequency of the system

    % Load estimated data
    x_est=load(str_est);    

    % Load reference data
    load(str_vicon);
    load(str_sensorboard);
    
    % Load measurements
    meas=load(str_meas);
    
    % Load the model used
    load('model_150603')           
    
    % Convert reference data
    [xbn_n,q_nb]=convert_vicon_data(dataViconSynced,sensor_board_vicon);
    
    % Resample the reference data
    U=f_mag/gcd(f_mag,f_vicon);
    D=f_vicon/gcd(f_mag,f_vicon);
    xbn_n = resample_vec(xbn_n,U,D);   
    q_nb = resample_vec(q_nb,U,D);   
    q_nb = q_nb./repmat(sqrt(sum(q_nb.^2,2)),1,4);

    % The move the origen of the sensor platform down to the
    xbn_n(:,3)=xbn_n(:,3)+0.021; %  (9mm - 3mm) + 2 mm + 13mm
    xbn_n=xbn_n+repmat([-0.0018    0.0010    0.0046],size(xbn_n,1),1); % Correct for calibrated position
    
    % Compute the position of the tool in n-frame
    xtb_b = [0.0648, 0.045, -0.013+0.0205];
    xtb_b = xtb_b + 1e-3*[-0.8066   -0.4555   -0.5493]; % Correct for calibrated position
    xtb_n = quatrotate(quatconj(q_nb),xtb_b);
    %pos_rel = quatrotate(q_nb,pos_mag);
    xtn_n = xbn_n + xtb_n;
    q_tn = q_nb;
     
    
    % Remove the second when the sensor network was calibrating
    %xtn_n=xtn_n(1*f_mag:end,:);        
    %q_tn=q_tn(1*f_mag:end,:);
    
    
    
    % Data length of the estimated data
    Nest=size(x_est,1);

    % Data length of the reference data
    Nref=size(xtn_n,1);

    % Augment the data
    x_ref=[xtn_n,q_tn,(1/f_mag)*(0:(Nref-1))'];
    x_est=[x_est,meas,(1/f_mag)*(0:(Nest-1))'];

    % Remove beginning and end
    %x_ref=x_ref(2500:(end-1200),:);
    %x_est=x_est(2500:(end-1200),:);

    % Do syncronization
    data_in{1}=x_ref;
    data_in{2}=x_est;    
    arI={[1 2],[1 2]}; % Syncronize for position
    data_out=synchronize_all_nodes(data_in,arI,5,syncInt);

    % Extract data
    x_ref=data_out{1}(:,1:(end-1));
    x_est=data_out{2}(:,1:(end-1));
    t=data_out{1}(:,end);
    
    % Cut away end and beginning
    if ~isempty(syncInt)
        ind = find(t > syncInt(1) & t < syncInt(2));
        x_ref = x_ref(ind,:);
        x_est = x_est(ind,:);        
        t = t(ind);
    end
    
    %Extract position
    pos_ref=x_ref(:,1:3);
    pos_est=x_est(:,1:3);
    meas=x_est(:,(end-11):end); % Extract measurement
    x_est = x_est(:,1:(end-12));
    
        
    %Extract orientation
    q_ref = x_ref(:,4:7);
    q_est = x_est(:,7:10);
    q_est = q_est./repmat(sqrt(sum(q_est.^2,2)),1,4);
    ang_delta = 2*acos(abs(sum(q_est.*q_ref,2)));
    m_b = [1 1 0];
    m_b = m_b/norm(m_b);
    m_n_ref = quatrotate(quatconj(q_ref),m_b);
    m_n_est = quatrotate(quatconj(q_est),m_b);
    ang_m_delta = acos(sum(m_n_est.*m_n_ref,2));
    
    if size(x_est,2) > 20
        q_est1 = x_est(:,(7:10)+14);
        q_est1 = q_est1./repmat(sqrt(sum(q_est1.^2,2)),1,4);
        q_est2 = x_est(:,(7:10)+14*2);
        q_est2 = q_est2./repmat(sqrt(sum(q_est2.^2,2)),1,4);
        ang_delta_12 = 2*acos(abs(sum(q_est1.*q_est2,2)));
        figure;
        plot(t,ang_delta_12);
        title('Likelihood')
        xlabel('Time [s]')
        figure;
        plot(t,x_est(:,(end-1):end))
        title('Likelihood')
    end
    
    
    % Convert to euler angles
    [r1,r2,r3] = quat2angle(q_ref,'ZYX');
    ang_ref = [r1 r2 r3];
    [r1,r2,r3] = quat2angle(q_est,'ZYX');
    ang_est = [r1 r2 r3];
            

    % Data length
    N=length(t);
    
    %% Visualize the result!
    
    % Plot the positions
    figure;
    plot(t,pos_ref,'linewidth',1)
    hold on
    %plot(t,pos_est,'linewidth',1)    
    plot(t,pos_est,':','linewidth',1)
    xlabel('Time [s]')
    ylabel('Position [m]')
    legend('x-position, VICON','y-position, VICON','z-position, VICON','x-position, 3DWand','y-position, 3DWand','z-position, 3DWand')
    title([titleStr,' - Position'])
    grid on
    %xlim([38 48])
    if saveIm==1
        print('-depsc', ['figures/',str1,'_pos.eps'])
        system(['epstopdf figures/',str1,'_pos.eps'])
    end
    
    if saveDat == 1
        dataSave = [t pos_ref pos_est];
        dataSave = dataSave(1:10:end,:);
        save([titleStr,'_pos.dat'],'-ascii','dataSave')
        th = network.th';
        save([titleStr,'_th.dat'],'-ascii','th')
    end
               
    %% Visualize likelihood
    m0 = [1 1 0];
    Y_aug = [];
    al_aug = [];
    ind = 4120;%1:10:length(t);    
    for i = 1:length(ind)
        figure(5);
        [X,Y,Y_ort,Y_ort2] = plot_likelihood(m,q_ref(ind(i),:),q_est(ind(i),:),x_est(ind(i),1:14)',meas(ind(i),:)',m0);
        %Y_aug(i,:) = Y;
        %al_aug(i,:) = al;           
    end
    if 1
        dataSave = [X', Y', Y_ort', Y_ort2'];        
        save(['likelihood.dat'],'-ascii','dataSave')
    end
    %%
    %figure;
    %imagesc(Y_aug(1:1200,:)')
    %hold on
    %plot(al_aug','k*')
    %%    
        
    %% Plot the trajectory
    figure;
    plot3(pos_ref(:,1),pos_ref(:,2),pos_ref(:,3));
    hold all
    plot3(pos_est(:,1),pos_est(:,2),pos_est(:,3),'Linewidth',2);    
    
    
    
    
   
    for i=1:size(network.th,2)
        plot3(network.th(1,i),network.th(2,i),network.th(3,i),'k.','Markersize',15)
    end
    legend('Vicon','3DWand','Sensors')

    grid on
    axis equal
    xlabel('x-position [m]')
    ylabel('y-position [m]')
    zlabel('z-position [m]')
    %title([titleStr,' - Trajectory'])
    
    if saveIm==1
        %title('Trajectory')
        legend('Estimated trajectory','True trajectory','Magnetometers','location','NorthWest')
        print('-depsc', ['figures/',str1,'_traj.eps'])        
        system(['epstopdf figures/',str1,'_traj.eps'])
        matlab2tikz(['figures/',str1,'_traj.tikz'], 'width', '\figurewidth', 'height', '\figureheight');        
    end
    
         %% Plot orientation
    figure;
    plot(t,ang_ref*180/pi,'linewidth',1)
    hold on
    plot(t,ang_est*180/pi,':','linewidth',1)    
    xlabel('Time [s]')
    ylabel('[Degree]')
    legend('Rz, VICON','Ry, VICON','Rx, VICON','Rz, 3DWand','Ry, 3DWand','Rx, 3DWand')
    title([titleStr,' - Orientation'])
    
    if saveIm==1
        print('-depsc', ['figures/',str1,'_ori.eps'])
        system(['epstopdf figures/',str1,'_ori.eps'])
    end
   if saveDat == 1
        dataSave = [t ang_ref*180/pi ang_est*180/pi];
        dataSave = dataSave(1:10:end,:);
        save([titleStr,'_ori.dat'],'-ascii','dataSave')
    end
    
     %% Plot orientation error
    figure;
    plot(t,ang_delta*180/pi,'linewidth',1)   
    hold all
    plot(t,ang_m_delta*180/pi,'linewidth',1)    
    xlabel('Time [s]')
    ylabel('[Degree]')
    legend('Between q_ref and q_est','Between m_ref and m_est')
    title([titleStr,' - Orientation'])
    
    if saveIm==1
        print('-depsc', ['figures/',str1,'_ori.eps'])
        system(['epstopdf figures/',str1,'_ori.eps'])
    end
    if saveDat == 1
        dataSave = [t ang_delta*180/pi ang_m_delta*180/pi];
        dataSave = dataSave(1:10:end,:);
        save([titleStr,'_ori_error.dat'],'-ascii','dataSave')
    end
     
    %return;
    %%
    for j=1:3 % Iterate over position and orientation
        switch j
            case 1            
                x_est=1000*pos_est;
                x_ref=1000*pos_ref;
                str_x='pos';
                str_x2='position';
                str_unit='mm';
            case 2
                x_est=180/pi*ang_delta;
                x_ref=0*ang_delta;
                str_x='ori';
                str_x2='orientation';
                str_unit='degree';
            
            case 3
                x_est=180/pi*ang_m_delta;
                x_ref=0*ang_m_delta;
                str_x='ori';
                str_x2='orientation';
                str_unit='degree';
        end
        
        
        %% RMSE as a function of time
        
        x_error=x_est-x_ref; % Compute the error
        MSE=sum((x_error).^2,2);
        RMSE=sqrt(MSE);
        if 0 % Don't plot this
            figure;
            plot(t,RMSE)
            title([titleStr,' - RMSE of ',str_x2,' - function of time'])
            xlabel('Time [s]')
            ylabel(['RMSE [',str_unit,']'])
        end

        %% Compute Bias and variance

        % Make a low pass filter with half the nyqvist frequency
        cutoff=10; % cutoff frequency
        Wn=cutoff/(f_mag/2);
        %Wn=0.1;
        [B,A] = butter(4,Wn);

        % Filter each axis
        x_error_low=zeros(size(x_est,1),size(x_error,2));          
        for i=1:size(x_error,2)
            x_error_low(:,i)=filtfilt(B,A,x_error(:,i));
        end
        
        x_error_high = x_error - x_error_low;
        

        %% Compute the variance multiply with fraction with is within [0 Wn]
        x_est_n=sum((x_error_high).^2,2)*(1/(1-Wn));        

        % Compute the bias^2 
        x_est_bias=max(MSE-x_est_n,eps);
        x_est_bias=sum((x_error_low).^2,2)-x_est_n*Wn;

        % Remove the begining and the end
        I=floor(1/20*N):floor(19/20*N);

        disp(['Bias [',str_unit,']'])
        bias=sqrt(mean(x_est_bias(I)));
        disp(bias)
        disp(['sqrt(Var) [',str_unit,']'])
        v=sqrt(mean(x_est_n(I)));
        disp(v)
        disp(['RMSE [',str_unit,']'])
        RMSE2=sqrt(mean(MSE(I)));
        disp(RMSE2)
        
        if j==1
            %disp([num2str(RMSE2,'%.2f'),' & ',num2str(bias,'%.2f'),' & ',num2str(v,'%.2f'),' \\'])
            disp(['Position & \SI{',num2str(RMSE2,'%.2f'),'}{mm} & \SI{',num2str(bias,'%.2f'),'}{mm} & \SI{',num2str(v,'%.2f'),'}{mm}  \\'])            
        else
            %disp([num2str(RMSE2,'%.2f'),'$^{\circ}$ & ',num2str(bias,'%.2f'),'$^{\circ}$ & ',num2str(v,'%.2f'),'$^{\circ}$ \\'])
            disp(['Orientation & ',num2str(RMSE2,'%.2f'),'$^{\circ}$ & ', num2str(bias,'%.2f'),'$^{\circ}$ & ',num2str(v,'%.2f'),'$^{\circ}$ \\']) 
        end
            
        if 0 % Don't plot this
            %% RMSE as a function of distance
            figure(10+3*j);

            dist=sqrt(sum(pos_ref.^2,2));
            loglog(dist,RMSE)
            title([titleStr,' - RMSE of ',str_x2,' - function of distance'])
            xlabel('Distance to center of sensor network [m]')
            ylabel(['RMSE [',str_unit,']'])
            hold all
            if saveIm==1
                print('-depsc', ['figures/',str1,'_',str_x,'_rmse.eps'])
                system(['epstopdf figures/',str1,'_',str_x,'_rmse.eps'])
            end

        
            %% Bias as a function of distance
            figure(11+3*j);


            semilogy(dist,sqrt(x_est_bias))
            title([titleStr,' - Bias of ',str_x2])
            xlabel('Distance to center of sensor network [m]')
            ylabel(['Bias [',str_unit,']'])        
            hold all
            if saveIm==1
                print('-depsc', ['figures/',str1,'_',str_x,'_bias.eps'])
                system(['epstopdf figures/',str1,'_',str_x,'_bias.eps'])
            end

            %% Var as a function of distance
            figure(12+3*j);


            semilogy(dist,sqrt(x_est_n))
            title([titleStr,' - Standard deviation of ',str_x2])
            xlabel('Distance to center of sensor network [m]')
            ylabel(['sqrt(Variance) [',str_unit,']'])
            hold all
            if saveIm==1
                print('-depsc', ['figures/','_',str_x,str1,'_std.eps'])
                system(['epstopdf figures/',str1,'_',str_x,'_std.eps'])            
            end
        end
    end   
    
end
