function p=innovationLikelihood(y,yp,Sinv)
    %P=INNOVATIONLIKLIHOOD  Computes N(y;yp,S) where S=Sinv^-1
    n=length(y);
    p=(1/((2*pi)^(n/2))*det(Sinv)^(1/2))*exp(-1/2*(y-yp)'*Sinv*(y-yp));
end