function str_date=DisplayRealTimeData(port,tool,network,sensor,tuning,saveData,mode_data,calibrate_startup,visualize,first_plot)

%% DisplayRealTimeData(port);
%
%
% Input argument
% port -        The COM-port or the file from where the data comes
% tool -        Target/tool specific parameters
% network -     The geometry of the sensor network
% sensor -      Sensor specific parameters
% tuning -      Tuning parameters
% saveData -    0: Do not save data. 
%               1: save estimated state to file (and measured data if we
%               read from COM-port).
% mode_data     0: Read data from file. 1: Read data from COM-port
% 
%
%
% Press key on keyboard while running:
%
% "Q" = Quit
% "Z" = Toggle Zoom in/out
% "D" = View all magnetometer data (default).  
%       Blue - measured magnetic field. 
%       Green - estimated magnetic field.
% "M" = View magnetometer + estimate.
%       Column 1: Measurements, Blue  - sensor 1, Red   - sensor 2, 
%                               Green - sensor 3, Black - sensor 4
%       Column 2: Position, Blue  - x, Red - y, Green - z
%       Column 3: Velocity, Blue  - x, Red - y, Green - z
%       Column 4: Dipole moment, Black
%                 Blue  - x, Red - y, Green - z, Black, norm of dipole
%                 moment (should be farily constant if the same dipole is used)
% "T" = Pose tracker - Pose is visualized with a bar in a 3D plot
% "O" = Color calligraphy
% "G" = Grey calligraphy
% "C" = Clear screen
%
% Commands used in the lic presentation:
%
% "1" = Measurement from sensor 1
% "2" = Measurement from all sensors
% "3" = Tracking, audience erspective
% "4" = Measurement from sensor 1 (two-dimensions)
% "5" = Rotation of measurement trajectory, used for heading direction
% classification
% "6" = ---"---- (filled area)


%
% Orientation data output MODE:
% "Q" = Quit 

% set default values
defaultValues={'caldata',2,1.0,1,1.0};
[DisplayMode,zoom_level_setting,filterSettings_gain,filterSettings_corr,filterSettings_weight]...
   =deal(defaultValues{:});

h=[]; %Allocate space

try
    % use try-catch to avoid the need to shutdown MATLAB if m-function fails (e.g. during debugging)
    % This is needed because MATLAB does not properly release COM-objects when
    % the m-function fails/crasehes and the object is not explicitly released with "fclose(h)"
    
    fpIMU=[];
    fpXF=[];
    
    if saveData==1
        if mode_data==1
            %% Output File - Measurement
            % Create an output file and open it
            str_date=datestr(now(), 30);
            dumpfile = sprintf('data/%s_meas.csv', str_date);
            fpIMU = fopen(dumpfile, 'w');
            if fpIMU < 0
                error('Could not open output file.');
            end
            
            %% Output File - State
            %Create an output file and open it  
            
            dumpfile2 = sprintf('data/%s_xf.csv', str_date);
            fpXF = fopen(dumpfile2, 'w');
            if fpXF < 0                
                error('Could not open output file.');
            end
            
        else
            %% Output File - State
            % Create an output file and open it  
            dumpfile2 = sprintf('data/%s_xf.csv', port);
            fpXF = fopen(dumpfile2, 'w');
            if fpXF < 0                
                error('Could not open output file.');
            end
            
        end
    end
    % init figure plotting variables
    % set time scale zoom level
    zoom_levels=[12*sensor.SF,8*sensor.SF,4*sensor.SF]; % in seconds
    zoom_level=zoom_levels(zoom_level_setting);
    OldFigureUserData = [0 0 5 0]; status = 0; last_t=0;   
    ResetFigure =1; first_time=1;
    
    % create figure
    % NOTE, the releasing of MTObj is done in the Figure Close Request function
    % "mt_figure_close"
    [f_handle, p_handle] = mt_create_figure(OldFigureUserData(3), -1 ,h, sensor,...
        zoom_level, OldFigureUserData,network.th,fpIMU,fpXF);
        
    % set figure UserData to appropriate values 
    %(format [if_being_plotted, Zoom, PlotType, if_reset])
    set(f_handle,'UserData', [0 0 first_plot 0])         
    set(f_handle,'Name','Display','WindowStyle', 'modal','NumberTitle','off','MenuBar','none','units','normalized','outerposition',[0 0 1 1])
   

    %% Initialize the hardwere/input files
    if mode_data==0; %Input is a file name
        DATA=load(['data/',port,'_meas.csv']);    
        % Number of sensors
        N_sensor=size(DATA,2)/3; %Each sensor has 3 measurement

    else %Input is a vector of COM-ports    
        % Number of sensors
        N_sensor=4;

        %open serial object
        h = serial(port);
        
        % Set parameters used for communication with the sensor
        h.BaudRate = 115200*4;
        h.InputBufferSize = 10000*3*4*4; % Have 500 samples in the buffer (each sample comes with 32 bits)
        h.timeout = 100;
        h.flowcontrol = 'none';
        fopen(h);
        fwrite(h,'0','uchar');  
        fwrite(h,'3','uchar');  
    end           
    
    %for i=1:N_sensor                        
    %        d_cell{i}=zeros(1,3);
    %        last_d_cell{i} = d_cell{i};            
    %end
    last_meas=zeros(1,3*N_sensor);
            
    
    %Compute the sacle factor of the sensor data
    %sensor.scale_factor=1;
    
    %% Do the calibration
    str_date='no captured data';
    if mode_data==1 %Data comes from the COM-ports
        if calibrate_startup==1
            %Wait a while
            pause(1)
            %% Compute the stationary offset for sensor (due to earth magnetic field)
            meas=COM_get_data(h,sensor); % retreive data from serial object
            meas=meas(max(1, (end-100)):end,:); %Remove the first 100 samples (usually defected)
            
            N=size(meas,1); %Number of samples in the calibration
            
            meas_zero=sum(meas,1)/N; %Compute the offset for all sensors
            
            y=(meas-repmat(meas_zero,N,1));
            R=y'*y/N; %Compute the covariance matrix of the measurement noise.
            ind=find(diag(R)==0);
            for i=1:length(ind)
                R(i,i)=max(max(diag(R))*1000,1);               
            end
            
            
            save('data/meas_zero.mat','meas_zero','R');
        else
            % Load calibration data
            load(['data/',port,'_meas_zero.mat'])
        end        
        
        if saveData==1            
            save(['data/',str_date,'_meas_zero.mat'],'meas_zero','R');
        end
    else
        load(['data/',port,'_meas_zero.mat'])

        if saveData==1   % Save state if saveData==1 (both if measurement comes from sensors as well as file!=            
            save(['data/',str_date,'_meas_zero.mat'],'meas_zero','R');
        end      
    end
        
    %% Do other initializations    
    
    % Initialize the model
    m=Model(sensor,tool,network,meas_zero,tuning,R);
    
    % Initialize filter stuff
    xp=m.x0;
    Pp=m.xP0;
    xf=xp;
    Pf=Pp;
    yf=meas_zero;
    
    %Initialize time duration for a certain plot
    t_acc=0;

     
    tic %Start counting time
    TIME=0; %Starting time
        
    %% Strart the main loop
    while ishandle(f_handle) % check if all objects are alive
        
        %% Get the data. 
        if mode_data==1 % If the data comes from the sensor
                [meas,status,N]=COM_get_data(h,sensor); 
                % meas  is on the form [mag0_x mag0_y mag0_z mag1_x mag1_y mag1_z mag2_x mag2_y mag2_z mag3_x mag3_y mag3_z]
                
                
                if status
                    if saveData==1 % Save measured data if that option is active
                        for n=1:N %Iterate over all samples and save them to a file
                            str='%d, %d, %d';
                            str_acc='';
                            data_acc=[];
                            for i=1:N_sensor
                                str_acc=[str_acc,str];
                                if i < N_sensor
                                    str_acc=[str_acc,', '];
                                end                                                                
                            end
                            % Do the print command
                            fprintf(fpIMU, [str_acc,' \r\n'],double(meas(n,:)));
                        end
                    end
                end
                
        else % If the data comes from a file
            if visualize==1 % If we visualize, diplay in normal speed, the new time stap will be the one according to the internal clock
                TIME_new=toc;
            else % If we get data from file, the new time step will be the
                TIME_new=TIME + 1/sensor.SF; %Otherwise do one sample
            end
            % Create a time indeces
            IND=((floor(TIME*sensor.SF)+1):floor(TIME_new*sensor.SF));
            
            % Set the time to the new time
            TIME=TIME_new;
            if max(IND)>size(DATA,1);
                break;
            end
            
            % Extract the measurements
            meas=DATA(IND,:);
            N=length(IND);
        end
        
        %% Now the data is available! Proceed if there are samples (i.e. N > 0)                
        if N > 0, % If there are samples to process, go further
            %% Filter
            last_xf=xf;
            last_yf=yf;
            CurrentFigureUserData = get(f_handle,'UserData');
            if (CurrentFigureUserData(3) == 0)&&(CurrentFigureUserData(3) == 1)&&(CurrentFigureUserData(3) == 2)&&(CurrentFigureUserData(3) == 9) % If pose tracker display, process all samples
                iter_ar=1:N;
            else
                iter_ar=1:N;
            end
            for n=iter_ar %Do a filter update for all samples (optionally we could make update only for the last sample if it takes too long time to process all)
                
                % create measurement
                y=meas(n,:)';
                
                % To the filter step!!                               
                if sum(diff(y)~=0) >= 6 % If two sensors sends data, do filtering                    
                    [xf,Pf,yf]=measurement_update(m,y,xp,Pp,tuning,tool);
                    [xp,Pp]=time_update(m,xf,Pf,tuning,1);
                end
                
                if saveData==1 % Save to file
                    str='%d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d';                            
                    fprintf(fpXF, [str,' \r\n'],xf);                        
                end
            end 
            
            %% Visualize
                if visualize==1
            

                    % retrieve values from figure
                    CurrentFigureUserData = get(f_handle,'UserData'); 

                    % create timebase        
                    t=[(last_t+(1:N)/sensor.SF)];
                    last_t=t(end);
                    T=t(end)-t(1);
                    t_acc=t_acc+T;

                    if ResetFigure ==1, % check if figure should be reset
                        last_t=0; % wrap plot around
                        figureUserData = get(f_handle,'UserData');
                        if first_time == 0
                            for jj = 1:length(p_handle)
                                %clearpoints(p_handle(jj));
                            end
                        end
                        % call local function to (re)setup figure
                        [f_handle, p_handle, a_handle] = mt_create_figure(CurrentFigureUserData(3), f_handle,h, sensor, zoom_level, figureUserData,network,fpIMU,fpXF);
                        ResetFigure = 0;
                        t_acc=10;
                    end

                    % check if figures should be reset                           
                    if any(CurrentFigureUserData(1:3) ~= OldFigureUserData(1:3)), % check if figure UserData changed by KeyPress
                        ResetFigure =1; % make sure plot is reset                        
                        first_time =1; % re-initialize zoom levels too

                    elseif ((last_t*sensor.SF>zoom_level)&&((CurrentFigureUserData(3) == 0)||(CurrentFigureUserData(3) == 1)||(CurrentFigureUserData(3) == 5)||(CurrentFigureUserData(3) == 6)||(CurrentFigureUserData(3) == 7)||(CurrentFigureUserData(3) == 8)||(CurrentFigureUserData(3) == 10)))|| (first_time==1) || (CurrentFigureUserData(4)==1)
                        ResetFigure =1; % make sure plot is reset
                        first_time =0;
                        tmp=get(f_handle,'UserData');
                        set(f_handle,'UserData',[tmp(1:3) 0]);
                        t_acc = mt_plot_data(meas, last_meas, t, CurrentFigureUserData, f_handle,p_handle,a_handle,xf,last_xf,yf,last_yf,meas_zero,t_acc,tool);
                    else % other wise --> plot
                        % plot the data using a local funtion                
                        t_acc = mt_plot_data(meas, last_meas, t, CurrentFigureUserData, f_handle,p_handle,a_handle,xf,last_xf,yf,last_yf,meas_zero,t_acc,tool);
                    end

                    %Save values for next iteration
                    OldFigureUserData = CurrentFigureUserData;                    
                    last_meas=meas(end,:);                    
                    status=zeros(1,3);
                end

            elseif status>1,
                % MTObj not correctly configured, stopping
                [str_out]=MT_return_error(status);
                disp(str_out);
                disp('MTObj not correctly configured, stopping.....');
                break
            end % if        

    end % while

    % release MTObj is done on figure close...not here
    if ishandle(f_handle), % check to make sure figure is not already gone
        close(f_handle)
    end

catch % try catch for debugging
    % make sure serial port object is released even on error
    if ~isempty(h) %Data comes from the COM-ports
        fclose(h)
    end
    % display some information for tracing errors
    disp('Error was catched by try-catch.....MTobj released')
    crashInfo = lasterror; % retrieve last error message from MATLAB
    disp('Line:')
    crashInfo.stack.line
    disp('Name:')
    crashInfo.stack.name
    disp('File:')
    crashInfo.stack.file
    rethrow(lasterror)
end
% -------------------------------------------------------------------------
% end of function MT_DisplayRealtimeData(varargin);

%% -------------------------------------------------------------------------
% LOCAL FUNCTIONS
% -------------------------------------------------------------------------
function [f_handle, p_handle, a_handle] = mt_create_figure(type, f_handle, h, sensor, zoom_level, figureUserData, network ,fpIMU, fpXF)

% local function to create the figure for real time plotting of data.
% accepts plot type information for custom plots
%
% if figure does not yet exist enter -1 in figure_handle input

if ~ishandle(f_handle),
    f_handle = figure('Name','Real-time display of MTi or MTx data','NumberTitle','off');
end

fontSizeUsed = 16;
% init
%p_handle = zeros(12); a_handle = zeros(12);

switch type
    case 0% magnetic data (default)        
        if figureUserData(2)==0
           a=[-0.01 0.01];
        else
           a=sort(sensor.range);         
        end
        
        YLims = [ a; a; a; a];     
        for i=1:12,
            subplot(3,4,i), p_handle(i)=plot(0,0,'EraseMode','none');a_handle(i) = gca; axis(a_handle(i),[0 (zoom_level+1)./sensor.SF YLims(mod(i-1,4)+1,:)]);
            grid on;
            if figureUserData(2)==1
                set(gca,'YTick',-10:1:10);
            end
        end
        tlh = title(a_handle(1),['Magnetometer1']);   set(tlh,'FontSize',fontSizeUsed);   xlh = xlabel(a_handle(9),'time [s]');  ylh = ylabel(a_handle(1),'X_S, [Gauss]');
        tlh = title(a_handle(2),['Magnetometer2']);   set(tlh,'FontSize',fontSizeUsed);   xlh = xlabel(a_handle(10),'time [s]');  ylh = ylabel(a_handle(5),'Y_S, [Gauss]');
        tlh = title(a_handle(3),['Magnetometer3']);   set(tlh,'FontSize',fontSizeUsed);   xlh = xlabel(a_handle(11),'time [s]');  ylh = ylabel(a_handle(9),'Z_S, [Gauss]');
        tlh = title(a_handle(4),['Magnetometer4']);   set(tlh,'FontSize',fontSizeUsed);   xlh = xlabel(a_handle(12),'time [s]');                 
        
    case 1 % magnetic data + estimated states                
        YLims = [-10 10; -0.7 0.7; -1 1; -5e-3 5e-3];
        for i=1:12
            subplot(3,4,i), p_handle(i)=plot(0,0,'EraseMode','none');a_handle(i) = gca; axis(a_handle(i),[0 (zoom_level+1)./sensor.SF YLims(mod(i-1,4)+1,:)]); grid on; 
        end
        
        tlh = title(a_handle(1),['Angular velocity']);   set(tlh,'FontSize',fontSizeUsed);   xlh = xlabel(a_handle(9),'time [s]');   ylh = ylabel(a_handle(1),'X_S');
        tlh = title(a_handle(2),['Position']);              set(tlh,'FontSize',fontSizeUsed);   xlh = xlabel(a_handle(10),'time [s]');  ylh = ylabel(a_handle(5),'Y_S');
        tlh = title(a_handle(3),['Velocity']);              set(tlh,'FontSize',fontSizeUsed);   xlh = xlabel(a_handle(11),'time [s]');  ylh = ylabel(a_handle(9),'Z_S');
        tlh = title(a_handle(4),['Quaternion']);         set(tlh,'FontSize',fontSizeUsed);   xlh = xlabel(a_handle(12),'time [s]');
           
    case 2 % Plot pose tracker
             YLims = [-1 1];
        
            subplot(1,1,1); p_handle(1)=surf(zeros(2,2),zeros(2,2),zeros(2,2),'EraseMode','none');%hold on;plot3(th(1),th(2),th(3),'b*','EraseMode','none');plot3(-th(1),-th(2),-th(3),'r*','EraseMode','none');
            a_handle = gca; axis(a_handle,[YLims YLims YLims]);axis off 
            
            %Fix the position and angle of the camera
            pos_t=[0,0,20];
            campos(pos_t);
            ang=get(a_handle,'CameraViewAngle');
            %camva(ang/11.5);
            camva(ang/2);
            set(a_handle,'CameraUpVector',[0 1 0])
            %set(f_handle,'units','normalized','outerposition',[0 0 1 1])
            
            %Make a grid
            grid off;
            tick=-0.20:0.1:0.20;
            set(a_handle,'XTick',tick);set(a_handle(1),'YTick',tick);set(a_handle,'ZTick',tick);
            
            %Labels
            xlabel(a_handle,'X_S');
            ylabel(a_handle,'Y_S');
            zlabel(a_handle,'Z_S');
            
            %The colormap
            %colormap([0.8 0.8 0.8;0 1 0])
            colormap([0.8 0.8 0.8;1 0 0;1 0 0])            
            caxis([0 1])
                 
    case 3  %Color calligraphy
            YLims = [-0.7 0.7];
            
            subplot(1,1,1); 
            p_handle(1)=surf(zeros(2,2),zeros(2,2),zeros(2,2),'EraseMode','none');
            a_handle(1) = gca; axis(a_handle(1),[YLims YLims YLims]); 
            
            %Fix the position and angle of the camera
            pos_t=[0,0,8];
            campos(pos_t);
            ang=get(a_handle(1),'CameraViewAngle');
            camva(ang/11.5);
            set(a_handle(1),'CameraUpVector',[0 1 0])
            set(f_handle,'units','normalized','outerposition',[0 0 1 1])
            
            %Make grid
            grid on;
            tick=-0.20:0.1:0.20;
            set(a_handle(1),'XTick',tick);set(a_handle(1),'YTick',tick);set(a_handle(1),'ZTick',tick);
            
            %Lables
            xlabel(a_handle(1),'X_S');
            ylabel(a_handle(1),'Y_S');
            zlabel(a_handle(1),'Z_S');
            tlh = title(a_handle(1),['Pose tracker']); set(tlh,'FontSize',fontSizeUsed);           
            
            %Colormap
            shading interp 
            colormap(hsv(128))
            caxis([0 1])

            
   case 4 % Grey calligraphy
            YLims = [-0.7 0.7];
            
            subplot(1,1,1); p_handle(1)=surf(zeros(2,2),zeros(2,2),zeros(2,2),'EraseMode','none');
            a_handle(1) = gca; axis(a_handle(1),[YLims YLims YLims]); 
            
            %Fix the position and angle of the camera
            pos_t=[0,0,8];
            campos(pos_t);
            ang=get(a_handle(1),'CameraViewAngle');
            camva(ang/11.5);
            set(a_handle(1),'CameraUpVector',[0 1 0])
            set(f_handle,'units','normalized','outerposition',[0 0 1 1])
            
            %Make grid
            grid on;
            tick=-0.20:0.1:0.20;
            set(a_handle(1),'XTick',tick);set(a_handle(1),'YTick',tick);set(a_handle(1),'ZTick',tick);
            
            %Labels
            xlabel(a_handle(1),'X_S');
            ylabel(a_handle(1),'Y_S');
            zlabel(a_handle(1),'Z_S');
            tlh = title(a_handle(1),['Pose tracker']); set(tlh,'FontSize',fontSizeUsed);           
            
            %Colormap
            shading interp            
            colormap(gray(128))
            
    case 5% one sensor
            if figureUserData(2)==0
               a=[-0.1 0.1];
            else
               a=sort(sensor.range);         
            end

            YLims = [ a; a; a; a];     
            for i=1:3,
                subplot(3,1,i), p_handle(i)=animatedline('Color','b','LineWidth',2);
                %clearpoints(p_handle(i));
                %p_handle(i)=plot(0,0,'EraseMode','none');
                %p_handle(i)
                a_handle(i) = gca; set(a_handle(i),'FontSize',fontSizeUsed); axis(a_handle(i),[0 (zoom_level+1)./sensor.SF YLims(mod(i-1,4)+1,:)]);
                grid on;
                if figureUserData(2)==1
                    set(gca,'YTick',-10:1:10);
                end
            end
            tlh = title(a_handle(1),['Magnetometer 1']);   set(tlh,'FontSize',fontSizeUsed);   xlh = xlabel(a_handle(3),'Time [s]');  ylh = ylabel(a_handle(1),'X_S, [Gauss]');
            ylh = ylabel(a_handle(2),'Y_S, [Gauss]');
            ylh = ylabel(a_handle(3),'Z_S, [Gauss]');
            %tlh = title(a_handle(2),['Magnetometer2']);   set(tlh,'FontSize',fontSizeUsed);   xlh = xlabel(a_handle(10),'time [s]');  ylh = ylabel(a_handle(5),'Y_S, [Gauss]');
            %tlh = title(a_handle(3),['Magnetometer3']);   set(tlh,'FontSize',fontSizeUsed);   xlh = xlabel(a_handle(11),'time [s]');  ylh = ylabel(a_handle(9),'Z_S, [Gauss]');
            %tlh = title(a_handle(4),['Magnetometer4']);   set(tlh,'FontSize',fontSizeUsed);   xlh = xlabel(a_handle(12),'time [s]');
            
            
    case 6% multiple sensor mode
        if figureUserData(2)==0
           a=[-0.1 0.1];
        else
           a=sort(sensor.range);         
        end
        
        YLims = [ a; a; a; a];     
        for i=1:12,
            subplot(3,4,i), p_handle(i)=animatedline('Color','b','LineWidth',2);%p_handle(i)=plot(0,0,'EraseMode','none');
            a_handle(i) = gca; set(a_handle(i),'FontSize',fontSizeUsed); axis(a_handle(i),[0 (zoom_level+1)./sensor.SF YLims(mod(i-1,4)+1,:)]);
            grid on;            
            if figureUserData(2)==1
                set(gca,'YTick',-10:1:10);
            end
        end
        tlh = title(a_handle(1),['Magnetometer 1']);   set(tlh,'FontSize',fontSizeUsed);   xlh = xlabel(a_handle(9),'Time [s]');  ylh = ylabel(a_handle(1),'X_S, [Gauss]');
        tlh = title(a_handle(2),['Magnetometer 2']);   set(tlh,'FontSize',fontSizeUsed);   xlh = xlabel(a_handle(10),'Time [s]');  ylh = ylabel(a_handle(5),'Y_S, [Gauss]');
        tlh = title(a_handle(3),['Magnetometer 3']);   set(tlh,'FontSize',fontSizeUsed);   xlh = xlabel(a_handle(11),'Time [s]');  ylh = ylabel(a_handle(9),'Z_S, [Gauss]');
        tlh = title(a_handle(4),['Magnetometer 4']);   set(tlh,'FontSize',fontSizeUsed);   xlh = xlabel(a_handle(12),'Time [s]');   
      
    case 7% Paper B illustration
            if figureUserData(2)==0
               a=[-0.3 0.3];
            else
               a=sort(sensor.range);         
            end

            YLims = [ a; a; a; a];     
            for i=1:2,
                subplot(2,5,(i-1)*5+1), p_handle(i)=plot(0,0,'EraseMode','none');a_handle(i) = gca; set(a_handle(i),'FontSize',fontSizeUsed); axis(a_handle(i),[0 (zoom_level+1)./sensor.SF YLims(mod(i-1,4)+1,:)]);
                grid on;
                if figureUserData(2)==1
                    set(gca,'YTick',-10:1:10);
                end
            end
            tlh = title(a_handle(1),['Magnetometer 1']);   set(tlh,'FontSize',fontSizeUsed);   xlh = xlabel(a_handle(2),'Time [s]');  ylh = ylabel(a_handle(1),'X_S, [Gauss]');
            ylh = ylabel(a_handle(2),'Y_S, [Gauss]');
            %ylh = ylabel(a_handle(3),'Z_S, [Gauss]');
            
            %subplot(2,5,[2 3 4 6 7 8])
            subplot(2,5,[3 4 5 8 9 10])
            %subplot(2,5,[5 10])
            %subplot(3,4,[2 3 4 6 7 8 10 11 12])
            p_handle(3)=plot(0,0,'EraseMode','none');a_handle(3) = gca; axis equal; set(a_handle(3),'FontSize',fontSizeUsed); axis(a_handle(3),[YLims(1,:) YLims(1,:)]);
            grid on;
            xlh = xlabel(a_handle(3),'X_S, [Gauss]');  
            ylh = ylabel(a_handle(3),'Y_S, [Gauss]');
            %axis equal
            if figureUserData(2)==1
                set(gca,'YTick',-10:1:10);
            end
            
            tlh = title(a_handle(3),'Magnetometer 1');   
            set(tlh,'FontSize',fontSizeUsed);   
            xlh = xlabel(a_handle(3),'X_S, [Gauss]');  
            ylh = ylabel(a_handle(3),'Y_S, [Gauss]');
            %tlh = title(a_handle(2),['Magnetometer2']);   set(tlh,'FontSize',fontSizeUsed);   xlh = xlabel(a_handle(10),'time [s]');  ylh = ylabel(a_handle(5),'Y_S, [Gauss]');
            %tlh = title(a_handle(3),['Magnetometer3']);   set(tlh,'FontSize',fontSizeUsed);   xlh = xlabel(a_handle(11),'time [s]');  ylh = ylabel(a_handle(9),'Z_S, [Gauss]');
            %tlh = title(a_handle(4),['Magnetometer4']);   set(tlh,'FontSize',fontSizeUsed);   xlh = xlabel(a_handle(12),'time [s]'); 
            
    case 8% Paper B illustration 2
            if figureUserData(2)==0
               a=[-0.3 0.3];
            else
               a=sort(sensor.range);         
            end

            YLims = [ a; a; a; a];
           for i=1:2,
                subplot(2,5,(i-1)*5+1), p_handle(i)=plot(0,0,'EraseMode','none');a_handle(i) = gca; set(a_handle(i),'FontSize',fontSizeUsed); axis(a_handle(i),[0 (zoom_level+1)./sensor.SF YLims(mod(i-1,4)+1,:)]);
                grid on;
                if figureUserData(2)==1
                    set(gca,'YTick',-10:1:10);
                end
            end
            tlh = title(a_handle(1),['Magnetometer 1']);   set(tlh,'FontSize',fontSizeUsed);   xlh = xlabel(a_handle(2),'Time [s]');  ylh = ylabel(a_handle(1),'X_S, [Gauss]');
            ylh = ylabel(a_handle(2),'Y_S, [Gauss]');
            %ylh = ylabel(a_handle(3),'Z_S, [Gauss]');
            
            
            %subplot(3,4,[2 3 4 6 7 8 10 11 12])
            subplot(2,5,[3 4 5 8 9 10])
            p_handle(3)=patch(0,0,0,'EraseMode','none');a_handle(3) = gca; axis equal; set(a_handle(3),'FontSize',fontSizeUsed); axis(a_handle(3),[YLims(1,:) YLims(1,:)]);
            grid on;
            xlh = xlabel(a_handle(3),'X_S, [Gauss]');  
            ylh = ylabel(a_handle(3),'Y_S, [Gauss]');
            %axis equal
            %if figureUserData(2)==1
                set(gca,'XTick',a(1):((a(2)-a(1))/4):a(2)); 
                set(gca,'YTick',a(1):((a(2)-a(1))/4):a(2)); 
            %end
            
            tlh = title(a_handle(3),'Magnetometer 1');   
            set(tlh,'FontSize',fontSizeUsed);   
            colormap([1 0 0;0 0 1])
            
            %tlh = title(a_handle(2),['Magnetometer2']);   set(tlh,'FontSize',fontSizeUsed);   xlh = xlabel(a_handle(10),'time [s]');  ylh = ylabel(a_handle(5),'Y_S, [Gauss]');
            %tlh = title(a_handle(3),['Magnetometer3']);   set(tlh,'FontSize',fontSizeUsed);   xlh = xlabel(a_handle(11),'time [s]');  ylh = ylabel(a_handle(9),'Z_S, [Gauss]');
              %tlh = title(a_handle(4),['Magnetometer4']);   set(tlh,'FontSize',fontSizeUsed);   xlh = xlabel(a_handle(12),'time [s]');  
            
        case 9 % Plot pose tracker
            YLims = [-1 1];
            th=network.th;
            subplot(1,1,1); p_handle(1)=surf(zeros(2,2),zeros(2,2),zeros(2,2));%,'EraseMode','none');
            hold on;
            for j=1:size(th,2)
                plot3(th(1,j),th(2,j),th(3,j));
                %ANIMATEDLINE
            end
            hold off
            axis equal
            %;plot3(-th(1),-th(2),-th(3),'r*','EraseMode','none');
            a_handle = gca; axis(a_handle,[-0.5 0.5 -0.5 0.5 0 0.5]);%axis off 
            
            
            %Fix the position and angle of the camera
            pos_t=[3,-20,2];
            %pos_t=[0,20,20];
            campos(pos_t);
            ang=get(a_handle,'CameraViewAngle');
            camva(ang/1.3);
            %camva(ang/2);
            %set(a_handle,'CameraUpVector',[0 1 0])
            
            
            %Make a grid
            %grid off;
            %tick=-0.20:0.1:0.20;
            %set(a_handle,'XTick',tick);set(a_handle(1),'YTick',tick);set(a_handle,'ZTick',tick);
            
            %Labels
            xlabel(a_handle,'X_S [m]');
            ylabel(a_handle,'Y_S [m]');
            zlabel(a_handle,'Z_S [m]');
            
            %The colormap
            %colormap([0.8 0.8 0.8;0 1 0])
            %colormap([0.8 0.8 0.8;1 0 0])            
            colormap([1 0 0;0 1 0; 0.8 0.8 0.8])
            caxis([0 1])
            
        case 10 % one sensor - 2-axis
            if figureUserData(2)==0
               a=[-0.1 0.1];
            else
               a=sort(sensor.range);         
            end

            YLims = [ a; a; a; a];     
            for i=1:2,
                subplot(2,1,i), p_handle(i)=plot(0,0,'EraseMode','none');a_handle(i) = gca; set(a_handle(i),'FontSize',fontSizeUsed); axis(a_handle(i),[0 (zoom_level+1)./sensor.SF YLims(mod(i-1,4)+1,:)]);
                grid on;
                if figureUserData(2)==1
                    set(gca,'YTick',-10:1:10);
                end
            end
            tlh = title(a_handle(1),['Magnetometer 1']);   set(tlh,'FontSize',fontSizeUsed);   xlh = xlabel(a_handle(2),'Time [s]');  
            ylh = ylabel(a_handle(1),'X_S, [Gauss]');
            ylh = ylabel(a_handle(2),'Y_S, [Gauss]');
            %ylh = ylabel(a_handle(3),'Z_S, [Gauss]');
            %tlh = title(a_handle(2),['Magnetometer2']);   set(tlh,'FontSize',fontSizeUsed);   xlh = xlabel(a_handle(10),'time [s]');  ylh = ylabel(a_handle(5),'Y_S, [Gauss]');
            %tlh = title(a_handle(3),['Magnetometer3']);   set(tlh,'FontSize',fontSizeUsed);   xlh = xlabel(a_handle(11),'time [s]');  ylh = ylabel(a_handle(9),'Z_S, [Gauss]');
            %tlh = title(a_handle(4),['Magnetometer4']);   set(tlh,'FontSize',fontSizeUsed);   xlh = xlabel(a_handle(12),'time [s]');
            
            
            

end

% Set the fontsize of the axis handles
%for i=length(a_handle)
%    set(a_handle(i),'FontSize',fontSizeUsed);
%end

set(f_handle,'CloseRequestFcn',{@mt_figure_close,h,f_handle,fpIMU,fpXF});
set(f_handle,'KeyPressFcn',{@mt_figure_keypress,f_handle});
set(f_handle,'UserData', figureUserData);
% -------------------------------------------------------------------------
% end of function


%% -------------------------------------------------------------------------
function mt_figure_keypress(obj, eventdata, f_handle)

% local function to handle KeyPress events on figure.
% (D)efault display, (M)agnetometer only, Pose
% (T)racker, c(O)lor calligraphy, (G)rey calligraphy
%
% envoked when a key is pressed when figure is in focus

in_key=lower(get(f_handle,'CurrentCharacter'));
tmp = get(f_handle,'UserData');

switch in_key
    case 'z' % toggle zoom mode
        pause(0.2)
        figure(f_handle)
        if tmp(2) == 0, % check zoom level
            set(f_handle,'UserData',[tmp(1) 1 tmp(3) 0]); % toggle to next zoom mode
        else
            set(f_handle,'UserData',[tmp(1) 0 tmp(3) 0]); % toggle to default zoom mode
        end
        
    case 'd'
        disp('Switching the default display mode, all 12 data streams...')
        pause(0.2)
        figure(f_handle)
        set(f_handle,'UserData',[tmp(1:2) 0 0]); % set to default mode

    case 'm'
        disp('Switching to display 3D magnetometer + estimated data...')
        pause(0.2)
        figure(f_handle)
        set(f_handle,'UserData',[tmp(1:2) 1 0]); % set to defult mode

    case 't'
        disp('Switching to display the pose tracker')
        pause(0.2)
        figure(f_handle)
        set(f_handle,'UserData',[tmp(1:2) 2 0]); % set to pose tracker mode
       
     case 'o'
        disp('Switching to display the color calligraphy')
        pause(0.2)
        figure(f_handle)
        set(f_handle,'UserData',[tmp(1:2) 3 0]); % set to color calligraphy mode
  
     case 'g'
        disp('Switching to display the grey calligraphy')
        pause(0.2)
        figure(f_handle)
        set(f_handle,'UserData',[tmp(1:2) 4 0]); % set to color calligraphy mode 
               
    case 'c'
        disp('Clear graph')
        pause(0.2)
        set(f_handle,'UserData',[tmp(1:3) 1]); % set to mag mode
        
    case '1'
        disp('Switching to one sensor display')
        pause(0.2)
        figure(f_handle)
        set(f_handle,'UserData',[tmp(1:2) 5 0]); % set to one sensor mode
        
    case '2'
        disp('Switching to one sensor display')
        pause(0.2)
        figure(f_handle)
        set(f_handle,'UserData',[tmp(1:2) 6 0]); % set to multiple sensor
        
    case '3'
        disp('Switching to tracker presentation')
        pause(0.2)
        figure(f_handle)
        set(f_handle,'UserData',[tmp(1:2) 9 0]); % set to tracker presentation
        
    case '4'
        disp('Switching to one sensor display- 2-axis')
        pause(0.2)
        figure(f_handle)
        set(f_handle,'UserData',[tmp(1:2) 10 0]); % set to one sensor mode
        
    case '5'
        disp('Switching to paper B display')
        pause(0.2)
        figure(f_handle)
        set(f_handle,'UserData',[tmp(1:2) 7 0]); % set to  paper B display mode 
        
    case '6'
        disp('Switching to paper B display 2')
        pause(0.2)
        figure(f_handle)
        set(f_handle,'UserData',[tmp(1:2) 8 0]); % set to paper B display mode 2
        
        
    case 'q'
        disp('Quitting demo DisplayRealtimeData...')
        pause(0.2)
        figure(f_handle)
        close(f_handle)
 
    otherwise
        disp('Unknown command option....displaying help data')
        disp(' ')
        eval('help DisplayRealtimeData')

end

%% -------------------------------------------------------------------------
function t_acc = mt_plot_data(meas,last_meas, t, CurrentFigureUserData, f_handle,p_handle, a_handle, xf, last_xf,yf,last_yf,meas_zero,t_acc,tool)    
    
% local function to plot the data using "low-level" set fucntions for smooth plotting
    N_sensor=length(last_meas)/3;
    style={'-',':','--','-.'};
    col={'b','r','g','k'};
    tmp = get(f_handle,'UserData');
    
    K=(length(xf)-1)/9;
    nx = (length(xf)-1)/K;
    for k=1:K
        pos(:,k) = xf((1:3)+nx*(k-1));
        vel(:,k) = xf((4:6)+nx*(k-1));        
        mag(:,k) = xf((7:9)+nx*(k-1)); 
        w(:,k) = xf((10:12)+nx*(k-1)); 
        
        last_pos(:,k) = last_xf((1:3)+nx*(k-1));
        last_vel(:,k) = last_xf((4:6)+nx*(k-1));        
        last_mag(:,k) = xf((7:9)+nx*(k-1)); 
        last_w(:,k) = last_xf((10:12)+nx*(k-1)); 
    end
    
    switch CurrentFigureUserData(3) % check plot type
    
        case 0 %default - PLot all measured data
            
            
        if CurrentFigureUserData(2)==1
           meas_zero=zeros(size(meas_zero)); % Plot around true zero if zoomed out
        end
 
        %  plot the data
        for i=1:N_sensor
            %Plot the measurement
            set(p_handle(i),'XData',t,'YData',[last_meas(1+3*(i-1)) meas(:,1+3*(i-1))']-meas_zero(1+(i-1)*3),'Color','b','LineWidth',2)
            set(p_handle(4+i),'XData',t,'YData',[last_meas(2+3*(i-1)) meas(:,2+3*(i-1))']-meas_zero(2+(i-1)*3),'Color','b','LineWidth',2)
            set(p_handle(8+i),'XData',t,'YData',[last_meas(3+3*(i-1)) meas(:,3+3*(i-1))']-meas_zero(3+(i-1)*3),'Color','b','LineWidth',2)
            
            %Plot the filterd measurement from the model (they should
            %correspond to the real measurement if the model is good)
            set(p_handle(i),'XData',t([1 end]),'YData',[last_yf(1+3*(i-1)) yf(1+3*(i-1))]-meas_zero(1+(i-1)*3),'Color','g','LineWidth',2)
            set(p_handle(4+i),'XData',t([1 end]),'YData',[last_yf(2+3*(i-1)) yf(2+3*(i-1))]-meas_zero(2+(i-1)*3),'Color','g','LineWidth',2)
            set(p_handle(8+i),'XData',t([1 end]),'YData',[last_yf(3+3*(i-1)) yf(3+3*(i-1))]-meas_zero(3+(i-1)*3),'Color','g','LineWidth',2)
          
        end
        
        

    case 1 % Only magnetometers
   
        
        %     plot the data                
        %for i=1:N_sensor
        %    set(p_handle(1),'XData',t,'YData',[last_meas(1+3*(i-1)) meas(:,1+3*(i-1))']-meas_zero(1+(i-1)*3),'Color',col{i},'LineStyle',style{i},'LineWidth',2)
        %    set(p_handle(5),'XData',t,'YData',[last_meas(2+3*(i-1)) meas(:,2+3*(i-1))']-meas_zero(2+(i-1)*3),'Color',col{i},'LineStyle',style{i},'LineWidth',2)
        %    set(p_handle(9),'XData',t,'YData',[last_meas(3+3*(i-1)) meas(:,3+3*(i-1))']-meas_zero(3+(i-1)*3),'Color',col{i},'LineStyle',style{i},'LineWidth',2)
        %end
        K=(length(xf)-1)/9;
        for k=1:K
            set(p_handle(2),'XData',t([1 end]),'YData',[last_pos(1,k) pos(1,k)'],'Color','b','LineWidth',2)
            set(p_handle(6),'XData',t([1 end]),'YData',[last_pos(2,k) pos(2,k)'],'Color','g','LineWidth',2)
            set(p_handle(10),'XData',t([1 end]),'YData',[last_pos(3,k) pos(3,k)'],'Color','r','LineWidth',2)

            set(p_handle(3),'XData',t([1 end]),'YData',[last_vel(1,k) vel(1,k)'],'Color','b','LineWidth',2)
            set(p_handle(7),'XData',t([1 end]),'YData',[last_vel(2,k) vel(2,k)'],'Color','g','LineWidth',2)
            set(p_handle(11),'XData',t([1 end]),'YData',[last_vel(3,k) vel(3,k)'],'Color','r','LineWidth',2)

            set(p_handle(4),'XData',t([1 end]),'YData',[last_mag(1,k) mag(1,k)'],'Color','b','LineWidth',2)
            set(p_handle(4),'XData',t([1 end]),'YData',[norm(last_mag(:,k)) norm(mag(:,k))],'Color','k','LineWidth',2)
            set(p_handle(8),'XData',t([1 end]),'YData',[last_mag(2,k) mag(2,k)'],'Color','g','LineWidth',2)
            set(p_handle(12),'XData',t([1 end]),'YData',[last_mag(3,k) mag(3,k)'],'Color','r','LineWidth',2)
            
            set(p_handle(1),'XData',t([1 end]),'YData',[last_w(1,k) w(1,k)'],'Color','b','LineWidth',2)
            set(p_handle(5),'XData',t([1 end]),'YData',[last_w(2,k) w(2,k)'],'Color','g','LineWidth',2)
            set(p_handle(9),'XData',t([1 end]),'YData',[last_w(3,k) w(3,k)'],'Color','r','LineWidth',2)

        end
        
    case 2 % Pose tracker
        
        K=(length(xf)-1)/9; %Number of tools          
        for k=1:K
            % Change the size of the cylinder dependent on the distance
            scale=0.9/(1-pos(3,k));
            % Construct the cylinder
            [X(:,:,:,k),Y(:,:,:,k),Z(:,:,:,k),C(:,:,:,k)]=cylinder_my(pos(:,k),-mag(:,k)/norm(mag(:,k))*tool(k).length*scale,tool(k).thickness*scale);
        end
           
        
        
        % Update the plot handle        
        set(p_handle(1),'EraseMode','normal')        
        for k=1:K
            if k>=2
                set(p_handle(1),'EraseMode','none')
                drawnow
            end
            set(p_handle(1),'XData',X(:,:,:,k),'YData',Y(:,:,:,k),'ZData',Z(:,:,:,k),'CData',C(:,:,:,k)/k) 
        end                                

        
    case 3 % Color calligraphy
                
        %Compute the coordinates of painted region
        [X,Y,Z,C,paint]=drawer_my_color(xf,last_xf,tool(1));
        
        % Decide if the pen is close enough to the screen        
        if paint
            set(p_handle(1),'XData',X,'YData',Y,'ZData',Z,'CData',C)
        end
        
        %if norm(d_cell{1}(:,7:9))>3
        %    text(0,0,0,'Ej f�r n�ra sensor 1!','FontSize',22)
        %end
        %if norm(d_cell{2}(:,7:9))>3
        %    text(0,0,0,'Ej f�r n�ra sensor 2!','FontSize',22)
        %end
        
    case 4 % Grey calligraphy
              
        shading interp
                
        [X,Y,Z,C,paint]=drawer_my(xf,last_xf,tool(1));
        
        % Decide if the pen is close enough to the screen         
       
        if paint
            set(p_handle(1),'XData',X,'YData',Y,'ZData',Z,'CData',C)
        end
        
        %Don't apply too large magnetic field, it might harm the sensor
        %if norm(d_cell{1}(:,7:9))>3
        %    text(0,0,0,'Ej f�r n�ra sensor 1!','FontSize',22)
        %end
        %if norm(d_cell{2}(:,7:9))>3
        %    text(0,0,0,'Ej f�r n�ra sensor 2!','FontSize',22)
        %end
        
    case 5 %default - PLot all measured data
                        
        if CurrentFigureUserData(2)==1
           meas_zero=zeros(size(meas_zero)); % Plot around true zero if zoomed out
        end
 
        %  plot the data
        for i=1:1
            %Plot the measurement            
            addpoints(p_handle(1),t,meas(:,1+3*(i-1))'-meas_zero(1+(i-1)*3))
            addpoints(p_handle(2),t,meas(:,2+3*(i-1))'-meas_zero(2+(i-1)*3))
            addpoints(p_handle(3),t,meas(:,3+3*(i-1))'-meas_zero(3+(i-1)*3))          

            %set(p_handle(1),'XData',t,'YData',[last_meas(1+3*(i-1)) meas(:,1+3*(i-1))']-meas_zero(1+(i-1)*3),'Color','b','LineWidth',2)
            %set(p_handle(2),'XData',t,'YData',[last_meas(2+3*(i-1)) meas(:,2+3*(i-1))']-meas_zero(2+(i-1)*3),'Color','b','LineWidth',2)
            %set(p_handle(3),'XData',t,'YData',[last_meas(3+3*(i-1)) meas(:,3+3*(i-1))']-meas_zero(3+(i-1)*3),'Color','b','LineWidth',2)          
          
        end
        
    case 6 %default - PLot all measured data
            
            
        if CurrentFigureUserData(2)==1
           meas_zero=zeros(size(meas_zero)); % Plot around true zero if zoomed out
        end
 
        %  plot the data
        for i=1:N_sensor
            %Plot the measurement
            addpoints(p_handle(i),t,meas(:,1+3*(i-1))'-meas_zero(1+(i-1)*3))
            addpoints(p_handle(4+i),t,meas(:,2+3*(i-1))'-meas_zero(2+(i-1)*3))
            addpoints(p_handle(8+i),t,meas(:,3+3*(i-1))'-meas_zero(3+(i-1)*3))            
          
        end
     
    case 7 %
                        
        if CurrentFigureUserData(2)==1
           meas_zero=zeros(size(meas_zero)); % Plot around true zero if zoomed out
        end
 
        %  plot the data        
        for i=1:1
            %Plot the measurement
            set(p_handle(1),'XData',t,'YData',[last_meas(1+3*(i-1)) meas(:,1+3*(i-1))']-meas_zero(1+(i-1)*3),'Color','b','LineWidth',2)
            set(p_handle(2),'XData',t,'YData',[last_meas(2+3*(i-1)) meas(:,2+3*(i-1))']-meas_zero(2+(i-1)*3),'Color','b','LineWidth',2)
            %set(p_handle(3),'XData',t,'YData',[last_meas(3+3*(i-1)) meas(:,3+3*(i-1))']-meas_zero(3+(i-1)*3),'Color',[1 1 1]*0.8,'LineWidth',2)          
          
        end
        
        x1=last_meas(1)-meas_zero(1);
        x2=meas(end,1)'-meas_zero(1);
        y1=last_meas(2)-meas_zero(2);
        y2=meas(end,2)'-meas_zero(2);
        
        %Plot the measurement
        set(p_handle(3),'XData',[x1 x2],'YData',[y1 y2],'Color','b','LineWidth',2)
        
    case 8 %
                        
        if CurrentFigureUserData(2)==1
           meas_zero=zeros(size(meas_zero)); % Plot around true zero if zoomed out
        end
        
         
        %  plot the data        
        for i=1:1
            %Plot the measurement
            set(p_handle(1),'XData',t,'YData',[last_meas(1+3*(i-1)) meas(:,1+3*(i-1))']-meas_zero(1+(i-1)*3),'Color','b','LineWidth',2)
            set(p_handle(2),'XData',t,'YData',[last_meas(2+3*(i-1)) meas(:,2+3*(i-1))']-meas_zero(2+(i-1)*3),'Color','b','LineWidth',2)
            %set(p_handle(3),'XData',t,'YData',[last_meas(3+3*(i-1)) meas(:,3+3*(i-1))']-meas_zero(3+(i-1)*3),'Color',[1 1 1]*0.8,'LineWidth',2)                    
        end
        
 
        %  plot the data
        x1=last_meas(1)-meas_zero(1);
        x2=meas(end,1)'-meas_zero(1);
        y1=last_meas(2)-meas_zero(2);
        y2=meas(end,2)'-meas_zero(2);

        %Plot the measurement
        set(p_handle(3),'XData',[0 x1 x2],'YData',[0 y1 y2],'CData',[1 1 1]*sign(x1*y2-y1*x2)) 
        
    case 9 % Pose tracker presentation
        
        K=(length(xf)-1)/9; %Number of tools          
        for k=1:K
            % Change the size of the cylinder dependent on the distance
            scale=1;%%0.9/(1-pos(3,k));
            % Construct the cylinder
            [X(:,:,:,k),Y(:,:,:,k),Z(:,:,:,k),C(:,:,:,k)]=cylinder_my(pos(:,k),-mag(:,k)/norm(mag(:,k))*tool(k).length*scale,tool(k).thickness*scale);
        end
                           
        % Update the plot handle        
        %set(p_handle(1),'EraseMode','normal')        
        for k=1:K
            if k>=2
                set(p_handle(1),'EraseMode','none')
                drawnow
            end
            set(p_handle(1),'XData',X(:,:,:,k),'YData',Y(:,:,:,k),'ZData',Z(:,:,:,k),'CData',C(:,:,:,k)/k) 
        end   
        
    case 10 % One sensor two axis
                        
        if CurrentFigureUserData(2)==1
           meas_zero=zeros(size(meas_zero)); % Plot around true zero if zoomed out
        end
 
        %  plot the data
        for i=1:1
            %Plot the measurement
            set(p_handle(1),'XData',t,'YData',[last_meas(1+3*(i-1)) meas(:,1+3*(i-1))']-meas_zero(1+(i-1)*3),'Color','b','LineWidth',2)
            set(p_handle(2),'XData',t,'YData',[last_meas(2+3*(i-1)) meas(:,2+3*(i-1))']-meas_zero(2+(i-1)*3),'Color','b','LineWidth',2)
            %set(p_handle(3),'XData',t,'YData',[last_meas(3+3*(i-1)) meas(:,3+3*(i-1))']-meas_zero(3+(i-1)*3),'Color','b','LineWidth',2)                    
        end                        
end % switch

% flush the graphics to screen
drawnow
% -------------------------------------------------------------------------
% end of function

function mt_figure_close(obj, ~, h, f_handle,fpIMU,fpXF)

if ~isempty(h) %Data comes from the COM-ports
    % local function to properly release the serial port object and the output file          
    fclose(h);        
end
if ~isempty(fpIMU) %Close the file if we write to it
    fclose(fpIMU);
end
if ~isempty(fpXF) %Close the file if we write to it
    fclose(fpXF);
end
% kill figure window as requested
delete(f_handle)
% -------------------------------------------------------------------------
% end of function




