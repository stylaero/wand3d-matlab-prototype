scale_analog=1;
visualize('analog_ort','analog_setup',scale_analog,[],'Experiment 1a - Analog magnetometers')
visualize('analog_par','analog_setup',scale_analog,[],'Experiment 2a - Analog magnetometers')
visualize('analog_all','analog_setup',scale_analog,[0 12],'Experiment 3a - Analog magnetometers')

scale_digital=1;
visualize('digital_ort','digital_setup',scale_digital,[],'Experiment 1b - Digital magnetometers');
visualize('digital_par','digital_setup',scale_digital,[],'Experiment 2b - Digital magnetometers')
visualize('digital_all','digital_setup',scale_digital,[0 15],'Experiment 3b - Digital magnetometers')
%%
%close all
grid on
legend('Analog magnetometers','Digital magnetometer')
title('Experimant 3a-b, RMSE of position') 
print('-depsc', '../figures/all_rmse_pos.eps')
system('epstopdf ../figures/all_rmse_pos.eps')
%%
grid on
legend('Analog magnetometers','Digital magnetometer')
title('Experimant 3a-b, RMSE of orientation') 
print('-depsc', '../figures/all_rmse_ori.eps')
system('epstopdf ../figures/all_rmse_ori.eps')
