function [x_nb,q_nb,q_vn,q_vb]=convert_vicon_data(DATA)
    % b - body fram (the magnet)
    % n - navigation frame  (the magnetometer network)
    % v - vicon frame
    
    % Orientation and position of n-frame
    euler_vn=pi/180*mean(DATA(:,7:9)); % Magnetometer network stationary
    q_vn=angle2quat(euler_vn(3),euler_vn(2),euler_vn(1));
    x_vn=mean(DATA(:,10:12));
    
    
    % Orientation and position of b-frame
    euler_vb=pi/180*DATA(:,1:3);
    q_vb=angle2quat(euler_vb(:,3),euler_vb(:,2),euler_vb(:,1));
    x_vb=DATA(:,4:6);

    % Orientation of b-frame wrt n-frame        
    q_nb=quatmultiply(q_vb,quatinv(q_vn));
                
    x_nb=quatrotate(q_vn,x_vb-repmat(x_vn,size(x_vb,1),1));
    x_nb=x_nb/1000; % use meter
end