function data_out=syncronize_all_nodes_res(data_in,arI,maxdiff)
%SYNCRONIZE_ALL_NODES_2     Syncronizes all nodes by minimizing the
%residual
%
%   SYNCRONIZE_ALL_NODES(DATA,I,MAXDIFF)
%
%   DATA_IN:    Is 1 x J cell array where each components contains a M x (Nj+1) matrix where the 
%               first Ni colums consists of data and the last column of
%               a time vector
%   DATA_OUT:   Is a M x (N1+...NJ+1) matrix where the first N1+.. + NJ colums consists of
%               syncronized data and the last column of a common time
%               vecotr
%   ARI:        A 1 x J cell array with indecis which point out the columns in DATA_IN that should 
%               be used for the correlation analysis that should be used for the
%               correlation analysis.
%   MAXDIFF:    The maximal allowed time difference between the first
%               sensor node and the other sensor nodes.
%
%
    N=length(data_in{1}(:,1));
    J=length(data_in);
    for j=1:(J-1)
        N=min(N,length(data_in{j+1}(:,1)));
    end
    col={'b','r','g'};
    figure;
    %Make a low-pass version of the data that will be input to the
    %correlation analysis
    fs=1/(data_in{1}(2,end)-data_in{1}(1,end));
    
    M=floor(fs*maxdiff);
    Mar=-M:M;
    
    
    
    %Find the maximum correlation
    for j=2:J
        for i=1:M
            data_in{1}(1:N,arI)-data_in{j}(1:N,arI)
        end
        i=1;
        ij=i+(j-1)*J;
        ji=j+(i-1)*J;
        corr=[R(ji,M:-1:1) R(ij,2:M)];
        [r,ind]=max(corr);
        m=(-(M-1)):(M-1);
        di(j)=m(ind);
        figure;
        plot((1/fs)*m,corr)
        title(['E x',num2str(i),'(t)*x',num2str(j),'(t+ti)'])
        xlabel('ti [s]')
    end
    di=di-min(di);
    
    %Find the maximal common length of the data
    N=inf;
    for j=1:J
       N=min(N,length(data_in{j}(:,1))-di(j));
    end
    data_out=[];
    
    %Build the output
    t_new=(1/fs)*(0:(N-1))';
    for j=1:J
        X_new=data_in{j}(di(j)+(1:N),1:(end-1));
        data_out=[data_out,X_new];        
    end
    data_out=[data_out,t_new];
end