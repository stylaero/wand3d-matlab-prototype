function [xp,Pp]=time_update(m,xf,Pf,tuning,N)
        % Implements a standard time update in a Kalman filter, for reference see for example 
       % [3] F. Gustafsson. Statistical Sensor Fusion. Studentlitteratur, 1 edition, 2010.
       
       % m - The model
       
       % xp - state, prediction
       % xp - state, prediction covariance
       
       % xf - state, filter
       % Pf - state, filter covariance
       Q=m.Q;       
       B = m.B(xf,N);
       Q=B*Q*B'; %Process noise
       %Q=m.B*m.Q*m.B'*m.Qs^2; %Process noise
       A=m.A(xf,N); %System matrix
    
       %% Time update   
       xp=A*xf;
       Pp=A*Pf*A'+Q*N^2;

end