function [xf,Pf,yf,yPf,res]=measurement_update(model,yk,xp,Pp,tuning,tool)
       % Implements a standard measurement update in an Extended kalman filter, here with a linear
       % dynamics and a couple of projections steps for increasing the
       % robustnes of the estimation, for reference see for example 
       % [3] F. Gustafsson. Statistical Sensor Fusion. Studentlitteratur, 1 edition, 2010.
       
       % model - Model object
       % yk -measurement
       % yf -measurement, filter
       % yPf -measurement, filter covariance       

       % xp - state, prediction
       % xp - state, prediction covariance
       
       % xf - state, filter
       % Pf - state, filter covariance
       
       % res - residual
       
       
       % Evaluation
       R=model.R; %Measurement noise       
       
       % Linearization
       %C=numgrad(@(x) model.h(x),1,xp); %Compute the Jacobian numerically
       
       %% Measurement update       
       [yphat,C]=model.h(xp); %Compute the predicted measurement and Jacobian              
       %R2 = diag(kron((sqrt(sum(reshape((yk'-model.meas_zero).^2,3,4),1))).^(6),ones(1,3))).^2;
       %sqrt(diag(R2)./diag(R))'
       %yk'-model.meas_zero
       %R = R + R2; 
       %yk(1) = yk(1)+1;
       epsilon = yk-yphat;
       %ind = find(abs(epsilon) < 0.1);
       %indn = find(abs(epsilon) >= 0.1);
       %epsilon'
       %indn'
       
       % Reduce measurement       
       S=C*Pp*C'+R;       
       ind = find(abs(epsilon) < sqrt(diag(S))*25);
       indn = find(abs(epsilon) >= sqrt(diag(S))*25);
       %epsilon=epsilon(ind);     
       %C = C(ind,:);
       %S = S(ind,ind);
       %R = R(ind,ind); 
       for i = indn
           S(i,i) = S(i,i)+1e4;
       end
       
       %S(indn,indn) = 0; % Set the inverse of the innovtion matrix to zero for these row and columns that correspond to the measurement dimension that should be removed. This corresponds to infinite noise covariance for these dimensions.       
       Sinv = inv(S);
       Kg=Pp*C'*Sinv;
       indn
       
       
       % State
       xf=xp+Kg*epsilon;
      
       %xf
       
       Pf=Pp-Kg*C*Pp;
       Pf=0.5*(Pf+Pf');
       if length(indn)>6
           disp('hit')
       end
       
       
       %% Projection step
       % Thise steps ae not included in the standard EKF but will decrease
       % the risk of divergence of the filter for this particulary setup
       
       % Make a projection of the magnetic dipole moment onto a sphere.                                          
       
       for k=1:1
           %norm(xf((7:9)+9*(k-1)))
           %if norm(xf((7:9)+9*(k-1))) > tool(k).dipole_strength
           %     xf((7:9)+9*(k-1))=xf((7:9)+9*(k-1))*tool(k).dipole_strength/norm(xf((7:9)+9*(k-1)));
           %end
       end
       
              
       % Project the position of the magnetic dipole onto a the tracking
       % volume of intrest, here given by a box
       
       lim=tuning.tracking_volume;
       nx = model.nx;
       for k=1:model.K             
               if lim(1) > xf(1+nx*(k-1)) || lim(2) < xf(1+nx*(k-1)) || lim(3) > xf(2+nx*(k-1)) || lim(4) < xf(2+nx*(k-1)) || lim(5) > xf(3+nx*(k-1)) || lim(6) < xf(3+nx*(k-1))
                   % Project the position onto the box.
                   xf((1:3)+nx*(k-1))=min(max(xf((1:3)+nx*(k-1)),lim([1 3 5])),lim([2 4 6]));
                   xf((4:6)+nx*(k-1))=zeros(3,1); %Set velocity to zero if on the boundery of the box.           
                   %xf((10:12)+nx*(k-1))=zeros(3,1); %Set velocity to zero if on the boundery of the box.           
               end     
               if norm(xf((7:9)+nx*(k-1))) > sqrt(tool(k).dipole_strength)
                   xf((7:9)+nx*(k-1)) = xf((7:9)+nx*(k-1))/norm(xf((7:9)+nx*(k-1)))*sqrt(tool(k).dipole_strength);
               end
           %xf(7:8+9*(k-1))=[0.0000001;-0.0000001];
       end
       
       %Measurement
       yf=model.h(xf);
       yf = yf(ind);

       %meas_zero(2)
       yPf=C*Pf*C'+R;
       res=norm(yk(ind)-yf);
       
       
end
