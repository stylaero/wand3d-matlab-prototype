ny=12;
nx = 6;
R=randn(ny,ny);R=R*R';
C=randn(ny,nx);
epsilon=rand(ny,1);
xp=rand(nx,1);
Pp = randn(nx,nx);Pp=Pp*Pp';

% Compute innovation
S=C*Pp*C'+R;

% Indeces to process
ind =[1 3 4 7 8];

%% Method 1
Pi = zeros(length(ind),ny);
for i=1:length(ind)
    Pi(i,ind(i))=1;
end

epsilon1=Pi*epsilon;     
C1 = Pi*C;
R1 = Pi*R*Pi';

Kg1=Pp*C'*Pi'*inv(Pi*S*Pi');

% State
xf1=xp+Kg1*Pi*epsilon;
Pf1=Pp-Kg1*Pi*C*Pp;

%% Method 2

    [S2,C2] = DoGating(S,C,ind);    
    Kg2=Pp*C2'*inv(S2);
    % State
    xf2=xp+Kg2*epsilon;
    Pf2=Pp-Kg2*C2*Pp;
    xf2-xf1
    Pf2-Pf1

