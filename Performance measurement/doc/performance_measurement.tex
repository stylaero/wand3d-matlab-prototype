\documentclass[11pt,a4paper]{article}

\usepackage[english]{babel}
\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\usepackage{float}
\usepackage{fancyhdr}
\usepackage{enumitem}
\usepackage{color}
\usepackage{tabularx}
\usepackage{verbatim}
\usepackage{cite}
\usepackage{url}
\usepackage{amsmath}
\usepackage{fullpage}
\usepackage{subfig}
\usepackage{booktabs}
%\usepackage{listings}

\title{Performance measurement - 3DWand}
\author{Niklas Wahlstr\"om}
\date{\today}


\begin{document}
\maketitle
This document will describe the experiment conducted in the VICON-lab at LiU 2012-10-01 for evaluating two sensor networks within the 3DWand project.
\section{Sensors}
Two magnetometer sensor network have been tested. The first sensor network consists of four analog three-Axis Magnetic Sensor HMC1043 from Honeywell with the sampling frequency 300 Hz and the second sensor network consists of four 3-Axis Digital Compass IC, HMC5983, also from Honeywell, which has a sampling frequency of 220 Hz.


\section{Target}
The target consists of two neodymmagnets placed upon each other, each with a Diameter of 12 mm and a height of 6 mm.
\section{Experiment}
In order to get ground truth data a camera based motion capture system produced by Vicon has been used. Markers detectable by the Vicon-system were attached to the two sensor networks, see Figure~\ref{fig:sen}, as well as to a wooden platform on which the magnet was attached, see Figure~\ref{fig:target}.
\begin{figure}[h!]%
\centering
\subfloat[First platform, four analog three-Axis Magnetic Sensors, HMC1043]{
  \includegraphics[width=7cm]{../figures/sensor_board_analog.jpg}
  \label{fig:sen1}
}
\subfloat[Second platform, four 3-Axis Digital Compass IC, HMC5983]{
  \includegraphics[width=7cm]{../figures/sensor_board_digital.jpg}
    \label{fig:sen2}
} 
\caption{The two sensor boards with VICON markers on it.}
\label{fig:sen}
\end{figure}

\begin{figure}[h!]%
\centering
\subfloat[The target platform from above]{
  \includegraphics[width=7cm]{../figures/magnet_board2.jpg}
  \label{fig:target1}
}
\subfloat[The target platform from the side.]{
  \includegraphics[width=7cm]{../figures/magnet_board.jpg}
    \label{fig:target2}
} 
\caption{The target platform with VICON markers and the magnet on it. One marker is on top of the magnet in the middle of the plattform.}
\label{fig:target}
\end{figure}
During the experiments the target platform was moved in different directions close to the magnetometer network, as depicted in Figure~\ref{fig:experiment_setup}. In Figure~\ref{fig:coordinate_systems} the coordinate systems of interest in the experiment are depicted.

\begin{figure}[h!]%
\centering
\subfloat[Experiment setup.]{
  \includegraphics[width=7cm]{../figures/experiment_setup.jpg}
  \label{fig:experiment_setup}
}
\subfloat[Coordinate systems in the experiment. Three coordinate frames are considered, vicon-frame, navigation-frame and body-frame]{
  \includegraphics[width=7.5cm]{../figures/coordinate_systems.jpg}
    \label{fig:coordinate_systems}
} 
\caption{The target platform with VICON markers and the magnet on it. One marker is on top of the magnet in the middle of the plattform.}
\label{fig:experiment_setup_coordinate_systems}
\end{figure}

In total 6 experiments where conducted:
\begin{itemize}
\item \textit{Experiment 1a} Ca 20 seconds of movement ca 15 cm above the sensor network with the magnet being vertically oriented using HMC1043.
\item \textit{Experiment 1b} Same as Experiment 1a but with HMC5983.
\item \textit{Experiment 2a} Ca 20 seconds of movement ca 15 cm above the sensor network with the magnet being horizontally oriented using HMC1043.
\item \textit{Experiment 2b} Same as Experiment 2a but with HMC5983.
\item \textit{Experiment 3a} Ca 50 seconds of movement with different orientations and distances to the sensor network using HMC1043.
\item \textit{Experiment 3b} Same as Experiment 3a but with HMC5983.
\end{itemize}

\section{Model and Tuning}
For the position of the target a constant velocity model has been used where the standard deviation of the process noise has been set to 1 m/s$^2$.
For the orientation a constant position model has been used where the standard deviation of the process noise has been set to 1 rad/s.

The covariance of the measurement noise has been estimated for each of the two sensor network using stationary data without targets. HMC5983 have a noise level which is approximately 5 times larger than the HMC1043, as displayed in Table \ref{tab:noise}.

\begin{table}[h!]
	\centering
	\caption{Sensor noise}
	\begin{tabular}{l | c c c c c}
		\toprule
		   Sensors  & Range & Resolution & Quant. $\Delta$ & $\frac{\Delta}{\sqrt{12}}$ & $\sqrt{\frac{\textrm{tr} R}{\dim{R}}}$ \\%$\sqrt{\frac{\textrm{tr} R}{\dim{R}}-\frac{\Delta^2}{12}}$\\
		\midrule
Analog (HMC1043) & $\pm 7$ [G] & 24 bits & 0.83 [$\mu$G] & 0.24 [$\mu$G] & 0.54 [mG] \\%& 0.54 [mG]  \\
Digital (HMC5983) & $\pm 5.6$ [G] & 12 bits & 2.73 [mG] & 0.79 [mG] & 2.77 [mG] \\%& 2.61 [mG] \\
\bottomrule
	\end{tabular}
	\label{tab:noise}
\end{table}

These estimates have been used as tuning of the covariance matrices in the filter for each of the two sensor networks respectively.


\section{Results}
\subsection{Position estimation}
With the the estimated position $\hat{\mathbf  x}_k$ and the ground truth position $\mathbf x_k^0$ data provided by the Vicon-system, the estimation error can be computed. 
\begin{align}
\mathbf x_k^e=\hat{\mathbf  x}_k-\mathbf x_k^0
\end{align}
and the Root mean square error can be computed as
\begin{align}
\text{RMSE}    	& =  \sqrt{\frac{1}{N}\sum_{k=1}^N	\|\mathbf x_k^{e}\|^2} \\
\end{align}

This can be decomposed in a bias and a variance part. A 4th order Butterworth-filter with the cutoff frequency of 10 Hz has been used to separate the error in a low and high frequency part. 
\begin{align}
\mathbf x_k^e=\mathbf x_k^{e,\textrm{low}}+\mathbf x_k^{e,\textrm{high}}
\end{align}
and the variance and bias contributions to RMSE can be defined as
\begin{align}
\text{RMSE}    	& =  \sqrt{\frac{1}{N}\sum_{k=1}^N	\|\mathbf x_k^{e}\|^2} \\
\sqrt{\text{variance}}	& =  \sqrt{\frac{1}{N}\sum_{k=1}^N	\frac{1}{1-W_n}\|\mathbf x_k^{e,\textrm{high}}\|^2} \\
\text{bias}		& =  \sqrt{\frac{1}{N}\sum_{k=1}^N	\|\mathbf x_k^{e}\|^2-\frac{1}{1-W_n}\|\mathbf x_k^{e,\textrm{high}}\|^2}
\end{align}


In Table \ref{tab:exp1} and Table \ref{tab:exp2} results from Experiment 1ab, 2ab are presented. 
Here it can be concluded that both sensor networks have almost the same performance regarding bias, but that HMC1043 has a lower variance.

In Figure~\ref{fig:exp1a_pos}, \ref{fig:exp1b_pos}, \ref{fig:exp2a_pos}, \ref{fig:exp2b_pos}, \ref{fig:exp3a_pos}, \ref{fig:exp3b_pos} all three Cartesian components of estimated position $\hat{\mathbf  x}_k$ and ground truth $\mathbf  x_k^0$ in all six experiments are displayed.


For experiment 3ab, Figure~\ref{fig:exp3} displays the RMSE as a function of position from the center point of the sensor network $\|\mathbf x_0\|$.

Here it can be concluded for both system the RMSE increases with the distance, approximately cubically to the distance, RMSE $\propto \|\mathbf x^0\|^3$, which is also the rate with which SNR increases with distance. Furthermore, the digital sensor network has a slightly worse RMSE than the analog one if the magnet is fare from the sensors.

\begin{table}[h!]
	\centering
	\caption{Experiment 1ab: RMSE, bias and variance for position}
	\begin{tabular}{r c c c}
		\toprule
		   Sensors  & RMSE [mm] & Bias [mm] & $\sqrt{\textrm{Var}}$ [mm]\\
		\midrule
Analog sensor network (HMC1043) &  7.29 & 7.28 & 0.36 \\
Digital sensor network (HMC5983)& 4.80 & 4.68 & 0.97 \\
\bottomrule
	\end{tabular}
	\label{tab:exp1}
\end{table}

%\subsection{Experiment 2}
\begin{table}[h!]
	\centering
	\caption{Experiment 2ab: RMSE, bias and variance for position}
	\begin{tabular}{r c c c}
		\toprule
		   Sensors  & RMSE [mm] & Bias [mm] & $\sqrt{\textrm{Var}}$ [mm]\\
		\midrule
Analog sensor network (HMC1043) & 4.24 & 4.21 & 0.44 \\
Digital sensor network (HMC5983) & 6.26 & 6.21 & 0.74 \\
\bottomrule
	\end{tabular}
	\label{tab:exp2}
\end{table}


%\subsection{Experiment 3}
\begin{figure}[h!]%
\centering
  \includegraphics[width=15cm]{../figures/all_rmse_pos.pdf}  
\caption{RMSE of position of Experiment 3.}
\label{fig:exp3}
\end{figure}




\subsection{Orientation estimation}
Also the orientation has been estimated and compared with the Vicon system. 3DWand is able to estimate two degrees of freedom for orientation. Thees two DOF can be compared with the corresponding two DOF given by the Vicon system. By aligning the $Z_b$-axis of the local coordinate system of the magnet with its dipole vector, the orientation of the b-frame wrt the n-frame (see Figure~\ref{fig:coordinate_systems}) can be presented with Euler angles using the convention Y-X'-Z''. 3Dwand can now be compared with the Vicon data in terms of rotation around Y and X' respectively. Note that 3DWand can not resolve rotation around Z'' since the dipole of the magnet is aligned with this axis. Therefor this rotation is not considered here.

In Figure~\ref{fig:exp1a_ori}, \ref{fig:exp1b_ori}, \ref{fig:exp2a_ori}, \ref{fig:exp2b_ori}, \ref{fig:exp3a_ori}, \ref{fig:exp3b_ori} the angles of these two rotations are displayed for all six experiments. In the same manner as with the position, the RMSE, bias and variance can be computed for the estimation error. 

In Table \ref{tab:exp1ori} and Table \ref{tab:exp2ori} results from Experiment 1ab, 2ab are presented.
Here it can be concluded that both HMC5983 has a better performance regarding bias, but that HMC1043 has a lower variance.

\begin{table}[h!]
	\centering
	\caption{Experiment 1ab: RMSE, bias and variance for orientation}
	\begin{tabular}{r c c c}
		\toprule
		   Sensors  & RMSE [degrees] & Bias [degrees] & $\sqrt{\textrm{Var}}$ [degrees] \\
		\midrule
Analog sensor network (HMC1043) & 5.57$^{\circ}$ & 5.56$^{\circ}$ & 0.23$^{\circ}$ \\
Digital sensor network (HMC5983) & 2.63$^{\circ}$ & 2.55$^{\circ}$ & 0.58$^{\circ}$ \\
\bottomrule
	\end{tabular}
	\label{tab:exp1ori}
\end{table}

%\subsection{Experiment 2}
\begin{table}[h!]
	\centering
	\caption{Experiment 2ab: RMSE, bias and variance for orientation}
	\begin{tabular}{r c c c}
		\toprule
		   Sensors  & RMSE [degrees] & Bias [degrees] & $\sqrt{\textrm{Var}}$ [degrees]\\
		\midrule
Analog sensor network (HMC1043) & 2.66$^{\circ}$ & 2.65$^{\circ}$ & 0.15$^{\circ}$ \\
Digital sensor network (HMC5983) & 2.05$^{\circ}$ & 2.04$^{\circ}$ & 0.23$^{\circ}$ \\
\bottomrule
	\end{tabular}
	\label{tab:exp2ori}
\end{table}

For experiment 3ab, Figure~\ref{fig:exp3} displays the RMSE of the orientation as a function of position from the center point of the sensor network $\|\mathbf x_0\|$.

Here it can be concluded that HMC5983 has a better performance regarding than HMC1043 close to the sensor network (due to lower bias as concluded earlier). However, further away HMC5983 has a worse performance than HMC1043.

\begin{figure}[h!]%
\centering
  \includegraphics[width=15cm]{../figures/all_rmse_ori.pdf}  
\caption{RMSE of orientation of Experiment 3.}
\label{fig:exp3ori}
\end{figure}

\section{Conclusion}
This document has presented results from a performance analysis using two systems of 3DWand. Both systems had a RMSE in the interval 4-7 mm for position. For the orientation RMSE, the digital sensor network (HMC5983) performed surprisingly better than analog one (HMC1043). The RMSE was 2-3 degrees (HMC5983) and 4-6 degrees (HMC1043) respectively.
However, the variance of the estimate is 2-4 times higher for HMC5983 than for HMC1043 (both position and orientation), which is natural since the measurement noise in HMC5983 is larger. For many applications the variance might be of greater importance than the RMSE. The variance will also depend on the tuning of the filter, which has not been investigated here. Also the bandwidth of the tracking system, which is a crucial parameter for many applications, has not been investigated.

\pagebreak
% Exp 1a
\begin{figure}[h!]%
\centering
\subfloat[Position]{
  \includegraphics[width=13cm]{../figures/analog_ort_pos.pdf}
  \label{fig:exp1a_pos}
} \\
\subfloat[Orientation]{
  \includegraphics[width=13cm]{../figures/analog_ort_ori.pdf}
    \label{fig:exp1a_ori}
} 
\caption{Experiment 1a}
\label{fig:exp1a}
\end{figure}

% Exp 1b
\begin{figure}[h!]%
\centering
\subfloat[Position]{
  \includegraphics[width=13cm]{../figures/digital_ort_pos.pdf}
  \label{fig:exp1b_pos}
} \\
\subfloat[Orientation]{
  \includegraphics[width=13cm]{../figures/digital_ort_ori.pdf}
    \label{fig:exp1b_ori}
} 
\caption{Experiment 1b}
\label{fig:exp1b}
\end{figure}

% Exp 2a
\begin{figure}[h!]%
\centering
\subfloat[Position]{
  \includegraphics[width=13cm]{../figures/analog_par_pos.pdf}
  \label{fig:exp2a_pos}
} \\
\subfloat[Orientation]{
  \includegraphics[width=13cm]{../figures/analog_par_ori.pdf}
    \label{fig:exp2a_ori}
} 
\caption{Experiment 2a}
\label{fig:exp2a}
\end{figure}

% Exp 2b
\begin{figure}[h!]%
\centering
\subfloat[Position]{
  \includegraphics[width=13cm]{../figures/digital_par_pos.pdf}
  \label{fig:exp2b_pos}
} \\
\subfloat[Orientation]{
  \includegraphics[width=13cm]{../figures/digital_par_ori.pdf}
    \label{fig:exp2b_ori}
} 
\caption{Experiment 2b}
\label{fig:exp2b}
\end{figure}

% Exp 3a
\begin{figure}[h!]%
\centering
\subfloat[Position]{
  \includegraphics[width=13cm]{../figures/analog_all_pos.pdf}
  \label{fig:exp3a_pos}
} \\
\subfloat[Orientation]{
  \includegraphics[width=13cm]{../figures/analog_all_ori.pdf}
    \label{fig:exp3a_ori}
} 
\caption{Experiment 3a}
\label{fig:exp3a}
\end{figure}



% Exp 3b
\begin{figure}[h!]%
\centering
\subfloat[Position]{
  \includegraphics[width=13cm]{../figures/digital_all_pos.pdf}
  \label{fig:exp3b_pos}
} \\
\subfloat[Orientation]{
  \includegraphics[width=13cm]{../figures/digital_all_ori.pdf}
    \label{fig:exp3b_ori}
} 
\caption{Experiment 3b}
\label{fig:exp3b}
\end{figure}




\end{document}
