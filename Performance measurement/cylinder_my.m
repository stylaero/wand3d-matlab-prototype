function [X,Y,Z,C]=cylinder_my(r,m,R)
    % Formes a cylinder with at position r, aligned with m and with radius
    % R
    
    n=40; %Number of N points around the circumference
    
    l=5; %Number of sections
    
    %Construct a circle with m as its normal
    R1=[m(2),-m(1),0]';
    R1=R1/norm(R1);
    R2=cross(m,R1);
    R2=R2/norm(R2);
    w=((0:n)/n)*2*pi;
    XYZ=R*(R1*cos(w)+R2*sin(w));
    
    % Contruct a cylinder from these circles by using l copies of these
    % circles
    M=repmat(m/2,1,n+1);
    XYZ=[XYZ+M/2;XYZ-M/2];
    for i=1:l
        X(i,:)=XYZ(1,:)+(0.5-(i-1)/(l-1))*m(1)+r(1);
        Y(i,:)=XYZ(2,:)+(0.5-(i-1)/(l-1))*m(2)+r(2);
        Z(i,:)=XYZ(3,:)+(0.5-(i-1)/(l-1))*m(3)+r(3);
    end
    
    % Color the sections
    C=[ones(1,n+1);zeros(l-1,n+1)];
        
end