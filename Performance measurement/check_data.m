saveData=0; %0: Do not save data. 1: Do save data
mode_data=0; %0: Read data from file 1: Read data from sensor
calibrate_startup=0; %0: Do not calibrate, use old calibration. 1: Calibrate
port=str_date;
%Run the demo in real time using data from the sensors
str_date=DisplayRealtimeData(port,tool,network,sensor,tuning,saveData,mode_data,calibrate_startup)