function [xf,Pf,yf,yPf,res]=measurement_update(sensor,yk,xp,Pp,tool,tuning,network,meas_zero)
       % Implements a standard measurement update in an Extended kalman filter, here with a linear
       % dynamics and a couple of projections steps for increasing the
       % robustnes of the estimation, for reference see for example 
       % [3] F. Gustafsson. Statistical Sensor Fusion. Studentlitteratur, 1 edition, 2010.
       
       % sensor - Sensor struct
       % yk -measurement
       % yf -measurement, filter
       % yPf -measurement, filter covariance       

       % xp - state, prediction
       % xp - state, prediction covariance
       
       % xf - state, filter
       % Pf - state, filter covariance
       
       % res - residual
       
       % Linearization
       %C=numgrad(m.h,1,xp); %Compute the Jacobian numerically
       
       % Evaluation
       R=sensor.R; %Measurement noise
       K=length(tool); %Number of tools
       
       %% Measurement update       
       [yphat,C]=h(xp,network,K,meas_zero); %Compute the predicted measurement and Jacobian       
       S=C*Pp*C'+R;
       Kg=Pp*C'*inv(S);
       
       % State
       xf=xp+Kg*(yk-yphat);
       
       Pf=Pp-Kg*C*Pp;
       Pf=0.5*(Pf+Pf');
       
       
       %% Projection step
       % Thise steps ae not included in the standard EKF but will decrease
       % the risk of divergence of the filter for this particulary setup
       
       % Make a projection of the magnetic dipole moment onto a sphere.                                          
       %for k=1:K
           %if norm(xf((7:9)+9*(k-1))) > tool(k).dipole_strength
                %xf((7:9)+9*(k-1))=xf((7:9)+9*(k-1))*tool(k).dipole_strength/norm(xf((7:9)+9*(k-1)));
           %end
       %end
       
              
       % Project the position of the magnetic dipole onto a the tracking
       % volume of intrest, here given by a box
       
       lim=tuning.tracking_volume;
       for k=1:K  
           if lim(1) > xf(1+9*(k-1)) || lim(2) < xf(1+9*(k-1)) || lim(3) > xf(2+9*(k-1)) || lim(4) < xf(2+9*(k-1)) || lim(5) > xf(3+9*(k-1)) || lim(6) < xf(3+9*(k-1))
               % Project the position onto the bo.
               xf((1:3)+9*(k-1))=min(max(xf((1:3)+9*(k-1)),lim([1 3 5])),lim([2 4 6]));
               xf((4:6)+9*(k-1))=zeros(3,1); %Set velocity to zero if on the boundery of the box.
           end
       end
       
       %Measurement
       yf=h(xf,network,K,meas_zero);

       %meas_zero(2)
       yPf=C*Pf*C'+R;
       res=norm(yk-yf);
       
       
end
