function [xp,Pp]=time_update(m,xf,Pf,tuning)
        % Implements a standard time update in a Kalman filter, for reference see for example 
       % [3] F. Gustafsson. Statistical Sensor Fusion. Studentlitteratur, 1 edition, 2010.
       
       % m - The model
       
       % xp - state, prediction
       % xp - state, prediction covariance
       
       % xf - state, filter
       % Pf - state, filter covariance
       
        % Change the process noise in an adaptive manner Comment: This is an adaptive Kalman filter, in the next version I would prefer a IMM (filter bank) implementation instead        
        if norm(xf(4:6)) < tuning.residual_level(1)
            m.Qs=tuning.noise_scaling(1);
            %disp('low')
        elseif norm(xf(4:6)) < tuning.residual_level(2)
            m.Qs=tuning.noise_scaling(2);
            %disp('normal')
        else
            m.Qs=tuning.noise_scaling(3);
            %disp('HIGH')
        end
       
       Q=m.B*m.Q*m.B'*m.Qs^2; %Process noise
       A=m.A; %System matrix
    
       %% Time update       
       xp=A*xf;
       Pp=A*Pf*A'+Q;

end