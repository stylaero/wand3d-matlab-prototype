function [data_out,Tdi]=synchronize_all_nodes_loop(data_in,arI,maxdiff,syncInt)
%SYNCHRONIZE_ALL_NODES_LOOP     Synchronizes all nodes by a correlation
%analysis, refine the time offset iteratively
%
%   SYNCHRONIZE_ALL_NODES(DATA,I,MAXDIFF)
%
%   DATA_IN:    Is 1 x J cell array where each components contains a M x (Nj+1) matrix where the 
%               first Ni colums consists of data and the last column of
%               a time vector
%   DATA_OUT:   Is a M x (N1+...NJ+1) matrix where the first N1+.. + NJ colums consists of
%               synchronized data and the last column of a common time
%               vecotr
%   ARI:        A 1 x J cell array with indecis which point out the columns in DATA_IN that should 
%               be used for the correlation analysis that should be used for the
%               correlation analysis.
%   MAXDIFF:    The maximal allowed time difference between the first
%               sensor node and the other sensor nodes.
%   Tdi:        The computed time shift
%   SYNCINT     The time window to be used for syncronization
%
%
    Tdi=zeros(1,length(arI));
    for i=1:10 % Iterate
        [data_in,Tdi_tmp]=synchronize_all_nodes(data_in,arI,maxdiff,syncInt); % Syncronize
        Tdi=Tdi+Tdi_tmp; % Accumulate the time shift
        if sum(Tdi_tmp)==0 % If no time shift, then break
            break;
        else
            maxdiff=Tdi*2; % The maxdiff for the next time shift
        end
    end
    data_out=data_in;
end