function q=sphere2quat(m)
    %switch t
    %    case 1
    %        [N1,N2]=size(m);
    %        u=[-m(:,2),m(:,1),zeros(N1,1)];
    %        u=u./repmat(sqrt(sum(u.^2,2)),1,3);
    %        m=m./repmat(sqrt(sum(m.^2,2)),1,3);
    %        alpha=acos(m(:,3));
    %        q=[cos(alpha/2) u.*repmat(sin(alpha/2),1,3)];
    %    case 2
            [N1,N2]=size(m);
            
            q=zeros(N1,4);
            
            m=m./repmat(sqrt(sum(m.^2,2)),1,3);
            q(:,1)=sqrt((m(:,3)+1)/2);
            q(:,2)=-m(:,2)./(2*q(:,1));
            q(:,3)=m(:,1)./(2*q(:,1));
            q(:,4)=0;
    %end
end