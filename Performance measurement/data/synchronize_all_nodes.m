function [data_out,Tdi]=synchronize_all_nodes(data_in,arI,maxdiff,syncInt)
%SYNCHRONIZE_ALL_NODES     Synchronizes all nodes by a correlation analysis
%
%   SYNCHRONIZE_ALL_NODES(DATA,I,MAXDIFF)
%
%   DATA_IN:    Is 1 x J cell array where each components contains a M x (Nj+1) matrix where the 
%               first Ni colums consists of data and the last column of
%               a time vector
%   DATA_OUT:   Is a M x (N1+...NJ+1) matrix where the first N1+.. + NJ colums consists of
%               synchronized data and the last column of a common time
%               vecotr
%   ARI:        A 1 x J cell array with indecis which point out the columns in DATA_IN that should 
%               be used for the correlation analysis that should be used for the
%               correlation analysis.
%   MAXDIFF:    The maximal allowed time difference between the first
%               sensor node and the other sensor nodes.
%   Tdi:        The computed time shift
%   SYNCINT     The time window to be used for syncronization
%
%
    N=length(data_in{1}(:,1));
    J=length(data_in);
    for j=1:(J-1)
        N=min(N,length(data_in{j+1}(:,1)));
    end

    %Make a low-pass version of the data that will be input to the
    %correlation analysis
    fs=1/(data_in{1}(2,end)-data_in{1}(1,end));
    cutfreq=0.1*fs/2;
    [B,A]=butter(8,cutfreq/(fs/2));
    
    C=zeros(N,J);
    for j=1:J %Iterate over all sensor nodes
       for i=arI{j} %Iterate over                      
            C(:,j)=C(:,j)+data_in{j}(1:N,i);
            %C(:,j)=C(:,j)+filtfilt(B,A,data_in{j}(1:N,i)-mean(data_in{j}(1:N,i)));  
       end      
       C(:,j)=C(:,j)/norm(C(:,j));
    end
    %C(syncInt,:);
    %C=1./(max(max(C))+C);
    %size(C)
    
    % Exclude first and last part of data
    %arN=floor(N/5):floor(4*N/5);
    if ~isempty(syncInt)
        arN=(floor((syncInt(1)*fs))+1):floor(syncInt(2)*fs);
    else
        arN=1:N;
    end
    C=C(arN,:);
    C=C.*repmat(hanning(length(arN)),1,J); % Put a window the data
    
    %col={'b','r','g'};
    %figure;
    %for j=1:J        
    %    plot(data_in{j}(arN,end),C(:,j),col{j});
    %    hold on
    %end
    %title('Syncronization data')    
    
    M=floor(fs*maxdiff);
    R=covf(C,M); % Do the correlation
    di=zeros(1,J);
    
    %Find the maximum correlation
    for j=2:J
        i=1;
        ij=i+(j-1)*J;
        ji=j+(i-1)*J;
        corr=[R(ji,M:-1:1) R(ij,2:M)];
        [r,ind]=max(corr);
        m=(-(M-1)):(M-1);
        di(j)=m(ind);
        %figure;
        %plot((1/fs)*m,corr)
        %title(['E x',num2str(i),'(t)*x',num2str(j),'(t+ti)'])
        %xlabel('ti [s]')
    end
    di=di-min(di);
    Tdi=di/fs; % Compute the time shift
    
    %Find the maximal common length of the data
    N=inf;
    for j=1:J
       N=min(N,length(data_in{j}(:,1))-di(j));
    end
    data_out=[];
    
    %Build the output
    t_new=(1/fs)*(0:(N-1))';
    for j=1:J
        X_new=data_in{j}(di(j)+(1:N),1:(end-1));
        data_out{j}=[X_new,t_new];               
    end
end