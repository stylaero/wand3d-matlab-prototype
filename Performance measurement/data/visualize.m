function visualize(str1,str2,scale,syncInt,titleStr)
    % str1 - Name of the data
    % str2 - Name of sensor network
    % scale - scale factor
    % syncInt - Time interval to syncronize
    saveIm=0;
    saveDat=0;
    
    close all
    load(str2)
    
    f_mag=sensor.SF; %Frequency of the magnetometer network
    f_vicon=250; %Frequency of the system

    % Load estimated data
    x_est=load([str1,'_xf.csv']);

    % Load reference data
    load([str1,'_vicon']);
    [x_nb,q_nb]=convert_vicon_data(x_ref);

    % The move the origen of the sensor platform down to the
    x_nb(:,3)=x_nb(:,3)+0.016; % Measure this!!
    x_est=x_est*scale;

    % Resample the reference data
    U=f_mag/gcd(f_mag,f_vicon);
    D=f_vicon/gcd(f_mag,f_vicon);

    % Resample data
    x_nb=resample_vec(x_nb,U,D);
    q_nb=resample_vec(q_nb,U,D);        
    
    % Remove the second when the sensor network was calibrating
    x_nb=x_nb(1*f_mag:end,:);        
    q_nb=q_nb(1*f_mag:end,:);
    
    % Data length of the estimated data
    Nest=size(x_est,1);

    % Data length of the reference data
    Nref=size(x_nb,1);

    % Augment the data
    x_ref=[x_nb,q_nb,(1/f_mag)*(0:(Nref-1))'];
    x_est=[x_est,(1/f_mag)*(0:(Nest-1))'];

    % Remove beginning and end
    x_ref=x_ref(2000:(end-1200),:);
    x_est=x_est(2000:(end-1200),:);

    % Do syncronization
    data_in{1}=x_ref;
    data_in{2}=x_est;    
    arI={[1 2 3],[1 2 3]}; % Syncronize for position
    data_out=synchronize_all_nodes_loop(data_in,arI,2,syncInt);

    % Extract data
    x_ref=data_out{1}(:,1:(end-1));
    x_est=data_out{2}(:,1:(end-1));
    t=data_out{1}(:,end);
    
    
    %Extract position
    pos_ref=x_ref(:,1:3);
    pos_est=x_est(:,1:3);
        
    %Extract orientation
    [r1_ref,r2_ref,r3_ref]=quat2angle(x_ref(:,4:7),'YXZ');       
    ori_ref=180/pi*unwrap([r1_ref r2_ref]);
    ori_ref2=quat2sphere(x_ref(:,4:7));
    ori_ref3=180/pi*unwrap([atan2(ori_ref2(:,2),norm(ori_ref2(:,[1 3]))),atan2(-ori_ref2(:,3),ori_ref2(:,1))]);
        
     x_est(:,7:9)=-x_est(:,7:9); % Change for the missalignment of the magnet
        
    [r1_est,r2_est,r3_est]=quat2angle(sphere2quat(x_est(:,7:9)),'YXZ');
    ori_est=180/pi*unwrap([r1_est r2_est]);    
   
    ori_est2=x_est(:,7:9)./repmat(sqrt(sum(x_est(:,7:9).^2,2)),1,3);
    ori_est3=180/pi*unwrap([atan2(ori_est2(:,2),norm(ori_est2(:,[1 3]))),atan2(-ori_est2(:,3),ori_est2(:,1))]);

    % Data length
    N=length(t);
    
    %% Visualize the result!
    
    % Plot the positions
    figure;
    plot(t,pos_ref,'linewidth',1)
    hold all
    plot(t,pos_est,'linewidth',1)    
    plot(t,pos_est/scale,':','linewidth',1)
    xlabel('Time [s]')
    ylabel('Position [m]')
    legend('x-position, VICON','y-position, VICON','z-position, VICON','x-position, 3DWand','y-position, 3DWand','z-position, 3DWand')
    title([titleStr,' - Position'])
    if saveIm==1
        print('-depsc', ['../figures/',str1,'_pos.eps'])
        system(['epstopdf ../figures/',str1,'_pos.eps']) 
    end
    if saveDat == 1
        dataSave = [t pos_ref pos_est];
        dataSave = dataSave(1:10:end,:);
        save([str1,'_pos.dat'],'-ascii','dataSave')
        th = network.th';
        save([str1,'_th.dat'],'-ascii','th')
    end
               
    
    %% Plot the trajectory
    figure;
    plot3(pos_ref(:,1),pos_ref(:,2),pos_ref(:,3));
    hold all
    plot3(pos_est(:,1),pos_est(:,2),pos_est(:,3));    
   
    for i=1:size(network.th,2)
        plot3(network.th(1,i),network.th(2,i),network.th(3,i),'r*')
    end    
    legend('Vicon','3DWand','Sensors')

    grid on
    axis equal
    xlabel('x-position [m]')
    ylabel('y-position [m]')
    zlabel('z-position [m]')
    title([titleStr,' - Trajectory'])        
    dataSave_est = [];
    dataSave_ref = [];
    for i=1:10:length(t)
        r = pos_est(i,:)';
        m = ori_est2(i,:)';
        m = m*0.05;
        R= 0.01;
        [X,Y,Z,C] = cylinder_my(r,m,R);        
        %surf(X,Y,Z)
        dataSave = [X(:) Y(:) Z(:)];
        dataSave_est = [dataSave_est, dataSave];
        %save(['patch/',str1,'_patch_est_',num2str(i),'.dat'],'-ascii','dataSave')
        r = pos_ref(i,:)';
        m = ori_ref2(i,:)';
        m = m*0.05;
        R= 0.01;
        [X,Y,Z,C] = cylinder_my(r,m,R);
        %surf(X,Y,Z)
        dataSave = [X(:) Y(:) Z(:)];
        dataSave_ref = [dataSave_ref, dataSave];
        %save(['patch/',str1,'_patch_ref_',num2str(i),'.dat'],'-ascii','dataSave')
    end
    if saveDat==1
        save([str1,'_patch_est.dat'],'-ascii','dataSave_est')
        save([str1,'_patch_ref.dat'],'-ascii','dataSave_ref')
    end
    
     %% Plot orientation
    figure;
    plot(t,ori_ref2,'linewidth',1)
    hold all
    plot(t,ori_est2,'linewidth',1)
    %plot(t,ori_est2,'linewidth',1)
    xlabel('Time [s]')
    ylabel('[]')
    legend('m_x, VICON','m_y, VICON','m_z, VICON','m_x, 3DWand','m_y, 3DWand','m_z, 3DWand')
    title([titleStr,' - Orientation'])
    
         %% Plot orientation
    figure;
    plot(t,ori_ref,'linewidth',1)
    hold all
    plot(t,ori_est,'linewidth',1)    
    xlabel('Time [s]')
    ylabel('[Degree]')
    legend('Ry, VICON','Rx, VICON','Ry, 3DWand','Rx, 3DWand')
    title([titleStr,' - Orientation'])
    
    if saveIm==1
        print('-depsc', ['../figures/',str1,'_ori.eps'])
        system(['epstopdf ../figures/',str1,'_ori.eps'])    
    end
    if saveDat == 1
        dataSave = [t ori_ref ori_est];
        dataSave = dataSave(1:10:end,:);
        save([str1,'_ori.dat'],'-ascii','dataSave')
        dataSave = [t ori_ref2 ori_est2];
        dataSave = dataSave(1:10:end,:);
        save([str1,'_ori2.dat'],'-ascii','dataSave')
    end
    
             %% Plot orientation
    figure;
    plot(t,ori_ref3,'linewidth',1)
    hold all
    plot(t,ori_est3,'linewidth',1)    
    xlabel('Time [s]')
    ylabel('[Degree]')
    legend('Pitch, VICON','Yaw, VICON','Pitch, 3DWand','Yaw, 3DWand')
    title([titleStr,' - Orientation'])    

    %%
    for j=1:2 % Iterate over position and orientation
        switch j
            case 1            
                x_est=1000*pos_est;
                x_ref=1000*pos_ref;
                str_x='pos';
                str_x2='position';
                str_unit='mm';
            case 2
                x_est=180/pi*ori_est2;
                x_ref=180/pi*ori_ref2;
                str_x='ori';
                str_x2='orientation';
                str_unit='degree';
        end
    
        %% RMSE as a function of time
        figure;
        x_error=x_est-x_ref; % Compute the error
        MSE=sum((x_error).^2,2);
        RMSE=sqrt(MSE);
        plot(t,RMSE)
        title([titleStr,' - RMSE of ',str_x2,' - function of time'])
        xlabel('Time [s]')
        ylabel(['RMSE [',str_unit,']'])

        %% Compute Bias and variance

        % Make a low pass filter with half the nyqvist frequency
        cutoff=10; % cutoff frequency
        Wn=cutoff/(f_mag/2);
        %Wn=0.1;
        [B,A] = butter(4,Wn);

        % Filter each axis
        x_error_low=zeros(size(x_est,1),3);    
        for i=1:3
            x_error_low(:,i)=filtfilt(B,A,x_error(:,i));
        end
        
        x_error_high = x_error - x_error_low;
        
        %% Plot x_error, x_error_low
        figure;
        plot(t,x_error)
        hold all
        plot(t,x_error_low)
        title(['Error and its lowpassed version -',str_x2])
        xlabel('Time [s]')
        ylabel(['Error [',str_unit,']'])    
        
        %% Plot fft
        figure;
        freq=t/t(end)*f_mag;
        Y=abs(fft(x_error));
        
        loglog(freq(1:floor(N/2)),Y(1:floor(N/2)))
        xlabel('Frequency [Hz]')

        %% Compute the variance multiply with fraction with is within [0 Wn]
        x_est_n=sum((x_error_high).^2,2)*(1/(1-Wn));        

        % Compute the bias^2 
        x_est_bias=max(MSE-x_est_n,eps);
        x_est_bias=sum((x_error_low).^2,2)-x_est_n*Wn;

        % Remove the begining and the end
        I=floor(1/20*N):floor(19/20*N);

        %figure;
        %plot(t(I),x_est_n(I))
        %hold all
        %plot(t(I),x_est_bias(I))
        %plot(t(I),MSE(I))
        %legend('Var','Bias','MSE')
        disp(['Bias [',str_unit,']'])
        bias=sqrt(mean(x_est_bias(I)));
        disp(bias)
        disp(['sqrt(Var) [',str_unit,']'])
        v=sqrt(mean(x_est_n(I)));
        disp(v)
        disp(['RMSE [',str_unit,']'])
        RMSE2=sqrt(mean(MSE(I)));
        disp(RMSE2)
        
        if j==1
            disp([num2str(RMSE2,'%.2f'),' & ',num2str(bias,'%.2f'),' & ',num2str(v,'%.2f'),' \\'])
        else
            disp([num2str(RMSE2,'%.2f'),'$^{\circ}$ & ',num2str(bias,'%.2f'),'$^{\circ}$ & ',num2str(v,'%.2f'),'$^{\circ}$ \\'])
        end
            

        %% RMSE as a function of distance
        figure(10+3*j);

        dist=sqrt(sum(pos_ref.^2,2));
        loglog(dist,RMSE)
        title([titleStr,' - RMSE of ',str_x2,' - function of distance'])
        xlabel('Distance to center of sensor network [m]')
        ylabel(['RMSE [',str_unit,']'])
        hold all
        if saveIm==1
            print('-depsc', ['../figures/',str1,'_',str_x,'_rmse.eps'])
            system(['epstopdf ../figures/',str1,'_',str_x,'_rmse.eps'])
        end

        %% Bias as a function of distance
        figure(11+3*j);


        semilogy(dist,sqrt(x_est_bias))
        title([titleStr,' - Bias of ',str_x2])
        xlabel('Distance to center of sensor network [m]')
        ylabel(['Bias [',str_unit,']'])        
        hold all
        if saveIm==1
            print('-depsc', ['../figures/',str1,'_',str_x,'_bias.eps'])
            system(['epstopdf ../figures/',str1,'_',str_x,'_bias.eps'])
        end

        %% Var as a function of distance
        figure(12+3*j);


        semilogy(dist,sqrt(x_est_n))
        title([titleStr,' - Standard deviation of ',str_x2])
        xlabel('Distance to center of sensor network [m]')
        ylabel(['sqrt(Variance) [',str_unit,']'])
        hold all
        if saveIm==1
            print('-depsc', ['../figures/','_',str_x,str1,'_std.eps'])
            system(['epstopdf ../figures/',str1,'_',str_x,'_std.eps'])            
        end
    end


    
    
end
