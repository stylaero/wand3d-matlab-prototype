
visualize('analog_ort','analog_setup',[],'Experiment 1a - Analog magnetometers')
visualize('analog_par','analog_setup',[],'Experiment 2a - Analog magnetometers')
visualize('analog_all','analog_setup',[0 12],'Experiment 3a - Analog magnetometers')


visualize('digital_ort','digital_setup',[],'Experiment 1b - Digital magnetometers');
visualize('digital_par','digital_setup',[],'Experiment 2b - Digital magnetometers')
visualize('digital_all','digital_setup',[0 15],'Experiment 3b - Digital magnetometers')

