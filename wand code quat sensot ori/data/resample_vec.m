function x_resample=resample_vec(x,U,D)
    % Resamples the data x
    [N,M]=size(x);
    Nred=N-mod(N,2*D);
    x_resample=zeros(Nred*U/D,M);
    for i=1:M % Iterate over all dimensions
        x_resample(:,i)=resample(x(1:Nred,i),U,D);
    end
end