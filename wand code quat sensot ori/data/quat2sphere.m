function m=quat2sphere(q)
    m=quatrotate(quatinv(q),[0 0 1]);
    %m=[2*q(:,1).*q(:,3)+2*q(:,2).*q(:,4),2*q(:,3).*q(:,4)-2*q(:,1).*q(:,2),q(:,1).^2-q(:,2).^2-q(:,3).^2+q(:,4).^2];
end