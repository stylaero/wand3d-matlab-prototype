function [X,Y,Z,C]=cylinder_my_q(r,q,size)
    % Formes a cylinder with at position r, rotated with quaternion q and with radius
    % R and height h               
    
    %Construct a circle with m as its normal
    XYZ = Qq(q/norm(q))*[size(1), -size(1), -size(1), size(1), size(1);
                size(2), size(2), -size(2), -size(2), size(2);
                size(3), size(3), size(3), size(3), size(3)]/2;
    
    XYZ2 = Qq(q/norm(q))*[size(1), -size(1), -size(1), size(1), size(1);
                size(2), size(2), -size(2), -size(2), size(2);
                -size(3), -size(3), -size(3), -size(3),-size(3)]/2;            
    X = [XYZ(1,:);XYZ2(1,:)]+r(1);
    Y = [XYZ(2,:);XYZ2(2,:)]+r(2);
    Z = [XYZ(3,:);XYZ2(3,:)]+r(3);    
    
    % Color the sections
    C=[ones(2,5)];
        
end