clear all

%% Sensor specific parameters

% The positions of the sensosrs, positive x - up, positive y - right, positive z into the screen. Origin in the center of the screen. (we could change this coordinate system, but this was how the sensors where aligned)
th1=[-0.152 ;0.088 ;0]; % These sensors are deployed behind the screen
th2=[0.152 ;0.088 ;0];    
th3=[0.152 ;-0.088 ;0];
th4=[-0.152 ;-0.088 ;0];
network.th=[th1,th2, th3, th4];
    for i=1:4 
        network.A(:,:,i)=eye(3); 
    end
    
sensor.SF=220; %The sampling frequency
sensor.bits=24; %Number of bits
sensor.range=[-5.6 5.6]; %The range in Guass

%% Tuning parameter
tuning.Q_acc=1; %Standard diviation for the process noise beeing the acceleration (m/s^2)
tuning.Q_ori=1; %Standard diviation for the process noise beeing the angular velocity (rad/s)
tuning.Q_gain=0.0000; %Standard diviation for the process noise on the sensor gain (Nominal value of the gain = 1, a change of 0.01 corresponds to ca 0.5 degrees Celsius)
tuning.tracking_volume=0.6*[-1 1 -1 1 0 1]'; %[XMIN XMAX YMIN YMAX ZMIN ZMAX] for the tracking volume of intrest, here a box infont of the screen.

% Parameters for adjusting the process noise dependent on residual.
tuning.residual_level=[0.05 0.2];
tuning.residual_level=[0.003 0.2];
tuning.noise_scaling=[4^(-2) 1 4^2];

%% Target specific parameters

K=2; %Number of tools
for k=1:K
    tool(k).dipole_strength=10*2.15e-4; % A upper limit of the strength for the magnetic dipole
    %tool(k).dipole(1).ori = [0 0 1]';
    %tool(k).dipole(1).pos = [0 0 1]';
    %tool(k).dipole(2).ori = [0 1 0]';
    %tool(k).dipole(2).pos = [0 0 -1]';
    
    % These are only for vizualization...
    tool(k).length=0.14; %Length of the pen
    tool(k).screen_touch=0.1; %Max distance from screen for painting
    tool(k).thickness=0.009; %Thickness of the pen
    tool(k).cal_width = 0.01; %Maximum width of the calligraphy pen
end
tool(1).dipole(1).ori = [0 0 1]';
tool(1).dipole(1).pos = [0 0 0.127/2]';
tool(1).dipole(2).ori = [0 1 0]';
tool(1).dipole(2).pos = [0 -0.006 -0.127/2]';
tool(2).dipole(1).ori = [0 1 0]';
tool(2).dipole(1).pos = [0 0 0.125/2]';
tool(2).dipole(2).ori = [0 -1 0]';
tool(2).dipole(2).pos = [0 0 -0.125/2]';

R = eye(12);
meas_zero = zeros(12,1)';

% Initialize the model
m=Model(sensor,tool,network,meas_zero,tuning,R);

x = randn(K*10,1);
x(end+1) = 1;


checkgrad(@(x) m.h(x),x,1e-8);

