function [X,Y,Z,C,paint]=drawer_my(xf,xf_last,target)
    % Formes a region which has been painted between two samples. The width of the reagion is decided by the angle of the pen.        
    % Rescale the dipole legnth such that it will be shorter when beeing
    % perpendicular to the screen (will be make it easier to draw thinner
    % lines). Set maximum width to cal_width.    
    m=target.cal_width*xf(7:9)/norm(xf(7:9))*(norm(xf(7:8))/norm(xf(7:9)));
    m_last=target.cal_width*xf_last(7:9)/norm(xf_last(7:9))*norm(xf_last(7:8))/norm(xf_last(7:9));
        
    % Calculate the position od the pen nib
    r=xf(1:3)-xf(7:9)/norm(xf(7:9))*target.length/2;
    r_last=xf_last(1:3)-xf_last(7:9)/norm(xf_last(7:9))*(target.length)/2;            
    
    % Compute the coordinates of the painted region, defined by the first two
    % components of m (projection of m onto the screen)
    X=[m(1) -m(1);m_last(1) -m_last(1)]+[r(1) r(1); r_last(1), r_last(1)];
    Y=[m(2) -m(2);m_last(2) -m_last(2)]+[r(2) r(2); r_last(2), r_last(2)];    
    Z=zeros(2,2);
    
    % Compute the color (which depends on the hight of the pen nib in
    % relative the screen.   
    C=([m(3) -m(3);m_last(3) -m_last(3)]+[r(3) r(3); r_last(3), r_last(3)]);
    caxis([0 target.screen_touch])
    
    % Check if pen nib is close enough to the screen for painting
    if abs(r(3)) < target.screen_touch
        paint=1;
    else
        paint=0;
    end    
end