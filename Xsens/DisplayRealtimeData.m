function DisplayRealTimeData(vargin,target,sensors,tuning)

%% DisplayRealTimeData(vargin);
%
% Real-time display of calibrated data or 3D orientation data from an MTi
% or MTx
%
% Input argument
% COMports [integer, integer, ...] to which MTs are connected
%
%
% Press key on keyboard while running:
%
% "Q" = Quit
% "Z" = Toggle Zoom in/out
% "M" = View only magnetometer data
% "T" = Pose tracker
% "O" = Color calligraphy
% "G" = Grey calligraphy
% "C" = Clear screen
% "D" = View all MTi / MTx data (default)
%
% Orientation data output MODE:
% "Q" = Quit 

% set default values
defaultValues={'caldata',2,1.0,1,1.0};
[DisplayMode,zoom_level_setting,filterSettings_gain,filterSettings_corr,filterSettings_weight]...
   =deal(defaultValues{:});


global mode_data %1: Read data from file 2: Read data from sensor

if isstr(vargin) %Input is a file name
    DATA=load(vargin);
    mode_data=1;
    N_imu=size(DATA,2)/9; %Each sensor has 9 measurement
    
else %Input is a vector of COM-ports
    COMports=vargin;
    mode_data=2;
end    


if mode_data==2 %Input is a vector of COM-ports
    % set up MTObj
    N_imu=length(COMports);
    for i=1:N_imu
        h(i)=actxserver('MotionTracker.FilterComponent');
    end
end

try
    % use try-catch to avoid the need to shutdown MATLAB if m-function fails (e.g. during debugging)
    % This is needed because MATLAB does not properly release COM-objects when
    % the m-function fails/crasehes and the object is not explicitly released with "delete(h)"
    
    if mode_data==2 %Input is a vector of COM-ports

        % call MT_SetCOMPort is required, unless using COM 1
        for i=1:N_imu
            h(i).MT_SetCOMPort(COMports(i));

            % request the Sample Frequency (update rate) of the MTi or MTx
            SF = h(i).MT_GetMotionTrackerSampleFrequency;

            % call MT_SetFilterSettings is optional
            h(i).MT_SetFilterSettings(filterSettings_gain,filterSettings_corr,filterSettings_weight);
            
            % request calibrated inertial and magnetic data
            h(i).MT_SetCalibratedOutput(1);
            
            h(i).MT_StartProcess; % start processing data
            
        end
        
    else %Data from file
        h=[]; %No sensors to handle
        SF=100; %Frequency
    end
    
    for i=1:N_imu
            Channels=10;
            % Define variables for data
            d_cell{i}=zeros(1,Channels);
            last_d_cell{i} = d_cell{i};
            odd_samples{i} = zeros(0,Channels);
    end
        
    
    %% Sensor Model
        
    % y_k = h(x_k) + e_k, where e_k ~ N(0,R) describes the dynamics of the
    % target, where y_k is the measurement, x_k the state of the system and
    % e_k measurment noise
    
    % We model the target as a magnetic dipole. Therefor, here we define the dipole model. This models the magnetic field as position r
    % relative to a magnetic dipole with dipole moment m. For reference see
    % for example 
    %
    % [1] J. D. Jackson. Classical Electrodynamics. John Wiley and Sons, Inc., 2 edition, 1975.
    %
    % or more related to this application
    %
    %[T1] N. Wahlstr�m, Target Tracking using Maxwell's Equations. Master's Thesis. Presented June 15, 2010.    
    dipole=@(r,m) (3*(r'*m)*r-(r'*r)*m)/((r'*r)^(5/2));
    
    % We use 9 components state-vecto. x(1:3) encodes the position of the
    % target, x(4:6) th velocity and x(7:9) the orientation. Here the
    % orientation is encoded with the dipole vector m.
    
    % Using spatially distributed sensors we will measure the magnetic
    % field at diffeent positions relative to the dipole. Since the sensor
    % i is located at th(i,:), the relative psotions will be
    % x(1:3)-th(:,i). This gives the following sensor model:
    
    th=sensors.th;
    stracc='';
    for i=1:N_imu
        str=['dipole(x(1:3)-th(:,',num2str(i),'),x(7:9))'];
        stracc=[stracc,str];
        if i<N_imu;
           stracc=[stracc,';']; 
        end
    end
    eval(['m.h=@(x) [',stracc,'];']);
    
    % Contruct the covariance matrix of the measurment noise
    m.R=sensors.noise^2*eye(3*N_imu);
    
    
    %% Motion model
    
    % x_{k+1} = A*x_k + B*w_k, where w_k ~ N(0,Q) describes the dynamics of the target
    %
    % Assume a (nearly) constant velocity model fo the position and a (nearly) constant position model for the orientation
    % This gives the following entries, see for example 
    %
    % [3] F. Gustafsson. Statistical Sensor Fusion. Studentlitteratur, 1 edition, 2010.
    
    m.A=@(T) blkdiag([eye(3) T*eye(3);zeros(3,3) eye(3)],eye(3));
    m.B=@(T) blkdiag([(T^2/2)*eye(3);T*eye(3)],T*eye(3));
    m.Q=blkdiag(tuning.Q_vel^2*eye(3), tuning.Q_ori^2*eye(3));
    
   
    %% Initial guess of the state    
    m.x0=[0;0;0;0;0;0;0.01*randn(3,1)];
    m.xP0=diag([1 1 1 0.1 1 1 0.001 0.001 0.001]);
    m.Qs=1;
    
    %% Other initializations        
    xp=m.x0;
    Pp=m.xP0;
    xf=xp;
    Pf=Pp;
    
    %Initialize time duration fo a certain plot    
    t_acc=0;

    %Initialize filtered value of the measurement
    y_filt=zeros(3*N_imu,1);
    
    
    if mode_data==2 %Data comes from the COM-ports
        %Wait a while
        pause(0.5)

        %% Compute the stationary offset for sensor (due to earth magnetic field)
        for i=1:N_imu 
            meas=MT_get_data(h(i), Channels); % retreive data from MTObj object
            zero_d_cell{i}=mean(meas); %Compute the offsetfor all sensors
        end

        sample_length=zeros(1,N_imu);

        %% Output File - IMU
        % Create an output file and open it
        dumpfile = sprintf('data/%s_meas.csv', datestr(now(), 30));
        fpIMU = fopen(dumpfile, 'w');
        if fpIMU < 0
            error('Could not open output file.');
        end
    else
        fpIMU=[];
    end
    
    % init figure plotting variables
    % set time scale zoom level
    zoom_levels=[12*SF,8*SF,4*SF]; % in seconds
    zoom_level=zoom_levels(zoom_level_setting);
    OldFigureUserData = [0 0 0 0]; status = 0; last_t=0;    
    % default vertical zoom
    YLims = [-22 22; -1200 1200; -1.8 1.8; -1 1];
    ResetFigure =1; first_time=1;
    % create figure
    % NOTE, the releasing of MTObj is done in the Figure Close Request function
    % "mt_figure_close"
    [f_handle, p_handle] = mt_create_figure(OldFigureUserData(3), -1 ,h, YLims, SF,...
        zoom_level, OldFigureUserData,th,fpIMU);
        
        % set figure UserData to appropriate values (format:
        % [MT_being_plotted, Zoom, PlotType])
        set(f_handle,'UserData', [0 0 0 0]) 
    
    tic %Start counting time
    TIME=0; %Starting time
    
    
    % Now we can start looping over oll samples!!       
    while ishandle(f_handle) % check if all objects are alive
        if mode_data==2
            for i=1:N_imu % retreive data from MTObj object
                [meas,s,N]=MT_get_data(h(i), Channels); % the measurement meas is on th form [acc_x acc_y acc_z gyro_x gyro_y gyro_z mag_x mag_y mag_z], however in this application we will on�y use the magnetometer measurement!
                
                if s
                    meas=meas-repmat(zero_d_cell{i},N,1); %Subtract earth magnetic field
                    odd_samples{i}=[odd_samples{i};meas]; %Add the odd samples from last iteration
                    status(i)=1;
                end
                sample_length(i)=size(odd_samples{i},1); %Compute the number of samples from sensor i that has not yet been processed
            end

            N=min(sample_length); %Compute the minimum number of available samples


                for i=1:N_imu %For each sensor, pick only N samples for each. The reamaining samples we save for next iteration.
                    d_cell{i}=odd_samples{i}(1:N,:);
                    odd_samples{i}=odd_samples{i}((N+1):end,:);
                end

                for n=1:N %Iterate over all samples and save them to a file
                    str='%d, %d, %d, %d, %d, %d, %d, %d, %d,';
                    str_acc='';
                    data_acc=[];
                    for i=1:N_imu
                        str_acc=[str_acc,str];
                        data_acc=[data_acc,double(d_cell{i}(n,1:9))];

                    end
                    fprintf(fpIMU, [str_acc,' \n\r'],double(data_acc));
                end
        else
            TIME_new=toc;
            IND=((floor(TIME*SF)+1):floor(TIME_new*SF));
            TIME=TIME_new;
            if max(IND)>size(DATA,1);
                break;
            end
            for i=1:N_imu
                d_cell{i}=DATA(IND,(1:9)+(i-1)*9);
            end
            N=length(IND);
        end
        % Now the data is available! The rest of the code is only to
        % display the data and to make it look nice and smooth on display....

        if N > 0, % If there are samples to proces, go further

            %% Filter
            for n=N %Do a filter update for the last sample in the iteration (would take too long time to proces all samples on my laptop)
                % create measurement
                y=zeros(3*N_imu,1);
                for i=1:N_imu
                    y((3*(i-1)+1):3*i)=d_cell{i}(n,7:9)';
                end
                last_xf=xf;       
                
                % To the filter step!!
                [xf,Pf,xp,Pp]=ekfStep(m,y,xp,Pp,N/SF,target,tuning);                
                
                % Change the process noise dependent on the stationarity of
                % the measurement
                a=0.8;
                y_filt=a*y_filt+(1-a)*y;
                res=norm(y_filt-y);
                
                if res < tuning.residual_level(1)
                    m.Qs=tuning.noise_scaling(1);
                    %disp('low')
                elseif res < tuning.residual_level(2)
                    m.Qs=tuning.noise_scaling(2);
                    %disp('normal')
                else
                    m.Qs=tuning.noise_scaling(3);
                    %disp('HIGH')
                end
            end
                        
            %% Vizualize the result!!!
            
            % retrieve values from figure
            CurrentFigureUserData = get(f_handle,'UserData'); 
            
            % create timebase            
            t=[last_t (last_t+(1:N)/SF)];
            last_t=t(end);
            T=t(end)-t(1);
            t_acc=t_acc+T;
            
            if ResetFigure ==1, % check if figure should be reset
                last_t=0; % wrap plot around
                figureUserData = get(f_handle,'UserData');
                % call local function to (re)setup figure
                [f_handle, p_handle, a_handle] = mt_create_figure(CurrentFigureUserData(3), f_handle,h, YLims, SF, zoom_level, figureUserData,th,fpIMU);
                ResetFigure = 0;
                t_acc=10;
            end
            
            % check if figures should be reset                           
            if any(CurrentFigureUserData(1:3) ~= OldFigureUserData(1:3)), % check if figure UserData changed by KeyPress
                ResetFigure =1; % make sure plot is reset
                first_time =1; % re-initialize zoom levels too
                
            elseif ((last_t*SF>zoom_level)&&((CurrentFigureUserData(3) == 0)||(CurrentFigureUserData(3) == 1)))|| (first_time==1) || (CurrentFigureUserData(4)==1)
                ResetFigure =1; % make sure plot is reset
                first_time =0;
                tmp=get(f_handle,'UserData');
                set(f_handle,'UserData',[tmp(1:3) 0]);
                [YLims, t_acc] = mt_plot_data(d_cell, last_d_cell, t, CurrentFigureUserData, f_handle,p_handle,a_handle,xf,last_xf,t_acc,target);
            else % other wise --> plot
                % plot the data using a local funtion                
                    [YLims, t_acc] = mt_plot_data(d_cell, last_d_cell, t, CurrentFigureUserData, f_handle,p_handle,a_handle,xf,last_xf,t_acc,target);
            end
            
            %Save values for next iteration
            OldFigureUserData = CurrentFigureUserData;
            for i=1:N_imu
                last_d_cell{i}=d_cell{i}(end,:);
            end
            status=zeros(1,3);
            
        elseif status>1,
            % MTObj not correctly configured, stopping
            [str_out]=MT_return_error(status);
            disp(str_out);
            disp('MTObj not correctly configured, stopping.....');
            break
        end % if

    end % while

    % release MTObj is done on figure close...not here
    if ishandle(f_handle), % check to make sure figure is not already gone
        close(f_handle)
    end

catch % try catch for debugging
    % make sure MTObj is released even on error
    if ~isempty(h) %Data comes from the COM-ports
        for i=1:length(h)
            h(i).MT_StopProcess;
            delete(h(i));
            clear h(i);
        end
    end
    % display some information for tracing errors
    disp('Error was catched by try-catch.....MTobj released')
    crashInfo = lasterror; % retrieve last error message from MATLAB
    disp('Line:')
    crashInfo.stack.line
    disp('Name:')
    crashInfo.stack.name
    disp('File:')
    crashInfo.stack.file
    rethrow(lasterror)
end
% -------------------------------------------------------------------------
% end of function MT_DisplayRealtimeData(varargin);





%% -------------------------------------------------------------------------
% LOCAL FUNCTIONS
% -------------------------------------------------------------------------
function [f_handle, p_handle, a_handle] = mt_create_figure(type, f_handle, h, YLims,SF, zoom_level, figureUserData, th,fpIMU)

% local function to create the figure for real time plotting of data.
% accepts plot type information for custom plots
%
% if figure does not yet exist enter -1 in figure_handle input

if ~ishandle(f_handle),
    f_handle = figure('Name','Real-time display of MTi or MTx data','NumberTitle','off');
end

fontSizeUsed = 12;

% init
p_handle = zeros(12); a_handle = zeros(12);

switch type
    case 0% calibrated inertial and magnetic data(default)
        for i=1:9,
            subplot(3,3,i), p_handle(i)=plot(0,0,'EraseMode','none');a_handle(i) = gca; axis(a_handle(i),[0 (zoom_level+1)./SF YLims(mod(i-1,3)+1,:)]);
            grid on;
        end
        tlh = title(a_handle(1),['Accelerometer']);   set(tlh,'FontSize',fontSizeUsed);   xlh = xlabel(a_handle(7),'time [s]');  ylh = ylabel(a_handle(1),'X_S');
        tlh = title(a_handle(2),['Gyro']);           set(tlh,'FontSize',fontSizeUsed);   xlh = xlabel(a_handle(8),'time [s]');  ylh = ylabel(a_handle(4),'Y_S');
        tlh = title(a_handle(3),['Magnetometer']);    set(tlh,'FontSize',fontSizeUsed);   xlh = xlabel(a_handle(9),'time [s]');  ylh = ylabel(a_handle(7),'Z_S');
        
        
        
    case 1 % 'mag' (only magnetometers)
        
        for i=1:12
            subplot(3,4,i), p_handle(i)=plot(0,0,'EraseMode','none');a_handle(i) = gca; axis(a_handle(i),[0 (zoom_level+1)./SF YLims(mod(i-1,4)+1,:)]); grid on; 
        end
        
        tlh = title(a_handle(1),['Magnetometer [a.u.]']);   set(tlh,'FontSize',fontSizeUsed);   xlh = xlabel(a_handle(9),'time [s]');  ylh = ylabel(a_handle(1),'X_S');
        tlh = title(a_handle(2),['Position']);           set(tlh,'FontSize',fontSizeUsed);   xlh = xlabel(a_handle(10),'time [s]');  ylh = ylabel(a_handle(5),'Y_S');
        tlh = title(a_handle(3),['Velocity']);    set(tlh,'FontSize',fontSizeUsed);   xlh = xlabel(a_handle(11),'time [s]');  ylh = ylabel(a_handle(9),'Z_S');
        tlh = title(a_handle(4),['Dipole moment']);    set(tlh,'FontSize',fontSizeUsed);   xlh = xlabel(a_handle(12),'time [s]');
           
    case 2 % Plot pose tracker
            subplot(1,1,1); p_handle(1)=surf(zeros(2,2),zeros(2,2),zeros(2,2),'EraseMode','normal');%hold on;plot3(th(1),th(2),th(3),'b*','EraseMode','none');plot3(-th(1),-th(2),-th(3),'r*','EraseMode','none');
            a_handle = gca; axis(a_handle,[YLims(2,:) YLims(2,:) YLims(2,:)]); 
            
            %Fix the position and angle of the camera
            pos_t=[0,0,-8];
            campos(pos_t);
            ang=get(a_handle,'CameraViewAngle');
            camva(ang/11.5);
            set(a_handle,'CameraUpVector',[1 0 0])
            set(f_handle,'units','normalized','outerposition',[0 0 1 1])
            
            %Make a grid
            grid on;
            tick=-0.20:0.1:0.20;
            set(a_handle,'XTick',tick);set(a_handle(1),'YTick',tick);set(a_handle,'ZTick',tick);
            
            %Labels
            xlabel(a_handle,'X_S');
            ylabel(a_handle,'Y_S');
            zlabel(a_handle,'Z_S');
            
            %The colormap
            colormap([0.8 0.8 0.8;0 1 0])
                 
    case 3  %Color calligraphy
            subplot(1,1,1); 
            p_handle(1)=surf(zeros(2,2),zeros(2,2),zeros(2,2),'EraseMode','none');
            a_handle(1) = gca; axis(a_handle(1),[YLims(2,:) YLims(2,:) YLims(2,:)]); 
            
            %Fix the position and angle of the camera
            pos_t=[0,0,-8];
            campos(pos_t);
            ang=get(a_handle(1),'CameraViewAngle');
            camva(ang/11.5);
            set(a_handle(1),'CameraUpVector',[1 0 0])
            set(f_handle,'units','normalized','outerposition',[0 0 1 1])
            
            %Make grid
            grid on;
            tick=-0.20:0.1:0.20;
            set(a_handle(1),'XTick',tick);set(a_handle(1),'YTick',tick);set(a_handle(1),'ZTick',tick);
            
            %Lables
            xlabel(a_handle(1),'X_S');
            ylabel(a_handle(1),'Y_S');
            zlabel(a_handle(1),'Z_S');
            tlh = title(a_handle(1),['Pose tracker']); set(tlh,'FontSize',fontSizeUsed);           
            
            %Colormap
            shading interp 
            colormap(hsv(128))
            caxis([0 1])

            
   case 4 % Grey calligraphy            
            subplot(1,1,1); p_handle(1)=surf(zeros(2,2),zeros(2,2),zeros(2,2),'EraseMode','none');
            a_handle(1) = gca; axis(a_handle(1),[YLims(2,:) YLims(2,:) YLims(2,:)]); 
            
            %Fix the position and angle of the camera
            pos_t=[0,0,-8];
            campos(pos_t);
            ang=get(a_handle(1),'CameraViewAngle');
            camva(ang/11.5);
            set(a_handle(1),'CameraUpVector',[1 0 0])
            set(f_handle,'units','normalized','outerposition',[0 0 1 1])
            
            %Make grid
            grid on;
            tick=-0.20:0.1:0.20;
            set(a_handle(1),'XTick',tick);set(a_handle(1),'YTick',tick);set(a_handle(1),'ZTick',tick);
            
            %Labels
            xlabel(a_handle(1),'X_S');
            ylabel(a_handle(1),'Y_S');
            zlabel(a_handle(1),'Z_S');
            tlh = title(a_handle(1),['Pose tracker']); set(tlh,'FontSize',fontSizeUsed);           
            
            %Colormap
            shading interp            
            colormap(gray(128))
end

set(f_handle,'CloseRequestFcn',{@mt_figure_close,h,f_handle,fpIMU});
set(f_handle,'KeyPressFcn',{@mt_figure_keypress,f_handle});
set(f_handle,'UserData', figureUserData);
% -------------------------------------------------------------------------
% end of function


%% -------------------------------------------------------------------------
function mt_figure_keypress(obj, eventdata, f_handle)

% local function to handle KeyPress events on figure.
% (D)efault display, (M)agnetometer only, Pose
% (T)racker, c(O)lor calligraphy, (G)rey calligraphy
%
% envoked when a key is pressed when figure is in focus

in_key=lower(get(f_handle,'CurrentCharacter'));
tmp = get(f_handle,'UserData');

switch in_key
    case 'z' % toggle zoom mode
        pause(0.2)
        figure(f_handle)
        if tmp(2) == 0, % check zoom level
            set(f_handle,'UserData',[tmp(1) 1 tmp(3) 0]); % toggle to next zoom mode
        else
            set(f_handle,'UserData',[tmp(1) 0 tmp(3) 0]); % toggle to default zoom mode
        end
        
    case 'd'
        disp('Switching the default display mode, all 9 data streams...')
        pause(0.2)
        figure(f_handle)
        set(f_handle,'UserData',[tmp(1:2) 0 0]); % set to default mode

    case 'm'
        disp('Switching to display only 3D magnetometer data stream...')
        pause(0.2)
        figure(f_handle)
        set(f_handle,'UserData',[tmp(1:2) 1 0]); % set to defult mode

    case 't'
        disp('Switching to display the pose tracker')
        pause(0.2)
        figure(f_handle)
        set(f_handle,'UserData',[tmp(1:2) 2 0]); % set to pose tracker mode
       
     case 'o'
        disp('Switching to display the color calligraphy')
        pause(0.2)
        figure(f_handle)
        set(f_handle,'UserData',[tmp(1:2) 3 0]); % set to color calligraphy mode
  
     case 'g'
        disp('Switching to display the grey calligraphy')
        pause(0.2)
        figure(f_handle)
        set(f_handle,'UserData',[tmp(1:2) 4 0]); % set to color calligraphy mode 
               
    case 'c'
        disp('Clear graph')
        pause(0.2)
        set(f_handle,'UserData',[tmp(1:3) 1]); % set to mag mode
        
    case 'q'
        disp('Quitting demo MT_DisplayRealtimeData...')
        pause(0.2)
        figure(f_handle)
        close(f_handle)
 
    otherwise
        disp('Unknown command option....displaying help data')
        disp(' ')
        eval('help MT_DisplayRealtimeData')

end

%% -------------------------------------------------------------------------
function [YLims, t_acc] = mt_plot_data(d_cell, last_d_cell, t, CurrentFigureUserData, f_handle,p_handle, a_handle, xf, last_xf,t_acc,target)

% local function to plot the data using "low-level" set fucntions for smooth plotting
    N_imu=length(d_cell);
    style={'-',':','--'};
    col={'b','r','g'};
    
switch CurrentFigureUserData(3) % check plot type
        case 0 %default

        if CurrentFigureUserData(2), % check if zoomed
            band = [0.8 50 0.1]; % define zoom range
            i=1;
            YLims = [min(d_cell{i}(1,1:3))-band(1) max(d_cell{i}(1,1:3))+band(1); min(d_cell{i}(1,4:6)./pi.*180)-band(2) max(d_cell{i}(1,4:6)./pi.*180)+band(2);...
                min(d_cell{i}(1,7:9))-band(3) max(d_cell{i}(1,7:9))+band(3)];
        else % default values of zoom (full range of MT9)
            YLims = [-22 22; -1200 1200; -0.01 0.01;-1 1];
        end

        %     plot the data
    for i=1:N_imu
        set(p_handle(1),'XData',t,'YData',[last_d_cell{i}(1,1) d_cell{i}(:,1)'],'Color',col{i},'LineWidth',2)
        set(p_handle(4),'XData',t,'YData',[last_d_cell{i}(1,2) d_cell{i}(:,2)'],'Color',col{i},'LineWidth',2)
        set(p_handle(7),'XData',t,'YData',[last_d_cell{i}(1,3) d_cell{i}(:,3)'],'Color',col{i},'LineWidth',2)

        % convert the rate of turn data to deg/s instead of rad/s
        set(p_handle(2),'XData',t,'YData',([last_d_cell{i}(1,4) d_cell{i}(:,4)'])./pi.*180,'Color',col{i},'LineWidth',2)
        set(p_handle(5),'XData',t,'YData',([last_d_cell{i}(1,5) d_cell{i}(:,5)'])./pi.*180,'Color',col{i},'LineWidth',2)
        set(p_handle(8),'XData',t,'YData',([last_d_cell{i}(1,6) d_cell{i}(:,6)'])./pi.*180,'Color',col{i},'LineWidth',2)

        set(p_handle(3),'XData',t,'YData',[last_d_cell{i}(1,7) d_cell{i}(:,7)'],'Color',col{i},'LineWidth',2)
        set(p_handle(6),'XData',t,'YData',[last_d_cell{i}(1,8) d_cell{i}(:,8)'],'Color',col{i},'LineWidth',2)
        set(p_handle(9),'XData',t,'YData',[last_d_cell{i}(1,9) d_cell{i}(:,9)'],'Color',col{i},'LineWidth',2)
    end

    case 1 % Only magnetometers

        if CurrentFigureUserData(2), % check if zoomed
            band = 0.1; % define zoom range (in a.u.)
            i=1;
            YLims = [min(d_cell{i}(:,7))-band max(d_cell{i}(:,7))+band; min(d_cell{i}(:,8))-band max(d_cell{i}(:,8))+band; min(d_cell{i}(:,9))-band max(d_cell{i}(:,9))+band];
        else % default values of zoom (full range of MT9)
            YLims = [-1 1; -0.7 0.7; -0.1 0.1; -0.004 0.004];
        end

        %     plot the data
        for i=1:N_imu
            set(p_handle(1),'XData',t,'YData',[last_d_cell{i}(1,7) d_cell{i}(:,7)'],'Color',col{i},'LineStyle',style{i},'LineWidth',2)
            set(p_handle(5),'XData',t,'YData',[last_d_cell{i}(1,8) d_cell{i}(:,8)'],'Color',col{i},'LineStyle',style{i},'LineWidth',2)
            set(p_handle(9),'XData',t,'YData',[last_d_cell{i}(1,9) d_cell{i}(:,9)'],'Color',col{i},'LineStyle',style{i},'LineWidth',2)
        end
        
        set(p_handle(2),'XData',t([1 end]),'YData',[last_xf(1) xf(1)'],'Color','b','LineWidth',2)
        set(p_handle(6),'XData',t([1 end]),'YData',[last_xf(2) xf(2)'],'Color','g','LineWidth',2)
        set(p_handle(10),'XData',t([1 end]),'YData',[last_xf(3) xf(3)'],'Color','r','LineWidth',2)

        set(p_handle(3),'XData',t([1 end]),'YData',[last_xf(4) xf(4)'],'Color','b','LineWidth',2)
        set(p_handle(7),'XData',t([1 end]),'YData',[last_xf(5) xf(5)'],'Color','g','LineWidth',2)
        set(p_handle(11),'XData',t([1 end]),'YData',[last_xf(6) xf(6)'],'Color','r','LineWidth',2)

        set(p_handle(4),'XData',t([1 end]),'YData',[last_xf(7) xf(7)'],'Color','b','LineWidth',2)
        set(p_handle(4),'XData',t([1 end]),'YData',[norm(last_xf(7:9)) norm(xf(7:9))],'Color','k','LineWidth',2)
        set(p_handle(8),'XData',t([1 end]),'YData',[last_xf(8) xf(8)'],'Color','g','LineWidth',2)
        set(p_handle(12),'XData',t([1 end]),'YData',[last_xf(9) xf(9)'],'Color','r','LineWidth',2)
        
    case 2 % Pose tracker

        YLims = [-0.5 0.5; -0.7 0.7; -0.1 0.1; -0.003 0.003];

        % Change the size of the cylinder dependent on the distance
        scale=0.9/(1+xf(3));
        
        % Construct the cylinder
        [X,Y,Z,C]=cylinder_my(xf(1:3),-xf(7:9)/norm(xf(7:9))*0.14*scale,0.009*scale);
        
        % Update the plot handle
        set(p_handle(1),'XData',X,'YData',Y,'ZData',Z,'CData',C)       

        
    case 3 % Color calligraphy
        
        YLims = [-0.5 0.5; -0.7 0.7; -0.1 0.1; -0.003 0.003];
                
        %Compute the coordinates of painted region
        [X,Y,Z,C,paint]=drawer_my_color(xf,last_xf,target);
        
        % Decide if the pen is close enough to the screen        
        if paint
            set(p_handle(1),'XData',X,'YData',Y,'ZData',Z,'CData',C)
        end
        
        if norm(d_cell{1}(:,7:9))>3
            text(0,0,0,'Ej f�r n�ra sensor 1!','FontSize',22)
        end
        if norm(d_cell{2}(:,7:9))>3
            text(0,0,0,'Ej f�r n�ra sensor 2!','FontSize',22)
        end
        
    case 4 % Grey calligraphy
        
        YLims = [-0.5 0.5; -0.7 0.7; -0.1 0.1; -0.003 0.003];
        
        shading interp
                
        [X,Y,Z,C,paint]=drawer_my(xf,last_xf,target);
        
        % Decide if the pen is close enough to the screen         
       
        if paint
            set(p_handle(1),'XData',X,'YData',Y,'ZData',Z,'CData',C)
        end
        
        %Don't apply too large magnetic field, it might harm the sensor
        if norm(d_cell{1}(:,7:9))>3
            text(0,0,0,'Ej f�r n�ra sensor 1!','FontSize',22)
        end
        if norm(d_cell{2}(:,7:9))>3
            text(0,0,0,'Ej f�r n�ra sensor 2!','FontSize',22)
        end
        
end % switch

% flush the graphics to screen
drawnow
% -------------------------------------------------------------------------
% end of function


%% -------------------------------------------------------------------------
function mt_figure_close(obj, eventdata, h, f_handle,fpIMU)

if ~isempty(h) %Data comes from the COM-ports
    % local function to properly release MTObj when the user kills the figure window.

    % release MTObj
    for i=1:length(h)
        MT_StopProcess(h(i))
        delete(h(i));
        clear h(i);
    end
    fclose(fpIMU)
end

% kill figure window as requested
delete(f_handle)
% -------------------------------------------------------------------------
% end of function
