function [xf,Pf,xp,Pp,yf,yPf,res]=ekfStep(m,yk,xp,Pp,T,target,tuning)
       % Implements a standard Extended kalman filter, here with a linear
       % dynamics and a couple of projections steps for increasing the
       % robustnes of the estimation, for reference see for example 
       % [3] F. Gustafsson. Statistical Sensor Fusion. Studentlitteratur, 1 edition, 2010.
       
       % m - The model
       % yk -measurement
       % yf -measurement, filter
       % yPf -measurement, filter covariance       
       
       % xp - state, prediction
       % xp - state, prediction covariance
       
       % xf - state, filter
       % Pf - state, filter covariance
       
       % res - residual
       % T - sample time
       
       
       % Linearization
       C=numgrad(m.h,1,xp);
       
       % Evaluation
       R=m.R; %Measurement noise
       Q=m.B(T)*m.Q*m.B(T)'*m.Qs^2; %Process noise
       A=m.A(T); %System matrix
       
       %% Measurement update
       yphat=m.h(xp);
       S=C*Pp*C'+R;
       K=Pp*C'*inv(S);
       
       % State
       xf=xp+K*(yk-yphat);              
       Pf=Pp-K*C*Pp;
       Pf=0.5*(Pf+Pf');
       
       %Measurement
       yf=m.h(xf);       
       yPf=C*Pf*C'+R;
       res=norm(yk-yf);
       
       %% Projection step
       % Thise steps ae not included in the standard EKF but will decrease
       % the risk of divergence of the filter for this particulary setup
       
       % Make a projection of the magnetic dipole moment onto a sphere.       
       if norm(xf(7:9)) > target.dipole_strength
        xf(7:9)=xf(7:9)*target.dipole_strength/norm(xf(7:9));
       end
              
       % Project the position of the magnetic dipole onto a the tracking
       % volume of intrest, here given by a box
       
       lim=tuning.tracking_volume;
       if lim(1) > xf(1) || lim(2) < xf(1) || lim(3) > xf(2) || lim(4) < xf(2) || lim(5) > xf(3) || lim(6) < xf(3)
           % Project the position onto the bo.
           xf(1:3)=min(max(xf(1:3),lim([1 3 5])),lim([2 4 6]));
           xf(4:6)=zeros(3,1); %Set velocity to zero if on the boundery of the box.
       end
             
       %% Time update       
       xp=A*xf;
       Pp=A*Pf*A'+Q;

end