% Run this file to define the parameters and un the demo!!


%% Sensor specific parameters
sensors.noise=0.005; % Standard diviation of the measurement noise for the sensors

% The positions of the sensosrs, positive x - up, positive y - right, positive z into the screen. Origin in the center of the screen. (we could change this coordinate system, but this was how the sensors where aligned)
th1=[0.01;-0.14;0.04]; % These sensors are deployed behind the screen
th2=[0.01;0.11;0.03];    
sensors.th=[th1,th2]; 

%% Target specific parameters
target.dipole_strength=3e-3; % The strength of the magnetic dipole

% These are only for vizualization...
target.length=0.14; %Length of the pen
target.screen_touch=0.05; %Max distance from screen for painting
target.cal_width = 0.01; %Maximum width of the calligraphy pen

%% Tuning parameter
tuning.Q_vel=0.01; %Standard diviation for the process noise on the velocity
tuning.Q_ori=0.001; %Standard diviation for the process on the orientation
tuning.tracking_volume=[-0.2 0.2 -0.2 0.2 -0.5 0]'; %[XMIN XMAX YMIN YMAX ZMIN ZMAX] for the tracking volume of intrest, here a box infont of the screen.

% Parameters for adjusting the process noise dependent on residual.
tuning.residual_level=[0.05 0.2];
tuning.noise_scaling=[0.1 1 3];

%% Run the demo!!

file = 'data\20120123T111659_meas.csv'; % In this scenario the pen is moved along the border of the screen and the moved to the center toing a couple of turns.
%file = 'data\20120124T213709_meas.csv'; % In this scenario we try to draw a face using the pen!

%Run the demo using recorded measurement
DisplayRealtimeData(file,target,sensors,tuning)

%Run the demo in real time using data from the sensors on COM-port 5 and 8
%DisplayRealtimeData([5 8],target,sensors,tuning)

% These are the commands!!
% "Q" = Quit
% "Z" = Toggle Zoom in/out
% "M" = View only magnetometer data
% "T" = Pose tracker
% "O" = Color calligraphy
% "G" = Grey calligraphy
% "C" = Clear screen
% "D" = View all MTi / MTx data (default)