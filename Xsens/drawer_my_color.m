function [X,Y,Z,C,paint]=drawer_my_color(xf,xf_last,target)
    % Formes a region which has been painted between two samples. The angle
    % in y-direction decides the color and the angle in x-direction the
    % width
    
    % Rescale the dipole legnth such that it will be shorter when beeing
    % perpendicular to the screen (will be make it easier to draw thinner
    % lines).
    m=xf(7:9)/norm(xf(7:9))*norm(xf(7:8))/norm(xf(7:9));
    m_last=xf_last(7:9)/norm(xf_last(7:9))*norm(xf_last(7:8))/norm(xf_last(7:9));
    
    % Extract the position
    r=xf(1:3)-xf(7:9)/norm(xf(7:9))*target.length/2;
    r_last=xf_last(1:3)-xf_last(7:9)/norm(xf_last(7:9))*target.length/2;
    
    % Extract a vector orthogonal to the velocity and the screen
    vort=[xf(5);-xf(4)];
    vort_last=[xf_last(5);-xf_last(4)];
    
    % Compute the width of the painted region
    w=target.cal_width*(m(1)/norm(m([1 3])))*vort/norm(vort)/2;   
    w_last=target.cal_width*(m_last(1)/norm(m_last([1 3])))*vort_last/norm(vort_last)/2;
    
    % Compute the coordinates of the painted region, defined by the first two
    % components of m (projection of m onto the screen)
    X=[w(1) -w(1);w_last(1) -w_last(1)]+[r(1) r(1); r_last(1), r_last(1)];
    Y=[w(2) -w(2);w_last(2) -w_last(2)]+[r(2) r(2); r_last(2), r_last(2)];
    Z=zeros(2,2);
    
    % Check if pen is close enough to the screen for painting
    if abs(r(3)) < target.screen_touch
        paint=1;
    else
        paint=0;
    end
    
    %compute the color
    ang_int=[-pi/2 pi/2]; %The angle interval for controlling the color
    ang=atan2(m(2),-m(3)); %The tilting angle of the pen
    ang_last=atan2(m_last(2),-m_last(3));
    hsv_color=(ang-ang_int(1))/(ang_int(2)-ang_int(1)); %The corresponding value on the hsv colormap [0 1]    
    hsv_color_last=(ang_last-ang_int(1))/(ang_int(2)-ang_int(1));
    
    % Compute color matrix
    C=[hsv_color(1) hsv_color(1);hsv_color_last(1) hsv_color_last(1)];
    C=max(min(C,1),0);    
           
end